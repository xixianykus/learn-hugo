---
title: "Links"
date: 2020-05-12T17:06:03+01:00
draft: false
style: |
  ol {
    margin: 0;
    padding: 1em;
      columns: 300px 3;
      column-gap: 2em;
  }
  ol > li {
    break-inside: avoid;
  }
---

## Tutorials on Hugo

1. [Main Hugo site](http://gohugo.io)
2. [Getting Started with Hugo](https://www.youtube.com/watch?v=hjD9jTi_DQ4) is a free video course on Youtube by Envato Tuts+.
3. [Mike Dane's Hugo series](https://www.mikedane.com/static-site-generators/hugo/) w both written and video tutorials.
4. [Bruno Amaral's Hugo posts](https://brunoamaral.eu/tags/hugo/) include how to build a gallery using [Photoswipe](https://photoswipe.com/) and also import images from Instagram
5. [Hugo cheat sheet](https://regisphilibert.com/blog/2017/04/hugo-cheat-sheet-go-template-translator/) is a very useful post by Regis Philibert.
6. [Hugo, the scope, the context and the dot](https://regisphilibert.com/blog/2018/02/hugo-the-scope-the-context-and-the-dot/) - Regis Philibert
7. [Page Resources](https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/) - explained well by Regis Philibert.
8. [Jekyll and Github to Hugo and Netlify](https://www.sarasoueidan.com/blog/jekyll-ghpages-to-hugo-netlify/) - a comprehensive article on Hugo and some of the trickier and less obvious aspects.
9. [How to create author pages](https://www.netlify.com/blog/2018/07/24/hugo-tips-how-to-create-author-pages/) very well written Netlify article that's worth checking out, not just for author pages.
10. [How to build a blog with Hugo](http://dsheiko.com/weblog/building-a-blog-with-hugo/) - terse and a bit higher level than most basic tutorials.
11. [Zwbetz Blog](https://zwbetz.com/blog/)&mdash;lots of [Hugo posts](https://zwbetz.com/tags/hugo/) and a full basic tutorial too.
12. [Hugo Modules](https://www.thenewdynamic.com/article/hugo-modules-everything-from-imports-to-create/) good article from The New Dynamic.
13. [dev.to](https://dev.to/search?q=hugo) Hugo links
14. [Tuts Plus Hugo Tutorial](https://code.tutsplus.com/tutorials/make-creating-websites-fun-again-with-hugo-the-static-website-generator-written-in-go--cms-27319)
15. [Adding Font Awesome Icons](https://matze.rocks/posts/fontawesome_in_hugo/)
16. [Responive Images in Hugo](https://laurakalbag.com/processing-responsive-images-with-hugo/) by Laura Kalbag
17. [Glenn McComb's guide to Pagination in Hugo](https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/)
18. [How to build menus in Hugo](https://www.youtube.com/watch?v=E6bhmixcR5k)
19. [Make Creating Websites Fun Again with Hugo](https://code.tutsplus.com/tutorials/make-creating-websites-fun-again-with-hugo-the-static-website-generator-written-in-go--cms-27319) at tutsplus.com.
20. [Kodify Hugo tutorials](https://kodify.net/hugo-static-site-tutorials/) are well explained and cover some less known features of Hugo.
21. [Harry Cresswell's Hugo articles](https://harrycresswell.com/tags/hugo/)
22. [Hugo survival guide](https://www.philipp-janert.com/guides/hugo-survival-guide/) and [A guide to Hugo concepts](https://www.philipp-janert.com/blog/2020/hugo-survival-guide-part1/) both by Philip Janert is an in depth resource. There are [many other articles](https://www.philipp-janert.com/categories/hugo/) on Hugo there too.

There is also the [Awesome Hugo mega list](https://github.com/theNewDynamic/awesome-hugo/blob/main/README.md#books) on Github and [axwesome-hugo.dev](https://www.awesome-hugo.dev/).

## Themes
1. [HTML 5 Up](https://html5up.net) has a good selection of free responsive designs (not Hugo secific).
2. [Jamstack Themes for Hugo](https://jamstackthemes.dev/#ssg=hugo)

## Plugins and services
1. [Algolia Search](https://www.algolia.com/) can be installed via Netlify plugins page.
2. [Hugo Shortcode Gallery](https://github.com/mfg92/hugo-shortcode-gallery) is a theme component for Hugo using Lightbox, another JS image gallery.
3. [Hugo Easy Gallery](https://www.liwen.id.au/heg/#gallery-usage) uses two Hugo shortcodes to create a gallery of images displayed with Photoswipe, a JS image gallery.
4. [Hugo Gallery](https://github.com/icecreammatt/hugo-gallery) can be used to generate pages from a folder of images. It's made in Go so you need Go installed. Now quite old and seeminly not maintained.
5. [My Client Wants](https://myclientwants.com/) has good info on Jamstack solutions covering a range of topics from forms to search to apis, comments and more.

## Online tools
1. [Online YAML tools](https://onlineyamltools.com/) can verify YAML files or text, converts YAML to JSON, CSV, XML and vice versa, minifies or prettifies YAML and much more.
2. [Convert Simple](https://www.convertsimple.com/convert-yaml-to-toml/) converts YAML to TOML and back and just about any other conversion you can think of. Also converts images inc. webp and svg.

**Also see:** a long list of links to [Hugo stuff](https://github.com/theNewDynamic/awesome-hugo?utm_campaign=tool&utm_medium=website&utm_source=thenewdynamic.org#articles) on GitHub.

## Serverless Static Hosting

1. [Netlify](https://www.netlify.com/) up to 100Gb bandwidth pcm.
2. [Vercel](https://vercel.com/) free for non commercial sites up to 100Gb bandwidth pcm.
4. [Edg.io](https://docs.edg.io/) free, unlimited.
5. [Cloudflare Pages](https://pages.cloudflare.com/) unlimited bandwidth.
6. [Railway](https://railway.app/)
7. [Surge.sh](https://surge.sh/)
8. [Render](https://render.com/)

## Forms

1. [Formspree](https://formspree.io/) is a form service. Up to 50 free submissions per month, "for testing and development".
2. [GetForm](https://getform.io/) 50 subs pcm, 100mb storage. After that it's $7.50 pcm for 1000 subs.
3. [Form-Data](https://form-data.com/) 100 subs pcm for free, after it's $6.80 for 1000 subs.
4. [Formcake](https://formcake.com/) 100 subs pcm, 90 days retention and 5 actions. After $59 per year.
5. [Netlify](https://www.netlify.com/products/forms/) for sites hosted on Netlify up to 100 subs pcm for free.

## Comment Systems

1. [Graph Comment](https://graphcomment.com/en/pricing.html) - free tier includes up to 1m data loads per month
2. [Utteranc.es](https://utteranc.es/) - free open source software to use comments via GitHub's issues system. By [Jeremy Danyow](https://danyow.net/using-github-issues-for-blog-comments/)
3. [Intense Debate](https://intensedebate.com/) - unclear whether this is free or not.
4. [Hyvor Talk](https://talk.hyvor.com/) - only 1 website but unlimited comments and 40k page views pcm on the free plan.

## CMS

1. [Netlify CMS](https://www.netlifycms.org/) is an open source React app that works with Git.
2. [Headless CMS Comparison](https://cms-comparison.io) covers several different CMS's 
3. [Strappi CMS](https://strapi.io/) open source CMS built with React that runs on Node.js (See this [tutorial](https://www.smashingmagazine.com/2020/08/static-blog-sapper-strapi/) using Strapi with Sapper and Netlify).
4. [Forestry.io](https://forestry.io/) limited free plan.
5. [Sanity.io](https://www.sanity.io/solutions/headless-cms)
6. [Contentful](https://www.contentful.com/) free for communtity projects, personal sites, includes Graph QL
7. [Stackbit](https://www.stackbit.com/) allows WYSIWYG editing of Jamstack sites. Free for one site.
8. [Agility CMS](https://agilitycms.com/) free for devs, included hosting, 100gb pcm, 10gb storage.
9. [More CMS's at Jamstack.com](https://jamstack.com/headless-cms/)

## Various Add Ons

1. [Hugo Gallery](https://github.com/icecreammatt/hugo-gallery)

## Showcase sites

1. [Lapa Ninja](https://www.lapa.ninja/)
1. [Land Book](https://land-book.com/)
1. [Pages XYZ](https://www.pages.xyz/)
1. [Klart.io](https://klart.io/pixels)
1. ui8.net - illustratons - cost
1. [Unmatched Style](http://www.unmatchedstyle.com/)
1. [Style Gala](http://www.stylegala.com/)
1. [CSS Import](http://www.cssimport.com/)
1. [Veracon](http://thesis.veracon.net/)
1. [Design Shack](http://designshack.co.uk/index.php) - not a gallery but great articles
1. [CSS Mania](http://cssmania.com/)
1. [CSS Remix](http://www.cssremix.com/)
1. [Best Web Gallery](http://www.bestwebgallery.com/)

## E-Commerce

1. [Snipcart](https://snipcart.com/) - £8 pcm up to £396 then 2% per transaction.
2. [Commerce.js](https://commercejs.com/) - free up to \$1870 of sales pcm + 3% transaction fee.
3. [Foxy.io](https://foxy.io/) - very easy set up w different pricing to Snipcart: basic plan: \$15 pcm + 15 cents for more than 100 transactions. Also the docs have some [useful intro info](https://docs.foxycart.com/) about e-commerce.
4. [9 Best e-commerce solutions for Jamstack](https://bejamas.io/blog/jamstack-ecommerce/)

## Other stuff

1. [Coolors.co](https://www.coolors.co/) - awesome colour palette tool.
2. [Happy Hues](https://www.happyhues.co/)&mdash;limited selection of palettes but can see them in context of a web page.
3. [Box Drawing Characters](https://www.w3schools.com/charsets/ref_utf_box.asp) at W3 schools
4. [Syntax highlighting examples](https://xyproto.github.io/splash/docs/all.html). See also the Hugo docs [config settings](https://gohugo.io/getting-started/configuration-markup#highlight)
5. [Favicon Generator](https://formito.com/tools/favicon) quick and easy favicons from letter or emojis (huge selection) in png, svg or inline svg formats.
6. [Font Awesome](https://fontawesome.com/) for icons
7. [Doodad Pattern Generator](https://doodad.dev/pattern-generator/)&mdash;amazing variety of possibilities.
8. [Shape Divider](https://www.shapedivider.app/)&mdash;great way to create wavey and other banner shapes.
9. [15 best font pairing tools](https://www.hongkiat.com/blog/font-pairing-tools/)
10. [Fontjoy font pairing article](https://fontjoy.com/pairing/) - on contrast but sharing some aspects
11. [Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/)
