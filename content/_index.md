---
title: "Home"
date: 2020-05-12T16:12:19+01:00
draft: false
---

So this is my site of random blog posts on using the [Hugo static site generator](https://www.gohugo.io).

It's a bit of a hotchpotch of stuff. I'm not really expecting anyone to find this site. But if anyone finds it useful then great.

This is more aimed at the intermediate Hugo user. There is great info out there to get you started. Mike Dane's video course on Youtube is excellent.

But after the beginner info there is something of a dearth of information. The offcial Hugo docs are pretty comprehensive but somewhat abtruse to those less familiar with using Hugo. They're a great reference but sometimes you need a bit more than just a reference to understand how things work.

Hopefully this site will bridge the gap a bit.