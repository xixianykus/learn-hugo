---
title: "Variables"
date: 2020-05-16T14:42:31+01:00
draft: true
summary: "Notes on using variables"
---


To define a new variable in Hugo
1. Start with a dollar sign
2. use a colon and equals 
3. All in double curly braces - of course.

```go
{{ $NewVariable := resources.Get "css/style.css" }}
```
This would get the css file that is in the assets folder.