---
title: "Target Active Link"
date: 2020-07-11T14:35:03+01:00
draft: false
tags: ["if", "active", "variables"]
summary: "How to target a link when it points to the current page"
---

It's common practice to differentiate a link to the current page in a list of links. One way to do this is to give the link a class of *active*:

```html
<a href="this-page.html" class="active">This Page</a>
```
I prefer to just give the `href` value a hash: `<a href="#">` and then target it in the CSS with an attribute selector:

```css
a[href="#"]
```

You can choose either way, it makes no difference to how we accomplish that in Hugo. The idea is there are two options and we want Hugo to choose the correct one.

Here's a method that's simple to understand and works well:

```go-html-template {hl_lines=["7-15"]}
<ul class="latest">
    {{ $currentPage := . }}

    {{ range first 20 (where .Site.RegularPages "Section" "posts") }}
    
    <li>
        {{ if eq $currentPage . }}

            <a href="#">{{ .Title }}</a></li>

        {{ else }}
        
            <a href="{{ .Permalink }}">{{ .Title }}</a>

        {{ end }}
    
    </li>
    {{ end }}
</ul>
```

The first thing to do is to grab the current page with a variable:

```go
{{ $currentPage := . }}
```

Next we use the range function. In this example we only want the first 10 posts. Because the range function won't accept too many arguments we tell Hugo where to look using the `where` function and keep all that in brackets. 

Next there are two options. The first is for the current page using the hash (#) in the link. The second is for a normal page listing with a link.

The basic syntax is saying: 

> if the current page is the current page use this link or `else` use this second one.

The syntax may seem a little odd. Why create a variable `$currentPage` for instance? Well the dot outside of the range function does represent the current page. But inside the value of the dot varies every time it adds a new list item to the list. By using this variable outside of the range function we grab and freeze that value which can then be used inside the range function.

