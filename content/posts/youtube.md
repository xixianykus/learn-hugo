---
title: "Embedding Youtube videos"
linktitle: "Embedding videos"
date: 2020-10-17T10:43:10+01:00
draft: false
tags: ["youtube", "shortcodes"]
summary: "Embedding and resizing Youtube vids with the built in shortcode"
---

## Update

Since first writing this the CSS property `aspect-ratio` has gained wide support in all major browsers. This means that you may prefer not to use Hugo's built-in `youtube` shortcode anymore. Instead you can write you're own shortcode and use some simple CSS to create a responsive iframe using a `width: 100%` (or any percentage) and `aspect-ratio: 16 / 9;`

```go-html-template
<iframe 
  width="560" height="315" 
  src="https://www.youtube.com/embed/{{ .Get 0 }}" 
  frameborder="0" allow="accelerometer; 
  autoplay; encrypted-media; 
  gyroscope; picture-in-picture" allowfullscreen
></iframe>
```

Note that the `{{ .Get 0 }}` is where the last bit of code at the end the Youtube url goes.

If you call your short code `yt.html` then the shortcode would look like this (pointy brackets changed here) when placed in your content file.

```go-html-template
{{⪡ yt "HfW3ssA9LG4" ⪢}}
```

I've not put the CSS within the shortcode (ie. `style="aspect-ratio: 16 / 9"` ) as it's best done in the main CSS file. Add a class of `video` to the `iframe` and add some CSS:

```css
iframe.video {
  width: min(560px, 100%);
  aspect-ratio: 16 / 9;
}
```

## Original

For better or worse it's common to embed Youtube videos into pages. Hugo has a built in shortcode for this making it simple to do:

```go-html-tempate
{< youtube z9omjR1ha9k >}
```
 Two things here:

 1. Use double curly braces&mdash;they're omitted here to prevent the shortcode from being executed when building this site
 2. Use lower case for `youtube`.

 If you look at the source code produced it's very different to that which you would get from Youtube to embed a video and contains some odd CSS.

 ```html
 <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
  <iframe src="https://www.youtube.com/embed/z9omjR1ha9k" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border:0;" allowfullscreen title="YouTube Video"></iframe>
</div>
```

The reason for this is that Youtube video embed code does not allow create responsive videos. The Hugo code is a little hacky but well used all over the web to make Youtube vids responsive.

Because the containing `div` has no height the `padding-bottom` percentage uses the width size to calculate it's height. It calculates the padding-bottom size as 56.25% of whatever the width is. This means the container will always have an aspect ratio of 16:9, the right size for Youtube videos.

Incidentally I tried to use the new CSS property `aspect-ratio` to solve this problem to no avail. 

## Resizing videos

One issue with this, depending on how or where you're using it, is that the video will automatically stretch to fill the size of it's container which may not be what you're after.

The fix for this is to limit the size of the `iframe` with some CSS: `max-width` and `max-height`. If you only have one `iframe` and don't intend to add anymore simply add

```css
iframe {
    max-width: 580px;
    max-height: 315px;
}
```

Use whatever max width and height you need but try to use a ratio of 16:9 which is the default video aspect ratio for *most* Youtube videos and the aspect ratio of the containing div.



## Targetting specific iframes

Since the Hugo shortcode does not contain any class or id attribute f you need to be more specific targetting just one video or just youtube iframes you can use attribute selectors. For instance:

```css
iframe[src*="z9omjR1ha9k"]
```

is very specific targetting that one video. Alternatively:

```css
iframe[src*="youtube"]
```
will target all Youtube videos.

NB. Note the asterick *wild card* before the equals sign to signify the string can be anywhere in the value of `src`.

## Links

1. [Responsive Youtube embed](https://avexdesigns.com/blog/responsive-youtube-embed)

