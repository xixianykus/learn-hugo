---
title: "Singularize Content Folders"
date: 2020-06-27T15:59:53+01:00
draft: true
tags: ["Singularize", "content", "list page"]
summary: "How to stop content folders ending as a plural"
---

By default if you create a new content folder like `/content/blog` or `/content/theme` Hugo will add an *s* when that appears in a menu or list on the page.

To prevent this simply add an `_index.md` file to the folder. By default the title should take the name of the folder so if it's singluar it will be fixed. Otherwise though just tweak it in the frontmatter:

```yaml
---
title: blog
---
```

