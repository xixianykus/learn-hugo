---
title: "3 ways to add the year"
date: 2020-06-01T20:59:16+01:00
draft: false
tags: ["date", "now function"]
summary: "Three different ways to add the current year"
---

Here are 3 methods to add the year to somewhere in your templates. They're all fairly short so it shouldn't matter which you choose. But this is illustrative of how there is usually more than one way to accomplish something in Hugo.


1. You can add the date with just the year: 

```go
{{ .Date.Format "2006" }}`
```

2. Another way uses the `now` function. You don't need `.Date` at all:

```go
{{ now.Year }}
```
A typical use case might be to add it to the footer along with a copyright symbol:

```go-html-template
<footer>
	<p>&copy; {{ now.Year }} <a href="{{ .Site.BaseURL }}">{{ .Site.Title }}</a></p>
</footer>
```

3. An here's another way to use the now function:

```go
{{ now.Format "2006"}}
```

Note the key date that's always used define the date format: 2 January 2006.

REMEMBER: These dates won't change with just time. They will only update when Hugo is run.

