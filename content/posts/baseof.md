---
title: "The baseof template"
date: 2020-05-16T10:42:12+01:00
draft: false
tags: ["baseof.html", "blocks", "partials", templates]
summary: "Using Hugo's baseof.html template to create the underlying structure of your pages."
---

In the baseof.html file you use blocks of content from elsewhere which you define.

For instance you might want to create a `block` called *content*. So you add the following to the baseof.html file:

```go
{{ block "content" . }}{{ end }}
```

Then you have to *define* what is in your block in the other template files, typically `single.html` and `list.html` at a minimum.

```go-html-template
{{ define "content" }}
    <h1>{{ .Title }}</h1>

    <time>{{ .Date.Format "02 Jan 2006" }}</time>

    {{ .Content }}

    {{ partial "next-and-previous" . }}

{{ end }}
```

Typically since you'll only have one `baseof.html` file you'll only have one `block` though that will contain different code depending on whether it's pulled from list.html, single.html or index.html. Typically this is often called *main*. I called it *content* here just to show you can call it whatever you want.