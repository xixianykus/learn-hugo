---
title: "How to Add Tags"
date: 2020-09-26T17:30:18+01:00
draft: false
tags: ["tags", "with", "range", config, taxonomies]
summary: "How to add a page's tags and links to each page"
---

You can create any taxonomy by adding a `Taxonomies` group your config file and adding it there:

```toml
[Taxonomies]
tag = "tags"
group = "groups"
```

Hugo adds both *tags* and *categories* by default to this is only needed if you are creating more taxonomies.

And if you do want *more* taxonomies, that is in addition to the default tags and categories, you need to add both of those default taxonomies to the list too.

```toml
[Taxonomies]
tag = "tags"
category = "categories"
group = "groups"
```

If your site does not need any taxonomies then you can add this line to the config file:

```toml
disableKinds = ['taxonomy', 'term']
```

NB. Sometimes this doesn't take effect immediately.

You add tags to the frontmatter of content pages in a standard YAML array:

```yaml
tags: [fruit, rabbits, europe]
```

Note that there is no information about the url of each tag. So we can't simply use `{{ .RelPermalink }}` in our template. So how do we get the link?

All the tags live in the automatically generated taxonomy folder `/tags/`. That provides the first part of our url. The second part, the name of the sub folder where the specific tag (taxonomy term) lives.

To retrieve the tag values for each page we need to use the `range function` on `.Params.tags`. However since some pages might not have the tags variable in the frontmatter we'll use the `with` function to say that: "only carry out the code inside when tags are present in the page".

```go-html-template
{{ with .Params.tags }}

{{ end }}
```

Inside here will go our tag information.

```go-html-template
{{ with .Params.tags }}
<p>Tagged in:
        {{ range . }}
            <a href="/tags/{{ . }}/">{{ . }}</a>,
        {{ end }}
</p>
{{ end }}
```

Theoretically this should work. Unfortunately it doesn't and the links go to 404 error page not found pages. The trailing forward slash isn't rendered. We need to do one final thing and that is `urlize`, in Hugo lingo, our word or phrase used for each tag. Fortunately this easy to do. We can pipe the word/phrase like so:

```go
{{ . | urlize }}
```

And with that we have the following, working template code:

```go-html-template
{{ with .Params.tags }}
<p>Tagged in:
        {{ range . }}
            <a href="/tags/{{ . | urlize }}/">{{ . }}</a>,
        {{ end }}
</p>
{{ end }}
```
