---
title: "Multiple Templates"
date: 2021-04-03T16:02:03+01:00
draft: false
tags: [""]
summary: "How to use multiple templates in Hugo"
---

The 3 main layout templates in Hugo are in the layouts folder and sub-folder, `_default`. These are:

- `index.html` for the home page
- `_default/list.html` for section, aka list, pages
- `_default/single.html` for regular pages

These serve the basic needs of a website and in many cases are all you need. 

## 1. Use a partial with a conditional statement

Often you may not need to change a whole page, just a bit of bit. In this case it may be easiest to add one or more partials in a conditional statement.

```go-html-template
{{ with .Params.recipe }}
  {{ partial "recipelist.html" }}
{{ end }}
```
What this is saying is that if the frontmatter of a page contains a variable with the key `recipe` the recipelist partial will be placed here. Of course you can put anything here including HTML if you want.

## 3. Shortcodes

Hugo shortcodes, like partials, can contain any amount of HTML and can be placed anywhere inside a markdown file. So it's easy to add HTML multiple times in the same page. The disadvantage is you have to manually insert them each time. You can add text via the shortcode parameters. See [shortcodes](/posts/shortcodes/) for more.

## 2. Mirror a content directory in your Layouts folders

If you have content folder called `blog` for instance create another folder called `blog` under your `layouts` one. If you then create any files, single.html, index.html or list.html, the pages in your `content/blog/` folder will use these instead default ones.

## 3. Using a Layout key

If you have specific layout for, let's say, your recipe pages you can first place `layout: recipes` in the frontmatter of such pages. This page will then use the `recipes` layout. This layout is stored in a file in `layout/recipes/` directory. Again use single.html or list.html for a list page.

## 4. Using CSS

OK this is not Hugo coding but worth mentioning for completeness. There are several ways to do this. If you've allowed HTML content in your markdown files, or you are using HTML pages, you can add style tags anywhere in your markdown file and it will be processed. However a better way is to add a class name to the body tag of your page. You could do this via the frontmatter.

```yaml
bodyclass: recipe
```

Then find the body tag in your template file and write:

```go-html-template
<body {{ with .Params.bodyclass }}class="{{ . }}"{{ end }}>
```

The pages with the above `bodyclass: recipe` in their frontmatter will then have `<body class="recipe">` in their pages allowing you to target the whole page's CSS. Of course this works with any HTML element on the page but the higher up it is the more elements you can control with CSS.

