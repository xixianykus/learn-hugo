---
title: "Coming Up"
date: 2030-05-12T16:42:33+01:00
draft: true
summary: A list of topics I want to add to this site.
---

**To Dos**

- add a responsive menu
- reduce number of sidebar posts for small screens.


**So this is just a list of topics I want to add at some point:**

`js.Build` and `hugo mod npm pack`

Using Hugo Modules

List of commonly used config settings.

Getting a list section to show up on every page, using aside.html?

Adding extra pages like a links page, contact etc.

Creating lists using taxonomies in the config file. (see more learning)

The archtypes folder and the replace function

Why is this page not showing up? HOw to debug it? Had to relaunch the server to get it show up.

Why is the aside.html partial only showing up on the home page? And should it have .Permalink or .RelPermalink

What is the .Name variable?

Implied pages (there is no posts.md or posts.html file but there is a posts page)

Conditionals - already a draft page

Minimum steps to take when building a site from scratch. - already a draft page.