---
title: "Syntax Highlighting"
date: 2019-11-03T19:52:08Z
draft: false
tags: ["syntax highlighting"]
summary: "How to implement Hugo’s built in syntax highlighting."
---

The Go language Hugo is built upon comes with built in syntax highlighting for marking up blocks of code in many different languages.

Originally it used a system called _pygments_. This still works (depending on which Hugo version you're using). BUT that system is being phased out in favour of the new one called _Chroma_.

There are many different languages available. For general web dev HTML, CSS and JS are obviously there. For the full list see [Chroma on GitHub](https://github.com/alecthomas/chroma).

The different highlight themes can be viewed [here](https://xyproto.github.io/splash/docs/all.html)

To enable this feature in Hugo simply add this to your config.toml:

```toml
[markup]
  [markup.highlight]
    codeFences = true
    guessSyntax = false
    hl_Lines = ""
    lineNoStart = 1
    lineNos = true
    lineNumbersInTable = false
    noClasses = true
    style = "native"
    tabWidth = 4
```

You then have to add the language (html, css, js etc.) to code block.

You can try different styles with specific languages [here](https://swapoff.org/chroma/playground/). Some good dark theme colours are:

- monokai
- dracula
- solarized-dark
- solarized-dark256
- nord
- native
- paraiso-dark
- swapoff

## Markdown

In markdown simply add _html, css_ or _js_ straight after the first 3 backticks that open the code block.

````markdown
```html
<p>Some <b>HTML</b> code.</p>
```
````

**Note:** you can also create code blocks using line spaces and indenting the code.

    <div><span><b>some text</b></span></div>

But then there's no highlighting, even if you tell Chroma to guess the language.

Other useful languages include:

- toml
- yaml
- go
- go-html-template (for Hugo templates)
- bash
- json

### Overiding the conf file settings

Aside from choosing the language you can also change the configuration of specific blocks of text. This is done using a single pair of curly brackets after you define the language. For instance to turn off the line numbers you can write:

````md {linenos=false}
```toml {linenos=false}

```
````

Other possible options include:

- {linenos=table}
- {hl_lines=[8,"15-17"]} this means highlight lines 8 and all those from line 15 to 17.
- {linenostart=199}

### Errors in the code

If there's an error in your code the highlighter will sometimes point it out by changing the background colour of specific characters.

## HTML files

In an HTML file add class name and _data-lang_ attributes:

```html
<code class="language-html" data-lang="html"></code>
```

However the best way is to use the built in shortcode:

### Results

This can look like this:

```html
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>{{ .Title }} | {{ .Site.Title }}</title>
  <link rel="stylesheet" href="/css/style.css" />
  <link
    rel="alternate"
    type="application/rss+xml"
    href="/tags/index.xml"
    title=".moreLearning"
  />
</head>
```

And for CSS:

```css
aside h3 {
  color: var(--headfootbg);
}
aside ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
```

You can change the colour of the background using `pre` selector in your CSS. This adds a background for any time you're not using a specified language. Unfortunately the same is not so straightforward for `<code>` . Because that is inline and used inside the pre tag it will just highlight the lines that show up as stripes against the background.

The solution is to use:

```css
pre > code {
  display: inline-block;
  background-colour: orange;
  padding: 0.5em;
}
```

NB. The drop shadow is not part of the syntax highlighting. That was added to the CSS using:

```css
pre {
  box-shadow: #999 1px 1px 5px;
}
```

## Shortcode version

Another way you can add syntax highlighting is to use Hugo's, built in, shortcode for the job. For HTML code it would look like:

```go
{{ highlight html "linenos=table,hl_lines=8 15-17,linenostart=199" }}
// ... code
{{ / highlight }}
```

NB. Don't forget the &lt; and &gt; angle brackets just inside the curly brackets, left out here to prevent this being an actual shortcode.

## Links

1. [Chroma on GitHub](https://github.com/alecthomas/chroma).
2. [Chroma themes](https://xyproto.github.io/splash/docs/all.html)
3. [The Chroma Playground](https://swapoff.org/chroma/playground/) lets you experiment with code, themes and languages.
4. [zwbetz post on chroma](https://zwbetz.com/syntax-highlighting-in-hugo-with-chroma/)
