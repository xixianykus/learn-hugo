---
title: "Page Specific Data"
date: 2020-07-10T11:13:21+01:00
draft: false
tags: ["data", "YAML", "index"]
summary: "How to bring data into specific pages"
---

It is possible to run a Hugo site just from data files stored in the `/data/` folder. There's probably not much point to that unless you already have those files lying around and want to create a web site from them. But regardless it is useful to know how to collect data from the `/data/` folder to be placed on individual pages.

Since the code to access template variables are in the templates themselves it's not obvious how to go about accessing data for just one page.

You could put everything in the frontmatter or you can add content to data files in the `/data/` folder. However doing this it's not so easy to link such content to each individual page. The way to access this content is via template variables which are in the template files. So how to link a particular data file to one specific page?

## Method 1

First name your data file the same as your page. For `my-blog-post.md` make a corrosponding file in the data directory, `my-blog-post.yaml` for example. You can use TOML or JSON files if you prefer.

Next you access this in your page template by grabbing the file name and appending it to the call for the data file. This is not obvious at all but I found the code on the Hugo forums, tried and it works fine.

```go-html-template
{{ with (index .Site.Data.blogposts (substr $.File.LogicalName 0 -3)) }}
    <div>Synopsis: {{ .Params.synopsis }}</div>
{{ end }}
```

Above the `.Site.Data.blogposts` refers to a sub-folder of the data folder called _blogposts_.

The `index` function according to Hugo docs:

> &hellip;returns the result of indexing its first argument by the following arguments. Each indexed item must be a map or a slice

The syntax the docs says is:

```go
index COLLECTION KEYS
```

The next bit `(substr $.File.LogicalName 0 -3)` is extracting the first part of the file name before the dot. So starting from the beginning letter ( which is what the `0` points to) to 3 characters from the end. Those unwanted 3 characters will be the `.md`. To reference a file in the data folder you don't need to give it an extension so that's set to pull the file.

The last part is the `{{ .Params.synopsis }}`. This is a key within the yaml file. This file will typically have multiple key/value pairs (variables) and so these can then be accessed anywhere between the opening `{{ with ... }}` and it's end tag via the keyname alone.

So using this you could store everything in a data file if you wanted to. Not sure how much of an advantage that would be though since you could store it all in the frontmatter too if you wished without the need to create an additional file. But being able to access data specific to certain pages has to be a good thing to know.

## A second method

Here's a different and perhaps better way. In the `/Data/` folder is a folder called art that contains YAML files. Each file is named and pertains to a different style of art: graffitti, portrait, oil painting, digital etc.

With each YAML file is an array of links for images:

```yaml
Images:
  - http://www.cloudflare.com/image1.jpg
  - http://www.cloudflare.com/image2.jpg
  - http://www.cloudflare.com/image3.jpg
  - http://www.cloudflare.com/image4.jpg
```

Now in our website we want to create a page for each style of art and list out the images in the page for that style.

This time we'll use the title of our page as the variable and name our data files the same as the title. But, in case the title has spaces we'll replace them with hyphens. And we'll make sure it's all lower case by piping our title to Hugo's `lower` function.

```go-html-template
(replace .Title " " "-" | lower)
```

Next we'll create a variable called _artlinks_ using the `index` function to incorporate our title variable into the data folder string.

Note the `"images"` on the end refers to the section of the YAML file we want the `range` function to loop through.

Finally we loop through our variable `$artlinks` and use the dot to to place the url of each image in an `<img>` tag.

```go-html-template
{{ $artlinks := index .Site.Data.art (replace .Title " " "-" | lower) "images"}}
{{ range $artlinks }}
    <div><img src="{{ . }}"></div>
{{ end }}
```
