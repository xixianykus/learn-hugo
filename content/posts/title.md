---
title: "The Title Variable"
date: 2020-07-07T21:26:03+01:00
draft: false
tags: ["variables", "title", "print", "home page", "IsHome"]
summary: "The home page typically only uses site title. How to set that up."
---


There are several kinds of title in Hugo. Every page can have it's own title which is set in the frontmatter of the page. Each page can also have a shortened version of the title for where the regular title is too long to appear in menus and elsewhere. This is set as the `linktitle` in the frontmatter and accessed as `{{ .LinkTitle }}` from within a Hugo template. If a page's frontmatter doesn't declare a `linktitle` then the template `{{ .LinkTitle }}` variable will use the regular title instead.

The other kind of title is the the title of the website. This is also declared using `title` but it's set in the config file, eg. `hugo.toml` or the older `config.toml`.


To access the websites title in the config file as:

```toml
title = "Learn Hugo"
```

you use `{{ .Site.Title }}` in your templates.

But every page (apart from the Home page usually) has it's own title. This is typically stored in the page's frontmatter with the same key word `title`:

```bqn
---
title: Variables
---
```

To get the title from the page's frontmatter you use the page context: `{{ .Page.Title }}`

However as this is typically used in a page you don't need to specify the context `.Page`

Just use `{{ .Title }}`


## Varying title for home page

Here is the code for setting the `<title>` tag in the `<head>` template.

For the home page we only want the title of the site. This is set in the site's config file `hugo.toml` with variable `title = "My Blog"` (in TOML). 

For all other pages we want the page title followed by the pipe character then the site title.

The built in variable `.IsHome` is used to check whether a page is the home page or not. When negated with the `not`  it does the opposite: it checks whether a page is any page except the home page. If that's *true* then the code inside runs. In this case the code is the page title along with the pipe character.

If it's not true, that is it's the home page, then only the site's title is used and any page title set (usually in `content/_index.md`) is ignored.


```go-html-template
<title>
    {{ if not .IsHome }}{{ .Title }} | {{ end }}
    {{ .Site.Title }}
</title>
```

For the home page you get:

```html
<title>Site Title</title>
```

.. and for all other pages you get:

```html
<title>Page Title | Site Title</title>
```


## Using `site` function

Another way to get the site title is: `site.Title`, that is lowercase `site` without the preceding dot.


