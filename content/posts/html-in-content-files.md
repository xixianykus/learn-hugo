---
title: "HTML in Content files"
date: 2020-07-10T14:44:02+01:00
draft: false
tags: ["markdown", "HTML", "goldmark", shortcodes]
summary: "Configure Hugo to allow HTML code in markdown files."
---

Typically content files are written in markdown and with Hugo's default configuration you cannot simply add HTML. If a site was hacked via it's CMS the damage done would mostly be limited to changing the text and images and script tags couldn't be used to spread malicious code. 

However markdown can be too limiting and there are several ways to include HTML in your content files.


1. Using an HTML file
2. Allowing HTML in markdown
3. Using shortcodes

## 1. Using an HTML file

The simplest way is just to create a content file as HTML. If your file has a `.html` extention then you can just use HTML in the file. You still need to add a frontmatter section at the top of the file for the page to be processed and included in your site by Hugo.

The disadvantages are you won't be able to use markdown in the file and if the page is hacked via a CMS the hacker can add malicous code via `<script>` tags.

## 2. Allowing HTML in markdown

From Hugo v0.60 there is a new markdown renderer called [Goldmark](https://github.com/yuin/goldmark/#goldmark). Settings for this can be set and changed in the site config file. The default settings do not allow HTML to be written directly in markdown files. However you can change this in the config file by setting `unsafe` in the config.toml file to `true`:

```toml {linenos=false}
[markup]
    [markup.goldmark.renderer]
      unsafe = true
```

If your config is in YAML rather than TOML (above) then it's:


```yaml {linenos=false}
markup:
  goldmark:
    renderer:
      unsafe: true
```

This is better than the first solution since you can now use both HTML and Markdown in the file. (You can't add markdown inside HTML tags though.)


## Shortcodes

The best way to include HTML for most cases is using shortcodes. These are extremely versatile and easily created by first adding a folder called *shortcodes* to your `/layouts/` folder. Then simply adding an HTML file to that folder. The name you give the file will be the name of the shortcode so for example: `my_shortcode.html` is called in a content file as {{⪡ my_shortcode ⪢}}.

Shortcodes can be straight blocks of HTML and Hugo's template language or can be used to wrap around content in your markdown file.

To wrap HTML tags around markdown content is with the `{{ .Inner }}`. To make something like a `<div>` shortcode:

```go-html-template
<div class="attention">
  {{ .Inner }}
</div>
```
The `.Inner` variable is whatever content you wish to put in between the opening and closing shortcode. 

```go-html-template
{{⪡ div ⪢}}

<!-- some HTML code here -->

{{⪡ /div ⪢}}
```

Using `{{ .Inner }}` means the shortcode now needs a closing tag. 

The default behaviour is that any *inner* content of such shortcodes is HTML. In earlier versions of Hugo this was expected to be markdown. You can change this back by using `markdownify`:

```go-html-template
<div>

{{ .Inner | markdownify }}

</div>
```

In this case any HTML content within the shortcode opening and closing tags will be stripped AND any markdown code such as `## headings` will be processed.


See the [shortcodes page](/posts/shortcodes/) for more info.



## Links

1. [Goldmark on GitHub](https://github.com/yuin/goldmark/#goldmark)
2. [Smartypants](https://daringfireball.net/projects/smartypants/)
3. [Hugo docs Goldmark](https://gohugo.io/getting-started/configuration-markup#goldmark)
4. [Hugo docs on shortcodes](https://gohugo.io/templates/shortcode-templates/)