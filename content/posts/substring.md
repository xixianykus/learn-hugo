---
title: "Substring"
date: 2020-07-19T09:58:06+01:00
draft: true
tags: ["substr", ]
summary: "How to extract part of a string using substr"
substring: my-file.md
---

## Start and length

The `substr` is used to extract part of a string. But it can be confusing at first. With just two numbers it can be used to extract any part of a string.

According to the docs it can take two parameters the *start* and the *length*. Or if there is just one parameter that will be the start point and it will go to the end of the string.

Imagine you are selecting text. If you want to begin with the first letter of the string you put your cursor BEFORE the letter. In `substr` we use the figure begins counting with a "0".

**NB. If using on a page parameter that is not always present then you need to wrap `{{ .Params.myVar }}` with a `if` statement to prevent errors:**


```go-html-template
{{ if }}
    {{ substr .Params.myVar  0 -3 }}
{{ end }}
```

Using a minus figure on the length will remove the last 3 characters:

```go-html-template
{{ substr "BatMan" 0 -3 }}
```
will leave just "Bat"


```go-html-template
{{ substr .Params.varname 0 -3 }}
```


For instance in a site I was creating from other existing files the titles were completely different to the file names which were numbered *chapter01.md* etc. 

I wanted just the number to put in front of the title listing so it was clear which chapter number was which.

Here is an example. "toolhead" is the string here and we want to start at the first letter (0) and end at the fourth.

`{{ substr "toolhead" 0 4 }}` 

This gives `tool`.

NB. **The first character after the string sets the starting point and the second sets it's length.**

### On a variable

So the same operation can be performed on a variable. Here on the built in variable `{{ .Title }}`

`{{ substr .Title 0 4 }}`

The output will be the first four letters of the page title.

## Counting from the end

If you want to count from the end of the string you use *minus*. So starting from the fifth character from the right means `-4` because the counting starts with zero.

`{{ substr "walking-back.md" -4 2 }}`

And this counts forwards from that character 2 more characters so you get ck

So how would use this to chop off the `.md` ?

Well we'd start at 0, the first letter and then use -3 to count 3 from the right. With this however long the file name is we'd extract it without the last 3 characters

## Links

1. [Hugo docs on substr](https://gohugo.io/functions/substr/)
