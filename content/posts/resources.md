---
title: "Resources"
date: 2024-04-02T08:16:55+01:00
draft: true
tags: ["resources", page bundle, assets]
summary: "What are resources and how and why can Hugo process them?"
---

In general terms *resources* are the extra things that augment a site or project. These are often referred to as *assets* and include things like images, CSS, and JavaScript.

In [Hugo parlence] resources are more specific in that they are incorporated into Hugo's site build process. You can keep CSS or image files, for example, in the static folder and Hugo won't touch them. However if you put the same files into the `/assets` folder or in to a leaf or branch bundle then Hugo can take them and process them in some way. Images can be resized, cropped etc. multiple CSS files can be concatenated into one, SASS files can be transformed into CSS and much more.

So a resource is something other than the project's content files that can be processed by Hugo.

The docs distinguish between 3 types of *resource*: global, remote, and page.

## Global, remote and page resources

A *global* resource is so named because it's available to the whole site. Such resources are kept in the `/assets` directory.

A *remote* resource is one that is not part of the site but somewhere else on the internet.

A *page* resource is one that is part of a page bundle, that is either a leaf or branch bundle [^1].


To use such resources in a project there are various functions to *get* or *capture* them. It's important to distinguish between the resource itself, the file, and the path to the resource. To work on a resource file we need Hugo to get the file itself rather than the path to it.

## resources vs .Resources

These are the two main ways to grab a resource: the `resources` function or the `.Resources` page method. The practical difference is fairly simple. The `resources` function is used to get global resources, that is those in the assets directory AND remote resources, those found elsewhere on the internet. The `.Resources` page method is used to grab resources within a page bundle.

There are various methods used with these functions to grab what you need depending on whether you want one file or several and what you want to do to such files.

First you need to get or grab the file/s you want so here are the methods you can use with `resources`.


| method                | what it does                                                                                                                                                                 |
| :-------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `resources.Get`       | This is used to get one file and takes one parameter: the path to the file relative to the assets folder                                                                     |
| `resources.GetMatch`  | Similar to above but you don't need the exact file name. For instance using parameter of  `images/*.png` will grab *the first* file with a `.png` extension.                 |
| `resources.Match`     | Similar to `GetMatch` except this will grab *all* files that match the pattern. So using parameter of  `images/*.png` will grab all PNGs within the `/assets/images` folder. |
| `resources.ByType`    | Instead of a path this takes a media type as a parameter. Using `resources.ByType "image"` will grab all images in the `/assets` folder.                                     |
| `resources.GetRemote` | Like `resources.Get` this gets one file but from the internet. So instead of it's parameter being a path it's a url.                                                         |


Note that the first four above can be used with `.Resources` to grab files within a page bundle too. Instead of starting with `resources` start with `.Resources`. The preceeding dot is important.

```go-html-template
.Resources.Get "images/church-tower.jpg"
```

## Examples

When retrieving a resource it's usual to either store it in variable or setting up a `{{ with }}` block of code. Both of can avoid the need to repeat yourself.

```go-html-template
{{ $my_css := resources.Get "css/main.css" }}
<link rel="stylesheet" href="{{ $my_css.RelPermalink }}">
```

Above you can see that one of the properties of the resource is it's location, that is  `.RelPermalink`.

Another way is using a `with` block. This way if the resource isn't found it doesn't cause an error.

```go-html-template
{{ with resources.Get "css/main.css" }}
    <link rel="stylesheet" href="{{ .RelPermalink }}">
{{ end }}
```


## Links

1. [Hugo docs](https://gohugo.io/methods/page/resources/) on the page `.Resources` method.
2. [Hugo docs](https://gohugo.io/methods/page/resources/) on the  `resources` function.


[^1]: Leaf bundles are alternatives to regular content files. Instead of a normal markdown file you have a folder which contains an index.md file for the page's content. The folder can then contain various resources to be used by the page.

[Hugo parlence]: https://gohugo.io/getting-started/glossary/#resource