---
title: "Prev and Next"
date: 2020-06-12T13:46:03+01:00
draft: false
tags: ["navigation", "if", "with"]
summary: "Generate links to the next page and the previous one"
---

The next and previous variable are very handy. Typically in a blog setting you might have a button or link the the previous post and and the next post. These two variables make it easy to accomplish this.


Here are two different ways to use the `.Next` and `.Prev` variables.

According to the docs `.Next`&hellip;

> Points up to the next regular page (sorted by Hugo’s default sort). 

```go
{{ with .Next }}{{ .Permalink }}{{ end }}
```

Here's a different way of doing it with `.Prev`

```go
{{ if .Prev }}
    {{ .Prev.Permalink }}
{{ end }}
```

Using this last example here is how the code for a previous and next page menu could look like:

```go-html-template
{{if .Next}}
<a href="{{ .Next.RelPermalink }}">&lt;&lt;Previous Post</a> | 
{{end}}

{{if .Prev}}
<a href="{{ .Prev.RelPermalink }}">Next Post&gt;&gt;</a>
{{end}}
```

Notice how, in this real live example from an online book, the *previous* and *next* captions are reversed. The default direction is used in a typical blogging list organized vertically and where the latest posts are at the top. In the above example though the list is more like the contents of a book with the first chapters at the top of the list and the later chapters lower down the list.

## Getting to the Next and previous pages alphabetically

Since `.Prev` and `.Next` are accessing lists those lists can be ordered in different ways. The default is usually by the date in the frontmatter. For one project I wanted the *Next* and *Previous* to match the way I'd ordered my list which was alphabetically using `ByTitle`.

This was not obvious. At first I was using `.Site.RegularPages.ByTitle.Next` but then couldn't work out how to limit the pages to just one section. The answer was to use `.CurrentSection` something I hadn't come across before&mdash;to replace `.Site`. Here's my code:

```go-html-template
<div>
    {{with .CurrentSection.RegularPages.ByTitle.Next . }}
        <div class="prev"><a href="{{.RelPermalink}}">&lt; Previous</a></div>
    {{end}}

    {{with .CurrentSection.RegularPages.ByTitle.Prev . }}
        <div class="next"><a href="{{.RelPermalink}}">Next &gt;</a></div>
    {{end}}
</div>
```

Although this was in a partial note the use of the dot here.

Another method that didn't work but could be used elsewhere is:

```go-html-template
<div>
    {{with .NextInSection }}
        <div class="prev"><a href="{{.NextInSection.Permalink }}">&lt; Previous</a></div>
    {{end}}
    {{with .PrevInSection }}
        <div class="next"><a href="{{.PrevInSection.Permalink }}">Next &gt;</a></div>
    {{end}}
</div>
```

## More to learn

Note also there is: `.NextInSection` and possibly `.Next` (mentioned somewhere).


## Links

1. Hugo docs [list of page variables](https://gohugo.io/variables/page/#page-variables).