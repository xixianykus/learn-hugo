---
title: "Range"
date: 2020-05-16T09:47:10+01:00
draft: false
toc: true
summary: "About the range function"
tags: [range, resources, page bundles, GetPage, Parent]
---

In a web site there are lots of similar things that we could consider lists. All the pages on the site could be a list. Or all the pages in the blog section of the site could be another list. All the items in the menu constitute another list. The range function is used to go through these lists, one item at a time, and thus essential for having power over Hugo for all kinds of things.

An example of the basic syntax is:

```go-html-template
{{ range .Pages }}
    {{ .Title }}
{{ end }}
```

This will loop over all the pages, grab the title and print it out on the page in an h3 tag.

The context is changing from page to page as Hugo loops through them one by one.

A typical way to set this up is:

```html
<ul>
{{ range .Pages }}
    <li><a href="{{ .Permalink }}">{{ .Title }}</a></li>
{{ end }}
</ul>
```

On the home page this will list every *section* or *list* page and every top level page (that is files that are direct children of the `content` folder.)

If you want just a list of the section (or list) pages first make sure each section folder has an `_index.md` file present. Then use

```go
{{ range .Sections }} {{ end }}
```

To extend the range to all pages in the site use:

```go
{{ range .Site.Pages }}
{{ end }}
```

&hellip;or `{{ range .Site.Sections }}` for section pages.


## How to add range to anywhere for a given section

A typical use is where you want to list out all the pages in a given part of your web site. For instance you may have one section (a folder) with all your posts on animals called *fauna*.

To access that from anywhere on the site (single as well as list pages and from any section) the first thing to do is get all the pages of the entire site.

```go
{{ range .Site.Pages }}
{{ end }}
```

Next you need to limit that to just the section *fauna*. This is done using the `where` function.

These content folders are *sections* in Hugo so tell Hugo you want to limit it to a section (`"Section"`) then which section you mean (`fauna`):

```go-html-template
     <ul>
         {{ range where .Site.Pages "Section" "=" "fauna" }}
            <li><a href="{{.Permalink }}">{{ .Title}}</a></li>
         {{ end }}
     </ul>
```

If you want the section to *equal* a value (above that is *fauna*), you can leave out the equals operator altogether:



```go-html-template
         {{ range where .Site.Pages "Section" "fauna" }}
         {{ end }}
```

However you can use other logical operators here too. For instance if you wanted to get a list of all pages except those in the *fauna* section you could use the not equal to `!=` instead.


```go-html-template
         {{ range where .Site.Pages "Section" "!=" "fauna" }}
         {{ end }}
```

- `=` equal to, also written as `eq`
- ~~`!=`~~ `! =` (no space) not equal to, also written as `ne`
- `<` less than, also written as `lt`
- `>` greater than, also written as `gt`
- ~~`>=`~~ `> =` (no space) **equals** to or greater than, also written as `ge`
- ~~`<=`~~ `< =` (no space) equals to or less than, also written as `le`
- `in` "Checks if an element is in an array or slice–or a substring in a string—and returns a boolean."

You could also use `Type` instead of `Section`. The page `type` can be set in the frontmatter or, if not set, will be the same as the `Section`: it's top level content folders instead: blog, post etc..

```go-html-template
{{ range where .Pages "Type" "post" }}
	{{ .Title }}
{{ end }}
```


You may need to limit the *range* to just normal pages (ie. not list or section pages):

```go
{{ range .Site.RegularPages }}
    {{ .Title }}
{{ end }}
```
can be used to filter out index pages, like list pages.

## Range parent directories

The `.Parent` variable is useful but needs careful use. From a single page the parent directory is the one it's in. So if you range over that you'll get all the page in that directory.

```go-html-template
{{ range .Parent.Pages }}
  {{ .Title }}
{{ end }}
```

Since there's not always a parent directory it's usually better used with `with`:

```go-html-template
{{ with .Parent.Pages }}
  {{ range .}}
    {{ .Title }}
  {{ end }}
{{ end }}
```

NB. To use either of the above within a shortcode add a `.Page` before the `.Parent`:

```go-html-template
{{ with .Page.Parent.Pages }}
  {{ range .}}
    {{ .Title }}
  {{ end }}
{{ end }}
```

## Range by parameter

A parameter or `param` in Hugo is simply a custom variable. You can create them by simply adding them to the frontmatter. For instance suppose we want to add a variable to measure the *weirdness level*. We can add `weirdness` to the frontmatter:

```yaml
weirdness: 12
```

When you added this to a bunch of pages you might want a listing of all pages ordered by *weirdness*. To do this we use `ByParam`:

```go
{{ range (.Pages.ByParam "weirdness") }}

{{ end }}
```

Now weirdness is not necessarily one simple concept. If we were talking about a video or film we might break the weirdness levels into different parts. Maybe the music is weird or the imagary or the dialogue:

```yaml
weirdness:
  sound: 5
  imagery: 7
  dialogue: 4
```

To create a list on just one of these we can use the dot:

```go
{{ range (.Pages.ByParam "weirdness.sound") }}
{{ end }}
```

### Another way

This is a different way from [Hugo docs](https://gohugo.io/functions/where/).

```go
{{ range where .Site.Pages "Params.weirdness" "sound" }}
   {{ .Content }}
{{ end }}
```

### From inside shortcodes

It can be a bit more fiddly when using `range` inside shortcodes. 

Here are a couple of ways to produces a list of pages with a specific parameter. The first is the preferred method. Note the use of the `.Page` (not `.Pages`). Also note that because the value for `draft` is boolean (ie. is either true or false) it's not a string and should not be inside quotes.

```go-html-template
<ul>
    {{ range where .Site.Pages ".Page.Params.draft" true }}
    <li><a href="{{ .RelPermalink }}">{{ .Title }}</a></li>
    {{ end }}
</ul>
```

This was my first attempt. It works from from inside a shortcode but is less concise. The `.Page` is not needed here because the `if` statement is inside the `range` function. So it has already shifted the scope to `.Site.Pages`.

```go-html-template
{{ range .Site.Pages }}
    {{ if .Params.unfinished }}
        <li><a href="{{ .RelPermalink }}">{{ .Title }}</a></li>
    {{ end }}
{{ end }}
```

See [Hugo docs](https://gohugo.io/templates/shortcode-templates/#params) for more info about using `.Params` inside shortcodes.

## Range from the Blank theme

From the blank theme the following is used in

```go-html-template
{{ range first 20 (where .Site.RegularPages "Type" "in" .Site.Params.mainSections) }}
  <li><a href="{{ .RelPermalink }}">{{ .Title }}</a></li>
{{ end }}
```

## Range over Page Resources

A leaf bundle is where a content file is named `index.md` and resides in its own folder. The folder can also contain other files that are used by the page such as image files. A typical way you might access these is with the `range` function.

These page resources are accessed using `{{ .Resources }}`:

```go-html-template
{{ range .Resources }}
{{ . }}
{{ end }}
```

NB. If you are writing your code in a short code the scope is different from a partial and you need to use `range .Page.Resources`.

Here is an example of a shortcode:

```go-html-template
{{ $carousel := .Site.GetPage "page" "home-page" }}

{{ range $carousel.Page.Resources.ByType "image" }}
<figure>
    <img src="{{ .RelPermalink }}" alt="{{ replace .Name "-" " "}}">
    <figcaption>{{ replace .Name "-" " " }}</figcaption>
</figure>
{{ end }}
```

This will grab all the images in the page bundle `home-page`. That is to say `home-page` is a directory containing images and an `index.md` file. 

If we just use `{{ .Name }}` or `{{ . }}` instead of the image tag with `{{ .RelPermalink }}` we'll get a list of the files:g

- image-01.jpg
- image-04.jpg
- image-03.jpg
- document.pdf
- some-file.doc
- etc..

These could be all types of file: markdown files, executables (exe), text files, MP3s, MP4s, zip files, JSON files, etc..

If you want the link you just use `.RelPermalink`:

```go-html-template
{{ range .Page.Resources }}
<a href="{{ .RelPermalink }}">{{ .Name }}</a>
{{ end }}
```

You cannot use `.File` in this this context. It's not part of the `.Resources` object.

`.Resources` has the following properties.

`.Name`, `.Title`, `.Permalink`, `.RelPermalink`, `.Content`,  and `.MediaType`.

### Media Types

The `{{ .MediaType }}` gets the MIME type. This will produce `image/jpeg` for jpeg files. However whilst JPEGs are fairly obvious many file types are not. A plain text file is `text/plain` whereas a markdown file is `application/octet-stream`. 

The first part, before the slash is it's *main type*, the bit after the slash is the *sub type*. `{{ .MediaType }}` can have the following sub properties:

- `{{ .MediaType.MainType }}` For a jpeg this would just the `image` from above
- `{{ .MediaType.SubType }}` and this would just be the `jpeg` part.
- `{{ .MediaType.Suffixes }}` produces a list of possible suffixes. In this case [jpg, jpeg]

If you page folder (leaf bundle) contains different kinds of file and you want just one type, say the image files you can use `.ByType`. For example if you want some images:


```go-html-template
{{ range .Page.Resources.ByType "Image" }}
<img src="{{ .RelPermalink }}" alt="{{ .Name }}">
{{ end }}
```

### Multiple Leaf Bundle folders

In a leaf bundle it is perfectly fine to have multiple folders to keep your resources in. Using `{{ range .Resources }}` will get the content of all of these. 

But if you just want to list the content of just one of these folders what do you range over? 

One way to do this is to use `.Resources.Match`. The `.Match` can be used in various ways.

```go-html-template
{{ range .Page.Resources.Match "myfolder/*" }}
```

This will get all the files in the folder called *myfolder*


## range and declaring variables

Take simple yaml file you wish to *range* over..

```yaml
mammals:
  sheep: Steve
  dog: Danny
  rabbit: Richard
  tiger: Timmy
  lion: Liam
```

A standard `{{ range site.Data.fauna.mammals }}{{ . }}{{end}}` will get the values (Steve, Danny, Richard etc..). However if you want the key names too (sheep, dog rabbit etc.) you can do it like this:

```go-html-template
{{range $keyname, $element_value := site.Data.fauna.mammals }}
```

This creates two variables `$keyname` and `$element_value` which you use elsewhere within the range function to access as required.

If the above file used an array instead:

```yaml
mammals: [Steve, Danny, Richard, Timmy, Liam]
```

Then the first variable declared as above (ie. `$keyname`) would give the array index - the count starting with 0 for the first item.

## range with a condition

You can use `{{ else }}` inside a range function. If what is being looped over turns out to be empty alternative text or code can go after the `{{ else }}`:

```go-html-template
{{ range $array }}
    {{ . }}
{{else}}
    <!-- This is only evaluated if $array is empty -->
{{ end }}

```

There are various similar methods to achieve different ends. See the [official docs](https://gohugo.io/templates/introduction/#iteration).


See [Ordering lists](/posts/ordering-lists/) for the range function too.

## Links

1. [Harry Creswell on listing content](https://harrycresswell.com/articles/listing-page-content-in-hugo/)
2. [Hugo docs on iteration](https://gohugo.io/templates/introduction/#iteration)
3. [Hugo docs where function](https://gohugo.io/functions/where/)
4. [Hugo docs range by parameter](https://gohugo.io/templates/lists#by-parameter)
