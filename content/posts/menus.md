---
title: "Menus"
date: 2020-05-26T15:11:42+01:00
draft: false
toc: true
tags: ["menus"]
summary: "Various ways of adding menus to a Hugo site"
---

There are several ways to put a menu in your web site. But first what is meant by *menu* in Hugo?

A menu in Hugo is an object with various properties. If set in the site's config file, see ([below](#adding-a-menu-to-the-config-file)), you can see what this looks like. If set using frontmatter variables, or using the `sectionPagesMenu`, you can't see it but know that the object exists and can be accessed in your templates.

All the menus created in a site are accessed using `site.Menus` or `.Site.Menus`. So a menu that you've named *main* for instance could be accessed like this.

```go-html-template
{{ range site.Menus.main }}
  {{ . }}
{{ end }}
```


## A simple section menu

The easiest config is using `sectionPagesMenu` variable in the config file. This requires a name for the menu, eg. _main_. So then the line in the config file would be:

```toml
sectionPagesMenu = "main"
```

This will create a menu object with the name _main_. You can name it what you like. This menu only contains each top level section of your site, eg. *blog*, *articles*, *tutorials* etc..

To put this in your template you need to use the range function to pull out each menu entry in this object like so:

```go-html-template
{{ range .Site.Menus.main }}
    <a href="{{ .URL }}">{{ .Name }}</a>
{{ end }}
```

Note there is only the URL and the name (which is pulled from the file name).

If you want to add any other page to this menu you'll have to either add the link manually in your template or add the menu's name to a page's frontmatter that you want in the menu:

```yaml
menu: main
```

See below for more info on frontmatter menus.

## Adding a menu to the config file

The following is added to the config file. But does it all mean? Have a look and all will be explained below.

```toml
[menu]

  [[menu.main]]
    name = "about"
    pageRef = "/about/"
    weight = "10"
    identifier = "about"
    pre = "<i class='far fa-angry'></i>"

  [[menu.main]]
  name = "posts"
  pageRef = "/posts/"
  weight = "20"
  identifier = "posts"
  pre = "<i class='fas fa-marker'></i>"

  [[menu.main]]
  name = "links"
  pageRef = "/links/"
  weight = "30"
  pre = "<i class='fas fa-bahai'></i>"
  identifier = "links"
```

So the above is written in TOML so is appropriate if your config file is called `hugo.toml`.

### [menu]

The first line [menu] opens up the menu section of the config file and everything indented below belongs to that.

### [[menu.main]]

The next bit `[[menu.main]]` heads one menu item in the menu called _main_. Your site may well use multiple menus and you can name them whatever you like. But the config file has to know which menu the entry refers to.

### name &rarr; `{{ .Name }}`

`name = "about"` is what will appear on your menu. Typically it will be the same as the identifier. But if I wanted to change the menu heading from say _posts_ to _blog_ I'd just change it here.

### pageRef &rarr; `{{ .URL }}`

The `pageRef` is for links to pages within the site. It's accessed using the same template key, `.URL`, as the next item. The forward slash denotes from the root of the site.

### weight &rarr;  `{{ .Weight }}`

The last item says `weight = "100"`. The weight denotes where in the menu that particular link will be. The higher the number the further down the menu that item will be. Because _posts_ has the lightest weight of only 20 that will appear first in this particular menu. The values here are positive or negative intergers or whole numbers: 1, 2, 3 etc.. It's good practice to use spaced higher numbers because it makes it easy to insert another item in between if needed.

### url &rarr; `{{ .URL }}`

If a menu item points to an external link use the key `url`. And to access it in a template use `.URL` as the previous item. 

### identifier &rarr; `{{ .Identifier }}`

This is optional ..

> when two or more menu entries have the same name, or when localizing the name using translation tables. Must start with a letter, followed by letters, digits, or underscores.

### pre &rarr; `{{ .Pre }}`

Next we have: `pre = "<i class='far fa-angry'></i>"`. What is this all about? Well if you look after the equals sign in inverted commas is some HTML. If you're familiar with the service [Font Awesome](https://fontawesome.com/) you may recognize it. Font Awesome provide free icons and that bit of code is to access a specific icon. But the HTML could be anything at all here, or empty. This line can be left out if you don't need it. The first part of this, `pre` is just a variable, which can be placed in a template file like this: `{{ .Pre }}`

### post &rarr; `{{ .Post }}`

This is is much the same as _pre_. This variable typically contains some HTML code for an icon but this is placed after the menu _name_

If this is not set AND if the menu is set in the pages frontmatter this value defaults to the page's weight.

### parent and children &rarr; `{{ .Children }}`

Not used here but used for menus with different levels. (see the [Hugo docs](https://gohugo.io/templates/menu-templates/))

### params

You can also create extra custom variables in a params section. These are accessed in the templates with `{{ .Params.name }}` where name is the name of your custom variable.

## Adding this to the template

Now to add this to a template we use the `range` function combined with `menus` variable.

Since this is in the config file? we use `.Site`:

```go-html-template
{{ range .Site.Menus.main }}
    <a href="{{ .URL }}">{{ .Name }}</a>
{{ end }}
```

Note to use `.URL` here rather than `.Permalink`. That's because the range function is going through the list in the site's config file, `hugo.toml`,  (see above) and variable used is `url`.

Also note that `Menus` is plural. Hugo anticipates several menus for the site and even if there is only one it is found in the _Menus_ section of the config.

## Frontmatter based menu

Another way to create a menu is by adding the menu variable to the frontmatter of pages you wish to appear on the menu. Think of a name for your menu, for example: *topmenu*, then add this line to the frontmatter of a page you want in the menu:

```yaml
menu: topmenu
```

Next use the `range` function to your template to loop over this menu object that created by these frontmatter variables.

```go-html-template
{{ range .Site.Menus.topmenu }}
  <a href="{{ .URL }}">{{ .Name }}</a>
{{ end }}
```

The range function will get the name and URL of each page that has the  menu and write them out one after the other using the HTML above for each one. You could put list item tags around the anchor tags too if you wanted to create a list. However don't place the `<ul>` list tags inside the range function. That would tell Hugo to create a separate list for every link and you only want a separate list item for each one.

```go-html-template
<ul>
{{ range .Site.Menus.topmenu }}
  <li><a href="{{ .URL }}">{{ .Name }}</a></li>
{{ end }}
</ul>
```

Some pages might appear in several menus. Here is the YAML frontmatter for a page that is in both the top menu and a side menu.

```yaml
menu:
 topmenu:
   weight: 200
   pre: '<i class="las la-link"></i>'
  sidemenu:
    weight: 300
```



## Menu for Lazy Bloggers

This is something built into Hugo and will list out all the top level sections in a site. It's actually of limited use as it produces the same list of links as:

```go-html-template
<ul>
    {{ range .Site.Sections }}
    <li><a href="{{.Permalink}}">{{.Title}}</a></li>
    {{ end }}
</ul>
```

However for completeness it's worth looking at how it works.

1. Give the menu a name (like _main_) and add this to your config: `sectionPagesMenu = "main"`
2. Create a range function over this menu:

```go-html-template
<ul>
   {{ range .Site.Menus.main }}
      <li><a href="{{ .URL }}">{{ .Title }}</a></li>
   {{ end }}
</ul>
```

NOTE the use of `{{ .URL }}` rather than `.Permalink`. You are ranging over this menu object which doesn't have a link to it since it's just a line of code in the config file.

3. Finally if you wish to re-order these links add a weight key to the frontmatter of each section list page (`_index.md`): `weight: 10`. The higher the number the weight gets the further that item will be from the left.

## Links

1. [Menus for Lazy Bloggers](https://gohugo.io/templates/menu-templates/#section-menu-for-lazy-bloggers)
