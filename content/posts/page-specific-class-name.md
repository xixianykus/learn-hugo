---
title: "Page Specific Class Name"
linktitle: "Page Class Name"
date: 2020-07-10T15:04:32+01:00
draft: false
tags: ["CSS"]
summary: "How to add a unique class name to the body tag of every page on the site automatically."
---

Being able to target elements specific to certain pages is easily done if each page has a unique body class tag:

```html
<body class="my-unique-class-name">
```

An easy way this can be done is using the `{{ .File.ContentBaseName }}` variable. This takes the name of the file before the dot and extention OR the name of containing folder if file is a leaf bundle. ([See the docs](https://gohugo.io/variables/files/))

To do this simply use the name of the file as the class of the body tag. So add this in your template file where your `<body>` tag is, which typically might be in your `layouts/_default/baseof.html` file:

```go
<body class="{{ .File.ContentBaseName }}">
```

In the HTML this will produce:

```html
<body class="my-page-name">
```

For list pages this will produce `_index` which may or may not be what you want.

To exclude list pages perhaps:

`<body{{ with .Kind "Page" }} class="{{ .FileContentBaseName}}"{{ end }}>`

Now you have a unique class name for every page. That can be used to target any page in your global CSS file without affecting other pages.

## Other possibilities

You may want to target all pages of a section or particular type. For those using `{{ .Section }}` or `{{ .Type }}` should work.

If you want complete freedom to add a specific class name anywhere just add it to the frontmatter. Suppose you want certain pages to have a red background and colour scheme. First create a new variable in the frontmatter of those pages. I'll call it *bodyclass* for instance:

```yaml
bodyclass: red-page
```

Then in your template file where your `<body>` tag is use the `.Params` to access it:

```go-html-template
<body class="{{ .Params.bodyclass }}">
```

In your generated pages you'll then have this:

```html
<body class="red-page">
```

## Conclusion

This is just one way to target pages with CSS in Hugo.

**More:** [Hugo docs](https://gohugo.io/variables/files/) on `.File` etc.
