---
title: "Params"
date: 2020-06-01T07:13:10+01:00
draft: true
tags: ["variables", "params", "config"]
summary: "Accessing variables with .Params"
---

Params are essentially just used by Hugo to access variables. Some are built in to Hugo like *author* and *title* but you can create your own by simply declaring them in your frontmatter.

Like all variables there is a *key* or the name of the variable AND a *value*. For example the key could be *age* and the value would be the age in years, for intance *77*

## Define the key and value

To set this up these need to be defined somewhere. If the value changes from page to page then set it up in the pages frontmatter using YAML:

```yaml
---
age: "77"
---
```

If the the value is the same for the entire site then you create a *params* section in your config file put it under that (using TOML):

```toml
[ params ]
  age = "36"
```

## Call the value in your template/s

Now that we have our *key/value* pair we need to get them onto the page. You write this into the relevant page template (say single.html) to get the value from the page's frontmatter like this:

```go
{{ .Param "age" }}
```

An alternative syntax is:

```go
{{ .Params.age }}
```

To use a variable stored in the site config file use: `{{ .Site.Params.age }}` or the more modern, better? way use the function `site` instead: `{{ site.Params.age }}`


On the page the *age* will be drawn from the page's frontmatter OR if it's not there, from the config file if defined under the `[ params ]` section.

## Built in params

Some variables are built in and don't need to be listed in the `[ params ]` section of your config file.

Hugo docs [list of built in](https://gohugo.io/variables/page/) page variables

## Do not use Empty strings in frontmatter

According to Hugo docs:

> The Param method may not consider empty strings in a content’s front matter as “not found.” [:smile:](https://gohugo.io/functions/param/)

## Creating a listing using params

This uses `ByParam` where the parameter is *rating*.

```go
{{ range (.Pages.ByParam "rating") }}
  <-- code here -->
{{ end }}
```

Hugo docs says:

> If the targeted front matter field is nested beneath another field, you can access the field using dot notation.

```go
{{ range (.Pages.ByParam "author.last_name") }}
  code here
{{ end }}
```

[Read more](https://gohugo.io/templates/lists#by-parameter) on Hugo docs

