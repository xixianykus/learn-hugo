---
title: "Shortcodes"
date: 2020-06-03T22:14:58+01:00
draft: false
tags: ["shortcodes"]
summary: "How to create custom shortcodes"
---

Shortcodes are very similar to partials. Like partials they are HTML files that can contain HTML along with Hugo's templating language. The main difference is whereas partials are used directly in template files (those in the layouts directory) shortcodes are used in content files, typically markdown.

A second diffence is that shortcode may also contain parameters in the code. For instance the built in shortcode for embedding Youtube videos contains all the HTML you need but cannot know which video you want to embed. So a parameter of this is the video code.

```go
{{⪡ youtube QGn-bHeL8a0 ⪢}}
```

Thirdly it's also worth noting that because a shortcode is inside a content page rather than the template file itself the scope is different and code that works in a partial might give the same result in a shortcode. See [below](#accessing-page-variables)

N.B. In these examples the inner brackets are regular greater and less than symbols like HTML. The nest version are used here to prevent Hugo attempting to process them as actual shortcodes.

## Creating your own shortcodes

1. Create a sub-folder called `shortcodes` of your `/layouts/` folder.
2. Create an HTML file in this folder with the name you want the short code to be called.
3. In this file add your HTML code and use the `{{ .Get 0 }}` variable to place your shortcodes parameters in the HTML. A second parameter will be `{{ .Get 1 }}`, a third would be `{{ .Get 2}}` etc. These count starting from 0 upwards.
4. Alternatively you might want your short code to enclose some text so it has a closing tag too:
   1. `{{⪡ myShortcode ⪢}} Some text here {{⪡ /myshortcde ⪢}}` The variable for the inner text is `{{ .Inner }}`. Just put this inside your HTML shortcode file where you want it to be. If you want to use markdown inside your shortcode use the `markdownify` function with the `.Inner`: `{{ .Inner | markdownify }}`
5. Use your shortcode in a content page adding the parameters from L to R.

**IMPORTANT** Shortcodes ARE case sensitive.

## Adding named parameters

Another way to add parameters is to name them. For instance suppose we are creating a short code to create a `<figure>` element. The figure element typically contains an image and a `<figcaption>` element. The image requires a `src` for the specific image and probably an `alt` and possibly a `title` attribute. And the `<figcaption>` needs a caption. You could use the `.Get 0`, `.Get 1` etc. but you would have to remember the exact order these four parameters were in.

An alternative is to use a key for each:

```go-html-template
{{ figure alt="My dog" title="This is my dog Fido" src="/images/mydog.jpg" caption="Fido eating a bone" }}
```

Then in your short code file:

```go-html-template
{{ $source := .Get "src" }}
{{ $alttext := .Get "alt" }}
{{ $title := .Get "title" }}
{{ $caption := .Get "caption" }}

<figure>
    <img src="{{ $source }}" alt="{{ $alttext }}" title="{{ $title }}">
    <figcaption>{{ $caption }}</figcaption>
</figure>
```

## Accessing Page variables

Shortcodes may not be able to access your page level variables, like those in your frontmatter. Adding `.Page` to your params can help. To access a frontmatter variable called recipes from within a shortcode: 

```go-html-template
{{ .Page.Params.recipes }}
```

If you want to list pages in the current section use `.Page` with something like this:

```go-html-template
<ul>
    {{ range .Page.Parent.Pages }}
    <li><a href="{{ .RelPermalink }}">{{ .Title }}</a> </li>
    {{ end }}
</ul>
```


## Links
1. [Hugo docs](https://gohugo.io/templates/shortcode-templates/) on shortcodes.

