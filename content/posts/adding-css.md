---
title: "Adding CSS"
date: 2020-06-27T11:02:57+01:00
draft: false
tags: ["CSS", "head", resources, assets folder, variables]
summary: "Different methods to link to a CSS file from a Hugo template"
---

There are several ways to link your CSS files in the head of your documents.

Since CSS is in the Static folder, if you create a CSS folder there `static/css/` you can simply add `/css/styles.css` for example to the regular link:


```html
<link rel="stylesheet" href="/css/styles.css">
```

(NB. This will work from the Hugo server but not if you build your site locally and try to access it locally.)

Another way is to pipe it to a relative or absolute url. These will be based on the value of `baseURL` in your config file:

```go-html-template
<link rel="stylesheet" href="{{ "css/styles.css" | relURL }}">
```

A slightly different syntax is used in this [zwbetz article](https://zwbetz.com/make-a-hugo-blog-from-scratch/) which first creates a variable:

```go-html-template
{{ $css := "css/styles.css" | absURL }}
<link rel="stylesheet" href="{{ $css }}">
```

## From the assets directory

You can also keep CSS files in your assets directory. By default this is in the root of your project and called  `/assets` [^1]. 

Unlike the `/static` directory, which is left untouched by Hugo, `/assets` can be processed by Hugo. If you are using SASS then these files are kept here so Hugo can do the processing into regular CSS.

But even if you are using regular CSS if you have multiple CSS files you might want these concatenated into just one file, or minified for instance. So for Hugo to process these files keep them in the `/assets` directory.

When set up to do so Hugo takes your assets, performs whatever operation it is told to, then deposits the processed files in the `/resources` directory. If there is no such directory Hugo will create that, typically these files are placed in `/resources/_gen`.

Here are two simple ways to link an assets CSS file to the `<head>` section of your pages. Note that the path assumes the CSS file is in `/assets/css/` directory. You don't need the `/assets/` bit though as using `resources.Get` knows this and only retrieves files from `/assets`.

```go-html-template
{{ with resources.Get "css/styles.css" }}
  <link rel="stylesheet" href="{{ .RelPermalink }}">
{{ end }}
```

This doesn't do anything to the CSS file.

Another way is:

```go-html-template
{{ $styles := resources.Get "css/styles.css" }}
  <link rel="stylesheet" href="{{ $styles.RelPermalink }}">
```

With this method you can easily have Hugo run some tasks on the file:

```go-html-template
{{ $styles := resources.Get "css/styles.css" }}
{{ $styles = $styles | fingerprint | minify }}
  <link rel="stylesheet" href="{{ $styles.RelPermalink }}">
```


### Multiple CSS files

Here is an example where 2 CSS files are concatenated into one file called `my.css`.

```go-html-template
{{ $cusprops := resources.Get "css/custom-props.css" }}
{{ $styles := resources.Get "css/styles.css" }}
{{ $mycss := slice $cusprops $styles | resources.Concat "css/my.css" }}
{{ $mycss = $mycss | fingerprint }}

<link rel="stylesheet" href="{{ $mycss.RelPermalink }}"> 
```

First the original CSS files have to be grabbed by Hugo. This is done using `resources.Get` which is then stored in a variable.

These 2 variables, `$cusprops` and `$styles` are then placed in array using Hugo's `slice` function. And then this array (or slice in Hugo's parlence) is made into one file using `resources.Concat`. Tell it where to put this new file and it's file name: in this case it will be in a directory `/css` in a file called `my.css`.

With all this stored in another variable, `$mycss` any further operations can be added using Hugo's pipes. In the above examle the file is *fingerprinted* which is just giving the file name a unique name based on a hash of it's contents. If the original files are changed the hash changes and this helps make sure the css file is refreshed by Hugo server.

Finally this is linked to the `<head>` of the HTML page and adding `.RelPermalink` is all that's needed for the path and filename.


## Concatenating all files from the assets directory

The above method works for concatenating specific files from your `assets` directory or subdirectory but what if you want to concatenate all of your CSS files into one.

So assuming you have all your CSS files in an assets subfolder `/assets/css`. To grab all the files we use `resources.Match` rather than `resources.Get`. 

```go-html-template
{{ $cssfiles := resources.Match "css/*.css" }}
```

Next we transform them into a single file:

```go-html-template
{{ $styles := $cssfiles | resources.Concat "styles.css" }}
```

Finally we link them to our document with `.RelPermalink` or `.Permalink`:

```go-html-template
   <link rel="stylesheet" href="{{ $styles.RelPermalink }}">
```



[^1]: To change the default location for the assets directory add the line `assetDir = "/path/to/your-dir"` to your config file.