---
title: "Terminal Commands"
date: 2020-05-12T13:32:54+01:00
draft: false
summary: "Useful Hugo commands from the CLI"
tags: ["CLI", "Hugo commands"]
---

Some useful commands from the command line.

All Hugo stuff has to begin with **hugo** to invoke the program.

## Command

- **hugo version** tells you which version of hugo you're using
- **hugo help** gives some basic help on commands. For more about that command use **hugo [command] --help** or just **-h**
- **hugo server -D** will build the site and start the local server. The `-D` means build the site with the draft pages visible too. Since the default setting in the `/archetypes/default.md` file is to create all posts as drafts this is useful when getting started. If a date in the frontmatter is in the future OR if `publishdate` is set in the future such pages can be published using the `-F` flag. Similarly with expired content, that is where a page has an `expirydate` set in the past, using `-E` will build those pages too.
- **hugo** will build the site and create a `/public` folder to put the site in. **hugo -v** will give the *verbose* output, so more info on the command line.
- **hugo new folder/filename.md** is for creating new content files in the content folder. The content folder `/content/` is assumed so just write the - foldername/filename if you (i.e. your terminal) are in the root of your project.
- **hugo new site *sitename*** creates a project folder with the folders and a couple of files for a new site. To create a site inside the current directory use `hugo new site .` If the directory already has some files, such as a git folder, use the `--force` flag: `hugo new site . --force`.
- **hugo new theme *themename*** creates a new theme with the folders and files needed. 

For the full list see the [Hugo docs](https://gohugo.io/commands/).

Or you can generate a full list of the commands, each in a Hugo compatible markdown file, using `hugo gen doc`. Use the `--dir` to set the output directory for these files. See [the docs](https://gohugo.io/commands/hugo_gen_doc/) for more info.