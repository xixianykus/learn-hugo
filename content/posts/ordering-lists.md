---
title: "Ordering Lists"
date: 2019-12-26T14:36:05Z
draft: false
tags: [sort, "list pages", "range"]
summary: "A look at the code to ordering the lists generated in Hugo, from alphabetical, to date etc.."
---


List pages order content in a certain order. For instance newest first or alphabetically. This can be changed with a number of different parameters.

In this typical list page template look at the line with `range` in. Following the word is `.Pages.ByTitle`. This lists the pages alphabetically based on their title name which is defined in the page’s front matter.

```go
{{ define "main" }}
	<main>
		{{ if or .Title .Content }}
		<div>
			{{ with .Title }}<h1>{{ . }}</h1>{{ end }}
			{{ with .Content }}<div>{{ . }}</div>{{ end }}
		</div>
		{{ end }}

		{{ range .Pages.ByTitle }}
			{{ .Render "summary" }}
		{{ end }}
		{{ partial "pagination.html" . }}
	</main>
{{ partial "sidebar.html" . }}
{{ end }}
```

Other ways to list the content here are:

- By `{{ range .Pages.ByDate }}` which uses the *date* field in the frontmatter.
- `{{ range .Pages.ByPublishDate }}` uses the *publishdate* field in the frontmatter.
- `{{ range .Pages.ByPublishDate.Reverse }}` was a default in this template (the default in this.
- `{{ range .Pages.ByLastmod }}` uses the *lastmod* field in the frontmatter.
- `{{ range .Pages.ByLength }}` lists the pages with the shortest content first.
- `{{ range (.Pages.ByParam "rating") }}` So you can make up your own variable like this. *rating* would be set in the frontmatter. Note the brackets. If no variable exists then it will use the site’s params, `.Site.Params`, instead. If the parameter is nested beneath something else in the frontmatter
  

**Add .Reverse to these to reverse the list order**


## Other Data

The above works when looping through pages but not for other types of data. However you can still order other data types using Hugo's `sort` function. `sort` can work on both slices (arrays) and maps (objects).

`sort` works on slices (arrays) like so:

```go-html-template
{{ $list = $list | sort }}
```

It can also easily be added into a `range` function:

```go-html-template
{{ range sort $list }}
```

This sorts a list alphabetically.

However if you are looping over a map (object) then you can choose which value to order by.

```go-html-template
{{ range sort $list "title" "asc" }}
```

Above we are looping through `$list` and ordering by the value of the `title` key. The `"asc"` means the order is ascending which is usually what you want. If you want the order reversed you'ld use `"desc"` instead.

Read more on [Hugo's `range`](/posts/range/) function.

## Links
1. [Hugo docs on lists](https://gohugo.io/templates/lists/)
2. [Hugo docs on sort](https://gohugo.io/functions/sort/)