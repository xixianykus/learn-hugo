---
title: "Conditionals"
date: 2020-06-11T16:30:05+01:00
draft: true
tags: ["with", "if", "in", "or", "and", "cond"]
summary: "A look at the 5 Hugo conditionals"
---

The 5 main conditionals are: `if, else, with, or` and `and`

There is also `cond` which is like an `if / else` statement (see [below](#cond)).

The two conditional functions `with` and `if` are both extremely useful and very similar. In fact most of the time you can choose either one with a slight modification.

The difference to be aware of is that, as Hugo docs puts it, using `with` "rebinds the context `.` within its scope (just like in range)".

What does this mean? Here's an example.

The function `with` is saying "if *something* exists do *this*".

Imagine that some of our site's content files use Hugo's  `summary` variable in their frontmatter with a summary of the page. But some files don't. We want to tell Hugo to use this variable only if it is available. Using `with` we do it like this:

```go-html-template
{{ with .Params.summary }}
    <p>{{ . }}</p>
{{ end }}
```
We can do the same thing with if but it's slightly longer:

```go-html-template
{{ if .Params.summary }}
    <p>{{ .Params.summary }}</p>
{{ end }}
```

In the first example the paragraph only has a dot, `{{ . }}` whereas when using `if` it is necessary to repeat `{{ .Params.summary }}` in the paragraph tag.

This is because when using `with` everything between the opening `{{ with }}` and closing `{{ end }}` tag is scoped to what is used in `with`, in this case `.Params.summary`. The dot (aka the *context*) is now bound to the summary variable. This means you only need the dot to access the summary variable.

This is the same as the `range` function though it may not seem like it. If `range` is used with `.Pages` you still have to tell Hugo which part of the page object you want: `.Title` or `.Permalink` for instance.

With the `if` statement the dot or context is not *rebound* so you may need to be more explicit in telling Hugo what you want.

<!-- Here's an example using `if` that will not work using `with`.

```go-html-template
{{ if eq .Section "articles" }}
    {{ partial "aside" . }}
{{ end }}
```

This adds a sidebar to pages in the articles section. It's saying: "if the section is called articles use this partial". Because of the change of scope that happens using `with` this only works with `if`. -->

## `not`, `and`, `or` or `in`

You can use `{{ if }}` or `{{ with }}` with `not`, `and` and `or`. Here are some examples:

Note the use of brackets here:

```go-html-template
{{ if not (eq .Section "blog" ) }}
    // some code
{{ end }}
```
If the section is `not` the blog section to this..



```go-html-template
{{ if and (eq .Section "blog" ) (eq .Type "list") }}
    // some code
{{ end }}
```

If the section is the blog section `and ` the page type is list do this..


Here is an example with `or`

```go-html-template
{{ if or (eq .Section "blog" ) (eq .Section "writing") }}
    // some code
{{ end }}
```

If the section is blog section `or` the writing section do this...

The `in` keyword is for use with arrays (slices).

Here it is used to add a logo to pages that are tagged with *Alpine JS*:

```go-html-template
{{ if in .Params.tags "Alpine JS" }}
    <img src="/img/alpinejs.svg" alt="Alpine JS logo">
{{ end }}
```

If *Alpine JS* is `in` the page's list of tags put this image here.

Also, from [the docs](https://gohugo.io/functions/in/)

> In addition, in can also check if a substring exists in a string.  

```go-html-template
{{ if in "this string contains a substring" "substring" }}Substring found!{{ end }}
```



## cond

Another conditional is `cond`. This works like an `if/else` statement. It has 3 parts:

1. A statement that can be true or false
2. What to use when the statement is true
3. What to use when the statement is false

An example:

```go
{{ cond (eq .Title "Ungulates")   "<h3>Donkey Rider</h3>" "<p>See the ungulates page.</p>" | safeHTML }}
```

This frivolous example is saying if the page title is *Ungulates* insert the h3 heading here. But if it's not *Ungulates* put this paragraph in instead. (NB. It's *piped* to `safeHTML` because if it wasn't the tags would just appear as written rather than formatting the text like tags in HTML do.)



## Links
1. [Hugo docs on conditionals](https://gohugo.io/templates/introduction/#conditionals)
2. [Kodify.net](https://kodify.net/hugo/functions/hugo-cond-function/) article on cond. (Also see the [offical page](https://gohugo.io/functions/cond/))
3. [Mike Dane's page on Hugo conditionals](https://www.mikedane.com/static-site-generators/hugo/conditionals/)