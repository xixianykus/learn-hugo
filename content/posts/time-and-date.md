---
title: "Time and Date"
date: 2020-06-03T15:02:52+01:00
tags: ["date", "Format", "Lastmod", "LastChange"]
toc: true
summary: "How to add the time and date variables in Hugo"
style: |
  th { text-align: left;}
---

## Where does the time come from?

Hugo can take the time from several sources.

1. the computer's clock (when a new page is created )[^1]
2. a page's frontmatter 
3. from the Git repo on git enabled sites[^2]. 
   
That time can be stored such as in the frontmatter. Hugo has a number of built in variables and functions to access these date-times.

| Time variable                     | What it does                                                                                 |
| :-------------------------------- | :------------------------------------------------------------------------------------------- |
| `{{ .Date }}`                     | Hugo will take this first from a page's frontmatter `date` variable.                         |
| `{{ .Site.LastChange }}`          | will look at all the pages timestamps and choose the last one. (deprecate)            |
| `{{ .Lastmod }}`                  | takes the time of the last commit in Git repos. [^2]                                         |
| `{{ now.Format "Monday Jan 2" }}` | `now` is a function that takes the time directly from the computer at the time of the build. |



There are multiple ways to add the time and date but the simplest is to use `{{ .Date }}` which is used to generate the time and date when a new page is created.

However the default value is complete but contains more information than required for most use cases and not presented in the most presentable way: `date: 2020-06-03T15:02:52+01:00`

So you need to format that information to get exactly what you want.

To do that you use `{{ .Date.Format }}` along with the time and date information from this very specific time and date: `Mon Jan 2 15:04:05 MST 2006`.

Notice the numbers are in sequence but all different: 

`Monday Jan 2 15:04:05 2006 MST`

1, 2, 3, 4, 5, 6 if we assume 15 means *3* o'clock and Jan is 1 (first month).

An example to put the date in your template pages would be:

```go-html-template
{{ .Date.Format "Monday January 02" }}
```


## The now function

The `now` function simply grabs the time and date of the current time, which means the time when the site was last built. Whereas `{{ .Date }}` grabs the time and date from the page's frontmatter the `now` function grabs it from the PC's clock. 

A good use case is using `{{ now.Year }}` for copyright info. This will simply produce the year in four digits.

For other strings use `{{ now.Format }}` if you want more than just the year. For just the time for instance:

```go-html-template
{{ now.Format "15.04:05" }}
```

This is very useful for sites where the content files themselves aren't changed but the site is changed in other ways. For example the content might be stored in data files. These won't have frontmatter and thus a date on them. Changes to these files will be datestamped in Git yet these won't be stored in `.Site.LastChange` because that is going through the frontmatter dates. 

Care must be taken if your site is being built by an online service which is in a different timezone to yours.

{{< timestamps >}}

## Lastmod

The last modified date works in conjunction with Git. You project must be in a Git repo with Git enabled on your system and in the system path.

Next you have to enable this feature in either the config file or with a flag on the Hugo command line.

In the site's config file add the line:  

```toml
enableGitInfo = "true"
```

To enable this from the command line make sure `--enableGitInfo` is added to your `hugo` command. 

`.GitInfo` is an object containing a bunch of info. This can be accessed using the appropriate [variable](https://gohugo.io/variables/git/) including `.LastMod`


With that done `.Lastmod` can get the date from `.GitInfo.AuthorDate`.


## LastChange

This is used with `.Site` and works as a site wide variable of the most recent date in all the site's frontmatter. If any pages on the site are changed this will be the date given. It's useful for the front page so repeat visitors get an idea of whether the site has added content since their last visit and gives new visitors an idea of how current the site is.

According to the [Hugo docs](https://gohugo.io/variables/site/#site-variables-list):

> a string representing the date/time of the most recent change to your site. This string is based on the date variable in the front matter of your content pages.

However it has since been updated and can now reflect the last modified page based of info in your git repo.

The LastChange variable is simply added anywhere as long as, like `.Lastmod`, it's a git repo and and the line `enableGitInfo` is added to the config file it will pull the date from there.

```go-html-template
<p>Site updated on: {{ .Site.LastChange.Format "02 Jan 2006" }}</p>
```

## Timestamps in custom fields

If you create a timestamp in a custom field Hugo will just see a string value and trying to use `.Format` on it won't work:

```yaml
the_day: 2024-03-31T08:52:36+01:00
```

To make Hugo see this as a date you can use `dateFormat` in your template like so:

```go-html-template
{{ .Params.the_day | dateFormat "Monday 2 January" }}
```

For more see the [Hugo docs page](https://gohugo.io/functions/time/format/) on this.


## More

If you use other variables like `.LastMod` and they're not found (say there's no Git info set) then they will default to different values.

Here is the default configuration for the config file.

```toml
[frontmatter]
date = ["date", "publishDate", "lastmod"]
lastmod = [":git", "lastmod", "date", "publishDate"]
publishDate = ["publishDate", "date"]
expiryDate = ["expiryDate"]
```

## VS Code extension

The [Insert Date String](https://marketplace.visualstudio.com/items?itemName=jsynowiec.vscode-insertdatestring) is a simple  but handy extension that can add the current date in the universal format:  
`2022-05-07T05:27:02+01:00` 

Just press the shortcut keys `Ctrl + Shft + i`. NB. You'll probably have to change the settings slightly to get exactly the right format for Hugo which ends with the `00`.


[^1]: this assumes the `{{ .Date }}` variable is present in the archetypes file for the page. For example:   
`/archetypes/default.md`
[^2]: On sites that have a Git repo you need to set the line `enableGitInfo = "true"` in your site's config file.