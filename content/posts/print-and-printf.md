---
title: "Print and Printf"
date: 2020-10-11T08:30:43+01:00
draft: true
tags: ["printf"]
summary: "A simple explantion of the print and printf functions"
---

## The `print` function

OK according to the Hugo docs:

> Prints the default representation of the given arguments using the standard fmt.Print function.

Not much use as is the next line.

**Syntax:** `print INPUT`

But they do provide a few concrete examples which make things a bit clearer:

```go
{{ print "foo" }} → "foo"
{{ print "foo" "bar" }} → "foobar"
{{ print (slice 1 2 3) }} → [1 2 3]
```

The first example here demonstrates that `print` can be used to add a text string somewhere.

The second example shows it can be used to join two strings together. In fact you can join two or more strings together and you can also use variables too. So `{{ print $var "is my " .Title }}` works perfectly too.

The third example demonstrates `print` can be used on an array too.

So when would you use this? Here is an example from a site I made recently. Some pages required specific style sheets to be linked in the head of the document. The name of the file could be defined in the frontmatter (`cssfile: filename`) and the link in the head would be automatically generated from that.

Here is the code in the head partial file.

```go-html-template
{{ with .Params.cssfile }}
{{ $csslink := print "css/" (.) ".css" }}
<link rel="stylesheet" href="{{ $csslink | relURL }}">
{{ end }}
```



## The `printf` function

The `f` in `printf` stands for format. So the `printf` function both prints and formats a string.

> The function printf() is used to print the message along with the values of variables. [Tutorials Point](https://www.tutorialspoint.com/printf-sprintf-and-fprintf-in-c)

According the Hugo docs `printf`

> Formats a string using the standard fmt.Sprintf function.

Not very useful. The syntax is

```go
printf FORMAT INPUT
```

And it then directs you to a [Go page](https://golang.org/pkg/fmt/).

### A real life example

This example is taken from the Hugo docs section on RSS feeds. Inserting this into the the head section of a template will create a link to your RSS feed.

```go-html-template
{{ range .AlternativeOutputFormats -}}
    {{ printf `<link rel="%s" type="%s" href="%s" title="%s" />` .Rel .MediaType.Type .Permalink $.Site.Title | safeHTML }}
{{ end -}}
```

On this (LearnHugo) site that outputs the following to `<head>` section of each page as:

```html
<link rel="alternate" type="application/rss+xml" href="https://learnhugo.ml/index.xml" title="Learn Hugo" />
```

If you compare this to the Hugo code above you can see that `printf` is combining a list variables (the `%s` which means format as a string) with Hugo variables to be written in each place or each `%s`.

As there is only one alternative output format (the RSS feed) specified the range function creates just one `<link>` tag.

## Links

1. W3 Schools on [print](https://www.w3schools.com/PHP/func_string_print.asp) and [printf](https://www.w3schools.com/PHP/func_string_printf.asp) (these are for php but it's much the same)


