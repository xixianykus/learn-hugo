---
title: "Config Syntax"
date: 2020-09-30T07:52:00+01:00
draft: false
tags: ["config", "TOML"]
summary: "Understanding the TOML syntax for the config file"
---

Hugo's configuration file can be written in JSON, YAML or TOML. The default is TOML and this page looks at the syntax for TOML.

TOML stands for: *Tom's Obvious, Minimal Language*. (Tom, the creator, is Tom Preston-Werner.)

## Basic variables

```toml
string = "String in quotes"
string_literal = 'this/is/in/single/quotes'
number = 53
floating_point_number = 4.6
boolean = false
```

## Lists

Also known as arrays or slices in Hugo, these look like a regular variable on the key side but use square brackets containing different values and separated by commas:

```toml
list1 = ["donkey", "chinaman", "sausage", "fox"]
numbers = [ 12, 7, 88]
nested = [["dog", "doughnut", "whistle"], "pig bladder", ["side train", "lorry", "warship"]]
```

## Dictionaries

Also known as associative arrays, maps, tables (in TOML), dictionaries (in Go) , objects (JSON). Anyway whatever you call them they are lists of variables with their keys, a list of key/value pairs.

They use square brackets for their names:

```toml
[person]
firstname = "Joe"
lastname = "Bloggs"
age = 28
jobtitle = "Web designer"
```

Now because white space and indentation don't mean anything in TOML when there are multiple levels the second and subsequent levels need a way to be identified with the top level title. This is done by simple daisychaining with the dot:

```toml
[restaurants]
 [restaurants.italian]
    name = "Pizza Table"
    street = "Milano Road"
 [restaurants.french]
    name = "La Mer de la France"
    street = "45 Rue de la Mer"
```

NB: Indentation here is for legibility only.

## A Problem

If you look at this and realise that white space and indents don't mean anything then any simple variable placed after a table will automatically belong to the last dictionary / object. In the above that would be `[restaurants.french]`. 

So how do you create a simple variable that's not part of that?

The answer is to simply place it above that and all other *dictionaries* / *tables* / *associative arrays*. This is an important thing to remember when editing your config file in TOML. Dictionaries need to go at the bottom of the config file since the only way to break out of the last one is to create another one with square bracket notation below it.

## Lists of objects

Sometimes you see double square brackets. What are these for? When do you use them?

Here is an example from the config file of this site:

```toml
[menu]

  [[menu.main]]
    identifier = "posts"
    name = "posts"
    pre = "<i class='fas fa-marker'></i>"
    url = "/posts/"
    weight ="20"
  
  [[menu.main]]
    identifier = "tags"
    name = "tags"
    pre = "<i class='far fa-angry'></i>"
    url = "/tags/"
    weight = "100"
```

Double brackets are used when creating a list (or array) of objects.

Let's look at the menu example above. A menu is a list (an array) of items. But each item has multiple properties: name, url, weight, an icon maybe etc. Here, under the `[menu]` section of the config file is the list of objects that make up the menu called `main`.

In this above list you don't even need the first `[menu]` at the top. Each item begins with `[[menu.]]` making clear they all belong to the menu array. This just makes it more organized and readable.

Here is the YAML equivelent of the above which uses a hyphen, `-` , to mark the start of each new object in the list.

```yaml
menu:
  main:
    - identifier: posts
      name: posts
      pre: <i class='fas fa-marker'></i>
      url: /posts/
      weight: '20'
    - identifier: tags
      name: tags
      pre: <i class='far fa-angry'></i>
      url: /tags/
      weight: '100'
```

## White space

As said above white space (spaces and line breaks) don't mean anything in TOML. But sometimes you might want to preserve white space for human readability.

This is done using 3 double quotes to open and close a variable.

```toml
credits = """
Brand font is Museo Moderno by [Omnibus Type](https://www.omnibus-type.com/) licensed under the [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)

Dummy metadata generated using [Mockaroo](https://www.mockaroo.com/)"""
```

See the [config file settings](/snippets/config-settings.md) to check out TOML in action.


## Extra: Using .Site.Params

So this bit is not about TOML specifically and might be moved to somewhere else later.... but anyway

If you create a `[params]` section in your config file you can create custom variables and access these in your templates. For instance if you wanted to switch between the regular production footer of a site and a test version with info for debugging you could add this to your config.toml file:

```toml
[params]
testfooter = true
```
You can then call this in a template using `.Site.Params.testfooter`.

```go-html-template
{{ if .Site.Params.testfooter }}
{{ partial "test-footer" . }}
{{ else }}
{{ partial "footer" }}
{{ end }}
```


  ## Links
  1. [Official guide on GitHub](https://github.com/toml-lang/toml)
  2. [Intro to TOML](https://npf.io/2014/08/intro-to-toml/) is a nicely written page covering everything you need.

