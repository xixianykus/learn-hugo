---
title: "Some Page Variables"
date: 2020-06-12T09:14:06+01:00
draft: true
tags: ["page"]
summary: "Some quick and easy page variables"
---

## Word count

You can add the word count to a page using `{{ .WordCount }}`

## Reading Time

Related and simple. Just add `{{ .ReadingTime }}` to your template/s

## Page weight

You can add the `weight` variable to your frontmatter along with a number (whole number only) then when there's a listing this value is used to order the list. Pages with the lowest numbers appear first in the list.

```yaml
weight: 50
```

## Page kind

To see what kind of page something is add `{{ .Kind }}`


## For the `<head>` section

`hugo.Generator` will generate a meta tag with the version of Hugo used like this:

```html
<meta name="generator" content="Hugo 0.70.0" />
```

This is useful for Hugo to see how many sites on the web are made with their software.



## Links
1. [Full list](https://gohugo.io/variables/page/#page-variables) of Hugo's page variables

