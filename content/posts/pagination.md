---
title: "Pagination"
date: 2020-06-04T09:11:11+01:00
draft: false
tags: ["pagination"]
summary: "Video by Thought Span on Pagination in Hugo"
---

{{< yt BSeYDNhTRB0 >}}

This short and easy to follow video uses code snippets from the presenter's Codepen pages to insert into a partial. So the more difficult parts of creating a pagination are left out.

## Set config file

First decide the maximum number of items you want displayed on each page. You set this in the config file using `paginate`. In a toml file write:

```toml
paginate = 2
```

## Create a partial

If you are using pagination in more than one file, say the home page and the list pages, create it in a partial first.

Copy the code from [the Codepen](https://codepen.io/vikrammehta/pen/VBvqWg?editors=1100) into this partial.

```go-html-template
{{ $pag := $.Paginator }}
{{ if gt $pag.TotalPages 1 }}
<nav aria-label="Page navigation">
    <ul class="pagination">
        {{ if $pag.HasPrev }}
        <li class="page-item"><a href="{{ $pag.Prev.URL }}" rel="prev" class="page-link">&laquo; Prev</a></li>
        {{ end }}
        {{ range $pag.Pagers }}
        {{ if eq . $pag }}
        <li class="page-item active"><a href="{{ .URL }}" class="page-link">{{ .PageNumber }}</a></li>
        {{ else }}
        <li class="page-item"><a href="{{ .URL }}" class="page-link">{{ .PageNumber }}</a></li>
        {{ end }}
        {{ end }}

        {{ if $pag.HasNext }}
        <li class="page-item"><a href="{{ $pag.Next.URL }}" rel="next" class="page-link">Next &raquo;</a></li>
        {{ end }}
    </ul>
</nav>
{{ end }}
```

## In the range functions

Instead of `{{ range .Pages }}` we append `.Paginator`:

```go-html-template
{{ range .Paginator.Page }}
```

After the range function ends add the partial created above. If the file was `pagination.html` it would be:

```go-html-template
{{ partial "pagination" }}
```

This works for smaller sites. However for a large site with hundreds of pages and many pagination pages you can use a different [code snippet](https://codepen.io/vikrammehta/pen/YjydEM).

```go-html-template
{{ $pag := $.Paginator }}
{{ if gt $pag.TotalPages 1 }}
{{ $.Scratch.Set "dot_rendered" false }}
<nav aria-label="page navigation">
    <ul class="pagination">
        <!-- Don't show on 1st and 2nd page -->
        {{ if and (ne $pag.PageNumber 1) (ne $pag.PageNumber 2) }}
        <li class="page-item"><a href="{{ $pag.First.URL }}" rel="first" class="page-link">« First</a></li>
        {{ end }}

        {{ if $pag.HasPrev  }}
        <li class="page-item"><a href="{{ $pag.Prev.URL }}" rel="prev" class="page-link">‹ Prev</a></li>
        {{ end }}

        {{ range $pag.Pagers }}
            {{ if eq . $pag }} <!-- Current Page -->
            <li class="page-item active"><a href="{{ .URL }}" class="page-link">{{ .PageNumber }}</a></li>
            {{ else if and (ge .PageNumber (sub $pag.PageNumber 2)) (le .PageNumber (add $pag.PageNumber 2)) }}
            {{ $.Scratch.Set "dot_rendered" false }} <!-- Render prev 2 page and next 2 pages -->
            <li class="page-item"><a href="{{ .URL }}" class="page-link">{{ .PageNumber }}</a></li>
            {{ else if eq ($.Scratch.Get "dot_rendered") false }} <!-- render skip pages -->
            {{ $.Scratch.Set "dot_rendered" true }}
            <li class="page-item disabled"><a class="page-link">...</a></li>
            {{ end }}
        {{ end }}

        {{ if $pag.HasNext }}
        <li class="page-item"><a href="{{ $pag.Next.URL }}" rel="next" class="page-link">Next ›</a></li>
        {{ end }}

        <!-- Don't show on last and 2nd last page -->
        {{ if and (ne $pag.PageNumber $pag.TotalPages) ((ne $pag.PageNumber (sub $pag.TotalPages 1))) }}
        <li class="page-item"><a href="{{ $pag.Last.URL }}" rel="last" class="page-link">Last »</a></li>
        {{ end }}
    </ul>
</nav>
{{ end }}
```