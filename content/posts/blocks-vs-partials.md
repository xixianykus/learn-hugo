---
title: "Blocks vs Partials"
date: 2020-05-24T07:41:58+01:00
draft: false
tags: ["blocks", "partials"]
summary: "What's the difference between blocks and partials? Why use one and not the other?"
---

So one thing that confused me for a while with Hugo was the difference between blocks and partials. Both are used to insert blocks of code so why use one and not the other? Why not just insert everything as a partial for instance and forget about blocks altogether? Wouldn't that make life simpler?

So here is some typical HTML from a baseof.html file using partials for the header and footer sections but a block for the main content. 

```go-html-template
<html>
    <head>
        <title>{{ .Title }}</title>
    </head>

    <body>
        {{ partial "header.html" . }}

        {{ block "main" }}
        {{ end }}

        {{ partial "footer.html" . }}

    </body>
</html>
```

Couldn't we just use another partial for the *main* section: `{{ partial "main.html" . }}` ?

The difference is this. The partials are unchanging blocks of code so they will be the same on every page. They can contain variables like `{{ .Title }}` or `{{ .Content }}` for example. And that might typically be what you want for every page on the site for a header and footer.

But a site will have different kinds of pages. Hugo assumes even the most basic blog site will have page list page or pages and content pages. That is some pages will be a list of all the blog posts while the content pages will be the actual blog posts themselves.

Using `blocks` instead of `partials` allows for this to change. The `block` of a content page will likely be different from that of a list page. The `blocks` are defined in each page template, one in *single.html* for content and one in *list.html* for list pages. And you can create as many different pages as you like.

The other difference with `blocks` is that they contain an end tag. This means you can customize further by adding extra code inside those tags. Now why would you want to do that when you could just do it in either the single.html or list.html files instead where you define the block? Wouldn't that be pointless?

IMPORTANT: Using HTML outside a main block definition can break the layout, even just a comment.

```go-html-template
<!-- A comment outside the main definition --->

{{ define "main" }}

{{ end }}
```