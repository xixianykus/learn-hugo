---
title: "Template Programming"
date: 2021-04-06T15:55:33+01:00
draft: true
tags: [index, variables, dict, maps]
summary: "Covering the basics of simple programming in Hugo templates"
---

Variables, arrays and objects are the building blocks of programming languages. In Hugo that's true too. There is coverage of these for YAML and TOML elsewhere but here we'll look at how to use and manipulate these things from within our template files (those found in the *layouts* folder) using Hugo's template language.

## Variables

### Simple variable

To declare a variable within any template file use `:=` like this:

```go-html-template
{{ $myVar := "dog" }}
```

To re-assign (change the value) of an existing variable use the `=` sign:

```go-html-template
{{ $myVar = "cat"}}
```

### Slice for arrays (aka slices, lists)

To declare an array or slice use the `slice` function

```go-html-template
{{ $myVar := slice "cat" "dog" "snail" "bear" }}
```

### Dict for objects (aka associative arrays, dictionaries or maps)

To declare an associative array (aka map, dictionary, object) use the `dict` function and alternate the keys with values.

```go-html-template
{{ $myVar := dict "mammal" "dog" "mollusc" "snail" "reptile" "crocodile" }}
```
In YAML this would be written as:

```yaml
myVar:
  mammal: dog
  mollusc: snail
  reptile: crocodile
```

The [Hugo docs](https://gohugo.io/functions/dict/) say:

> `dict` is especially useful for passing more than one value to a partial template.

## Joining variables

You can join two variables or strings or numbers using `add`. But add takes only two arguments so can't join more than two in one go.

```go-html-template
{{ add $var " weird" }}
```

## Editing strings

You can cut the front or end from a string using:

- `strings.TrimLeft`
- `strings.TrimRight`
- `strings.TrimPrefix`
- `strings.TrimSuffix`

[substr](https://learnhugo.ml/posts/substring/) is another function for editing strings.

[trim](https://gohugo.io/functions/trim/) removes the begining and end of a string

## Union - merging arrays

[union](https://gohugo.io/functions/union/) Given two arrays or slices, returns a new array that contains the elements or objects that belong to either or both arrays/slices.

[intersect](https://gohugo.io/functions/intersect/) is similar

## Uniq

The `uniq` function takes out duplicates in an array. As the docs put it:

> Takes in a slice or array and returns a slice with subsequent duplicate elements removed.

You can use it in the normal way or with a pipe:

```go-html-template
{{ uniq (slice 1 2 3 2) }}

{{ slice 1 2 3 2 | uniq }}
```

## Index

The `index` function is used to get values from arrays (aka slices) and objects (aka maps).

In an array each item is numbered from zero (the first item) upwards.

```go-html-template
{{ index $myVar 0 }}
```
will return the first item in the list of `$myVar`


With an object or associative array. To get the value of reptile (above)

```go-html-template
{{ index $myVar "reptile"}}
```
A list can contain objects and those object may contain other objects or lists. For example:

```yaml
- name: Bob Juurg
  address: 23 Lone Road
  contact:
    email: bob@watfers.com
    phone: 0773489123
```

Assume we have a YAML file in the `/Data` folder called `people.yaml`. The hyphen shows this object is part of a list. It has 3 keys (name, address, contact) and contact itself is an object containing two variables (note the indents). To access these  you use the `index` ability to have multiple parameters, each one drilling down further.

So to grab the value of name, and assuming this was the first object in our file we would use:

```go-html-template
{{ index .Site.Data.people 0 "name" }}
```

It works like this: `index` invokes the function, `.Site.Data.people` is the file we want. The `0` is a reference to the fact that this is the first item in the list (or array) and `name` is name of the key we want a value from, *Bob Juurg*.

If you wanted the email address you would use:

```go-html-template
{{ index .Site.Data.people 0 "contact" "email" }}
```



## Equals, less than etc.

There are a number of boolean (true/false) operators that are used compare variables.

These are

- `eq` equal
- `gt` greater than
- `ge` greater than or equal to
- `lt` less than
- `le` less than or equal to
- `ne` not equal to

For instance `{{ ge 10 9 }}` is saying 10 is greater or equal to 9. In this case it will return the value of `true` because 10 is greater than 9.

## Replace

Replace is used in the default.md archetypes file like this:

```go-html-template
title: "{{ replace .Name "-" " " | title }}"
```

The built in `.Name` takes the name of the current file. The replace function is then used to replace any hyphens found with blank spaces. The final `| title` simply takes the result and converts it to *title* case which just means each word begins with a capital letter. You could also us *upper* or *lower* for all uppercase or lowercase letters.




## Other Functions

1. [intersect](https://gohugo.io/functions/intersect/) returns the common values of two arrays or slices in the same order as the first
2. [append](https://gohugo.io/functions/append/) appends one or more values to a slice and returns the resulting slice.
3. [apply](https://gohugo.io/functions/apply/) Given a map, array, or slice, apply returns a new slice with a function applied over it.
4. [delimit](https://gohugo.io/functions/delimit/) Loops through any array, slice, or map and returns a string of all the values separated by a delimiter.
5. [isset](https://gohugo.io/functions/isset/) Returns true if the parameter is set.
6. [merge](https://gohugo.io/functions/merge/) deep merges two maps and returns the resulting map.
7. [split](https://gohugo.io/functions/split/) splits a string into substrings separated by a delimiter



## Links

1. [Accessing the Data folder with index and with](https://harrycresswell.com/articles/passing-data-to-templates-hugo/) by Harry Cresswell
2. [How to use data files in Hugo](https://novelist.xyz/tech/hugo-data-files/) by Peter Chuang
3. [Working with Strings in Hugo](https://kodify.net/hugo-static-site-tutorials/#strings) by Kodify.net
4. [Manipulation: slices and maps](https://www.thenewdynamic.com/article/hugo-data/manipulation-slices-and-maps/) an excellent article by The New Dynamic