---
title: "Generator"
date: 2020-06-01T11:04:50+01:00
draft: false
tags: ["generator", "head section"]
summary: "How to automatically insert the generator meta tag"
---

The HTML meta tag with the `name="generator"` is common to tell anyone interested what tools were used to create a web site.

```hmtl
<meta name="generator" content="Hugo 0.70.0" />
```

To have this automatically added to your pages add this to the `<head>` of your template/s.

```go
{{ hugo.Generator }}
```
## Purpose

If you're site is built online by your web host then this could be useful to check which version of Hugo was used. Also the official Hugo docs highly recommend using this as it allows them to track the use of Hugo. 

All themes hosted by [gohugo.io](https://themes.gohugo.io/) come with this meta tag.

## hugo.Version

Related is the `{{ hugo.Version }}` function which prints out the version of Hugo used. Place this in a template where you want just the version number to appear.

```go-html-template
<p>Hugo version: {{ hugo.Version }}</p>
```

## Links

1. [Full list](https://gohugo.io/functions/hugo/) of uses of the hugo function

