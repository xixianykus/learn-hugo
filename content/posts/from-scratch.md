---
title: "New Site From Scratch"
date: 2020-05-14T07:09:54+01:00
draft: false
toc: true
summary: "List of steps to building a new Hugo site"
tags: [CLI, Hugo new, new site, beginner]
---

What are the minimum steps you need to do to create a hugo site?

## Instant

The quickest way is to use the `hugo new theme` command and set the themes directory to the current one using the dot.

```bash
hugo new theme <sitename> --themesDir .
```

This will create a new directory with the name of `sitename` with everything you need for a site.

Then it's just a case of:

```bash
cd sitename && hugo serve
```

to see your new site at `localhost:1313`.

If you store the sitename in a variable first you can automate the process:

```bash
sitename='my-blog'

hugo new theme $sitename --themesDir . && cd $sitename && hugo serve
```

This works because most themes, including the basic skeleton one that's built into Hugo, are a full site.


## Single page site

A single page can be really quick to set up and can still be worth having if you want to use a CMS for example.

1. `hugo new site <site name>`
2. Create a template file: `touch layouts/index.html` OR `touch layouts/_default/list.html`
3. Add some html and some go-template code to this file.
4. `hugo new _index.md` and add content to this page

The site will now run though you may want to:

5. Edit the `config.toml`[^1] file inc. the `title` and `baseURL`

NB. To create a new site inside the current directory use `hugo new site .`. If the directory already contains some content use the `--force` flag: `hugo new site . --force`.

## Multipage site

The default *new theme* that is created with the  `hugo new theme` command changed quite a lot around Hugo v0.110 and quite a bit more sophisticated than the earlier version. 

1. `hugo new site sitename`. This creates a new folder called *sitename* and fills it with various folders and a couple of files to help start a skeleton site.
2. `cd sitename`
3. `hugo new theme themename` - this is the quickest way to get premade templates for your site in the layouts folder. It will create a folder called *themename* or whatever name you use in the `/themes/` folder. This contains everything needed for a working site.
4. `echo 'theme = "themename"' >> hugo.toml`. This sets the site's theme to that used in the previous step.
5. `cp -r themes/themename/content/* content`. Hugo needs some content to work and while most files in a theme's folder are used by Hugo the content folder is not. This copies those files - some generated lorem ipsum pages - into the main content folder.
6. `hugo server` runs the develpment server with the site.
7. Go to your browser and type `localhost:1313` in the address bar and press enter to see the new site.


## Exploring the code

If you wish to understand how Hugo works you can explore the template code in the `themes/themename/layouts` folder.

## Getting online

The simple way is to type the command `hugo` from your CLI at the root of your project. This will create a new folder called `public` and generate all the HTML and other code for your static site. You can then ftp this to your web host.

A better but more complex way is make your whole project folder a git repository and upload it to a git host like GitLab, GitHub or Bitbucket. Then open an account with a modern static site hosting company like [Netlify](https://netlify.com), [Vercel](https://vercel.com/) or [Cloudflare](https://cloudflare.com/). Your website repository on GitLab, GitHub or Cloudflare can then linked to the hosting company via their account interface. When changes are made to your repo this will trigger a rebuild of your site with the updated changes. 


## Without using a theme

Hugo is most often used for multipage sites and the process is a little longer. You'll need at least two template files, `single.html` and `list.html` though this method creates an `index.html` file as well along with partial for the `head`, `header` and `footer` sections.

You can write your own HTML template files in the `/layouts/` folder from scratch.

For a simple list page add:

```html
<ul>
    {{ range .Pages.ByTitle }}
       <li><a href="{{ .Permalink }}">{{ .Title }}</a></li>
    {{ end }}
</ul>  
```

1. Add a `main.css` file to `/static/css` folder then add a link to style sheet in the `head.html` file:

```css
    <link rel="stylesheet" href="{{ "css/main.css" | relURL }}">
```

7. Add the theme to config.toml: 

```toml
theme = "themename"
```

8. Add some content: `hugo new post/firstpage.md`


[^1]: From Hugo version 0.110.0 you can use a file named `hugo.toml` instead of `config.toml`. Both work though the latter may be phased out in the future.