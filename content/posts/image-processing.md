---
title: "Image Processing"
date: 2022-10-02T08:32:10+01:00
draft: true
tags: [image processing, resources.Get, .Resources.GetMatch, print ]
summary: "How to use Hugo to resize and change images."
---


One of the great things Hugo can do is process your site or apps images. By process it means it can resize, crop or change the format

## How it works

When Hugo *processes* an image it doesn't actually touch the original. Instead it makes a copy of it, performs the operations on that and stores it in a folder at `/resources/_gen`. This image can then be linked into a page using it's `.Permalink` or `.RelPermalink` methods.

## Grabbing the image

To work on an image Hugo needs the source file itself not just the link to the image, in other word the image as a *resource*. It cannot process anything in the `/static` directory so the images have to be either in the `/assets` folder, in a leaf bundle, or anywhere on the web (assessible via `http` or `https`). Depending on which of these 3 place your image resides determines the method Hugo uses to retrieve the image.


| Source of image      | Code to fetch the image                                    | Notes                                 |
| :------------------- | :--------------------------------------------------------- | :------------------------------------ |
| leaf bundle          | `.Resources.GetMatch "images/pic.jpg"`                     | NB. begins with a dot and a capital R |
| assets folder        | `resources.Get "images/pic.jpg"`                           |                                       |
| somewhere on the web | `resources.GetRemote "https://example.com/images/pic.jpg"` |                                       |


Typically this will be saved in a variable. For example:

```go-html-template
{{ $image := "resources.GetRemote "https://example.com/images/pic.jpg" }}
```

You can then perform various operations on your image and have access to variables such as a link to it, width and height and exif data.

To get the link to your new, resized or modified image simply add `.Permalink` or `.RelPermalink` like so:

```go-html-template
<img src="{{ $image.RelPermalink }}">
```

Here is a simple example of grabbing an image named in a frontmatter variable, resizing it then using it in a page template.

```yaml
---
image: some-image.jpg
---
```

Then in a Hugo template file:

```go-html-template
{{ with .Params.image }}
    {{ $asset_image := print "img/" . }}
    {{ $image := resources.Get $asset_image }}
    {{ $image = $image.Resize "300x" }}
    <img src="{{ $image.RelPermalink }}" alt="{{ .Name }}">
{{ end }}
```


The methods you can use on the image are:

| Method    | How it works                                                                                                                                                                                                                             | Example                          |
| :-------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------- |
| `.Resize` | Give it either a width (`x`) or a height (`y`) value in pixels and the image is resized maintaining the aspect ratio                                                                                                                     | `$image.Resize "550x"`           |
| `.Crop`   | This cuts off the sides of an image and you must there for define both width and height. The default crop is done using *smartcrop*, which can be changed by simply stating a different *crop box* positon (see below)                   | `$image.Crop "550 x 290 center"` |
| `.Fill`   | Resizes an image to a fixed width and height. This means, unless the aspect ratio is the same as the original image, some cropping will occur too. Like `.Crop` the anchor point for cropping can be changed from the default of `smart` |                                  |
| `.Fit`    |                                                                                                                                                                                                                                          |                                  |
| `.Filter` | Applies the defined [filter or filters](https://gohugo.io/functions/images/) to the image.                                                                                                                                               |                                  |
| `.Colors` | Gets the colours of an image (see [the docs](https://gohugo.io/content-management/image-processing/#colors))                                                                                                                             |                                  |
| `.Exif`   | Is used to grab EXIF data from jpeg and tiff image files.                                                                                                                                                                                |                                  |


## Setting the anchor

For `.Crop` and `.Fill` the image is processed according a certain point. The default value is `Smart` which tries to identify the subeject of the image and crop accordlingly.

Other options are:

- `Top`
- `TopLeft`
- `TopRight`
- `Center`
- `Bottom`
- `BottomLeft`
- `BottomRight`
- `Right`
- `Left`

## Getting an image from the assets folder

This is an example of how to grab an image from a subfolder of the `/assets/` folder where the image to be used is set in a pages front matter. In this case the variable is named `cover_image`.

So assuming the frontmatter is somthing like:

```yaml
title: Octopus thing
cover_image: red-octopus.jpg
```

In a Hugo template, for instance `/layouts/_default/single.html` the following code will grab the image from the `/assets/` folder, save it in a variable, in this case `$image` and get the link for the image to be used in the HTML `<img>` tag.


```go-html-template
{{ with .Params.cover_image }}
    
    {{ $img_url := print "img/" . }}
        {{ with resources.Get $img_url }}
        {{ $image := . }}
            <img src="{{ $image.RelPermalink }}" alt="Cover image">
        {{ end }}
{{ end }}
```

So the first line makes sure the enclosed code only runs where frontmatter `cover_image` variable is present.

```go-html-template
{{ with .Params.cover_image }}
```

Since the image is in a subfolder of `/assets/` the next line creates the path to the image and stores it in a variable called `$img_url`. The `print` function is used to join the first part of the path, `img/` with the name of the file, represented here by the dot.


```go-html-template
{{ $img_url := print "img/" . }}
```

Another `with` statement checks that the image is available ..
    
```go-html-template
{{ with resources.Get $img_url }}
```    

And if it is stores it in a variable called `$image`:

```go-html-template
{{ $image := . }}
```

Note that this is the image itself, not merely the path to the image. This means the various Hugo image processing options can be performed on `$image`.

Finally the url of the image is used in the HTML `src` attribute using `$image.RelPermalink`.


## Resizing images

If you have a folder of randomly sized images when it comes to resizing there is little point in making images bigger since if that's what's really needed it can be done in the browser with a bit of CSS for example: `width: 100%`. Upscaling an image, unless done with an AI, just results in a larger file size and poor quality image. 

So a typical task might be to only resize those images that are bigger than can fit on the page. Two useful image variables are `.Width` and `.Height` to get the dimensions of the image. 

This simple `if` statement only resizes images greater than 900 pixels in width.

```go-html-template
{{ if gt $image.Width 900 }}
    {{ $image = $image.Resize "900x" }}
{{ end }}
```