---
title: "Pages"
date: 2020-05-30T10:13:25+01:00
draft: true
tags: [""]
summary: "How Hugo identifies different pages"
---

To Hugo everything is a page. However pages can be grouped into subsets in different ways.

These are *section*, *kind* and *type*.

You can access these using the built Hugo variables: `{{ .Section }} {{ .Kind }}` and `{{ .Type }}`.

## Sections

Each folder inside your content folder represents a section. Say you have a website about animals you might have sections on mammals, fish, birds and reptiles for instance. If there is a folder for each of these off the content folder that will be their section. Pages in `content/birds` folder will be in the birds section or, to put it another way, have a section value of *birds*.

## Kinds

There are 5 kinds of page in Hugo. These refer to the type of template used.

The different kinds are: *home, page, section, taxonomy, taxonomyTerm*

**home** is the type of page used by a single page that typically uses the `layouts/index.html` template.

**page** kind is typically used for a blog post or general content page and uses the `_default/single.html` template

**section** kind is the Hugo name for a list page, typically using  `_default/list.html` or `_default/section.html`. You don't need to add these to the `/content` folder as they will be created for each section using the list page template. If you do wish to include some content for these pages they should be kept in the folder for each section and named `_index.md`. 

**taxonomy** kind refers to pages listed from a taxonomy term. For example if you have a tags page that lists all the different tags used that page will be a *taxonomy kind*. and click on one of the tags the page that appears and lists all posts with that tag is a taxonomy term.

**taxonomyTerm** or simply *term* is the page that lists the pages for each *tag*.  These terms are subsets of the above taxonomy. In an animals web site you might use the tags *rodent*, *shark*, *snake* for instance. The would be listed under `/tags/snake/`.


Also note according the Hugo docs:

 > Note that there are also RSS, sitemap, robotsTXT, and 404 kinds, but these are only available during the rendering of each of these respective page’s kind and therefore not available in any of the Pages collections.

## Types

Page types are a custom field that can be set in the frontmatter. However if unset the page type will refer either to the section a page is in or as a *page* if it doesn't have a section (like the site's home page and other top level pages.)