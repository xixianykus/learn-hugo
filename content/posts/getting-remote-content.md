---
title: "Getting Remote Content"
linktitle: " Remote Content"
date: 2023-03-21T07:37:44Z
draft: false
tags: ["JSON", APIs, resources.GetRemote, transform]
summary: "How to get remote content from APIs."
css:
- ".details-body {padding: 2em 1em 2em 1em; columns: 400px 2; background-color: #0003; font-size: 0.8em; border-bottom: #64aa08 5px solid;}"
- ".dinosaur + .dinosaur {margin-top: 0.5em;}"
---

One of the nice things about Hugo is the ease of use in accessing remote API's.

~~Using `getJSON` with just the url allows easy access to this API.~~

The old way, `getJSON`, has been deprecated since Hugo version 0.123.0. Now we use `resources.GetRemote` along with `transform.Unmarshal`.

In the example below the API links to a JSON file called `db.json` which contains an array of objects. The array called `theropods`.

Each object in the array contains the properties: `name`, `where`, `when` and `link` and are accessed using dot notation like so:

```go-html-template
{{ with resources.GetRemote "https://my-json-server.typicode.com/xixianykus/theropods/db" | unmarshal }}
    {{ range .theropods }}
    <div class="dinosaur"><a href="{{ .link }}">{{ .name }} </a>discovered in {{ .where }} in {{ .when }}</div>
    {{ end }}
{{ end }}
```

The loops through the list of objects and produces a list like below.

{{< theropods >}}

The above link is a *fake* REST API but this should work on a *real* one so long as there is either no authentication or authentication can be sent in a url. Currently it's [not possible](https://gohugo.io/templates/data-templates/#authentication-when-using-rest-urls) to access APIs using OAuth. It will also work on a regular json file hosted anywhere. For instance you can create a GitHub or GitLab repo with a json file. Get the URL to the *raw* content and access that too.

## Other formats

The `transform.Unmarshal` can be used with other data serialisation languages besides JSON. These are YAML, TOML, CSV and XML.

## More..

There is more you can do with `transform.Unmarshall`. Check [the docs](https://gohugo.io/functions/transform/unmarshal/) for how send http headers and more.

## Pros and cons

There are pros and cons to accessing API's like this rather than with Javascript.

The pros are:

- ease of use: I find using Hugo's code much simpler and more straightforward than using Javascript's `fetch`.
- more robust: when a user accesses a page they're just getting straight HTML. There's less work to do in the browser and less to go wrong.

The cons are:

- Potentially less up to date: the data is only correct at the last build time. Should data files change between build times the data supplied on the web site won't be updated accordingly until the site is rebuilt. Using JS will get the data at runtime, that is the time someone accesses the page.
- You need to rebuild the site to update the data.
- Less power: using JS gives much more power to manipulate the data.

So choosing between Hugo or JS will depend on your use case. If you need the data to be updated between site builds and/or have more complex need manipulating the data then JS will probably be a better choice. But if you don't then Hugo's ease of use and pure HTML at runtime probably makes that a better choice.

Finally, if you do choose to use Hugo, don't forget there is still m[any ways](/posts/template-programming/) to manipulate the data.