---
title: "Truncate"
date: 2020-06-03T13:05:16+01:00
draft: true
tags: [""]
summary: "How to shorten text and titles"
---

Sometimes things can be longer than we want them.

For instance suppose you have a list of links that use the title variable from the pages frontmatter. Now if some of these are too long for your neat and tidy planned menu it's possible to shorten them using `| truncate`

```go
{{ .Title }}
```

Can be limited to a maximum of however many characters you feel is relevant.

```go
{{ .Title | truncate 10 }}
```

Now the maximum number of characters is 10 but it won't split whole words so it could be less. When text is truncated the viewer also gets 3 dots at the end to show that's its been truncated &hellip;

## With HTML

> If you have a raw string that contains HTML tags you want to remain treated as HTML, you will need to convert the string to HTML using the safeHTML template function before sending the value to truncate. Otherwise, the HTML tags will be escaped when passed through the truncate function.

```go-html-template
{{ "<em>Keep my HTML</em>" | safeHTML | truncate 10 }}
```
produces

```html
<em>Keep my …</em>
```


Links
1. [Hugo docs ref](https://gohugo.io/functions/truncate/)

