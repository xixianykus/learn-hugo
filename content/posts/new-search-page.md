---
title: "New Search Page"
date: 2021-08-25T13:01:00+01:00
draft: false
tags: ["search"]
summary: "How to install a search page on a Hugo site"
---

So I've added a new search page to the site. It seems to work well and was very easy to install. This was created by Tanja Becker and Erhard Maria Klein who put the [code and explanation](https://discourse.gohugo.io/t/a-simple-javascript-based-full-text-search-function/29119) on the Hugo forums.

There are several steps to adding this but they're all very easy to do.

The search function works by creating a JSON page of all your site's content. This is accessed using a Javascript file. The results are listed on the page using a Hugo shortcode with the HTML and script tags on the page. Sounds complicated? Well it really isn't so here goes.

## 1. Create JSON page

Add a new page called index.json to the root of your layouts folder.

Add the following code to the file and that step is done.

```go-html-template
[ {{- $i := 0 -}}
    {{- range where .Site.RegularPages "Section" "ne" "" -}}
       {{- if not .Params.noSearch -}}
          {{- if gt $i 0 }},{{ end -}}
          {"date":"{{ .Date.Unix }}", "url":"{{ .Permalink }}", "title":{{ .Title | jsonify  }}, "summary":
            {{ with .Description}} 
                {{ . | plainify | jsonify }}{{ else }}{{ .Summary | plainify | jsonify }} 
            {{ end }}
            , "content":{{ .Content | plainify | jsonify }},"tags":[ {{- $t := 0 }}{{- range .Param "tags" -}}
            {{ if gt $t 0 }}, {{ end }}
            {{ . | jsonify }}{{ $t = add $t 1 }}{{ end -}} ], "section": {{ .Section | jsonify -}} }
          {{- $i = add $i 1 -}}
       {{- end -}}
    {{- end -}} ]
```

## 2. Configure outputs in your config file

Add these two lines to your config.toml file. (They need to be placed below any one line variable in the file)

```toml
[outputs]
   home = [ "HTML", "JSON", "RSS" ]
```

If your config is a YAML file the code is:

```yaml
outputs:
  home: [HTML, JSON, RSS ]
```

NB. Adding this line to the config file will wipe out the defaults so both HTML and RSS need to be added there.

## 3. Add the Javascript file

Add [the search.js file](/js/search.js) to your `/static/js/` folder.

## 4. Create a new page

Create a new page called `search.md` in your site.

## 5. Create a new shortcode

Create a file called `search.html` to your `/layouts/shortcodes/` folder. (create these if you don't have them already.)

Add the following code to the file:

```go-html-template
<form id="custom-search" name="custom-search" method="post" action="" onsubmit="customSearchResults(); return false;">
    <p>
    <input id="custom-search-field" type="text" name="search" value="" title="Search String" placeholder="SearchString">
    <!-- <input type="submit" value="Suchen"> -->
    </p>
    <!-- <p><em>Search Section:</em><br>
        <input type="checkbox"name="section[]" value="site" checked="checked"> all<br>
        <input type="checkbox" name="section[]" value="post"> Blog<br>
        <input type="checkbox" name="section[]" value="other-section"> Other Section
    </p> -->
    <p>
        <input type="radio" name="option" value="AND" checked="checked"> AND-Search<br>
        <input type="radio" name="option" value="OR"> OR-Search
    </p>
</form>

<div id="custom-search-results"></div>

<script>
// CUSTOM AREA
let params = {
    json_src       : '/index.json', // for multiple sources: comma separated list of JSONarrays
    minlength      : 3,
    defaultsearch  : 'AND',
    sort_date      : 'DESC',
    autocomplete   : 1, // 0: form needs a submit button
    section_search : 0, // 1: needs checkboxes with name="section[]"
    badwords       : 'and,there,the,this,that,you', //ignore this words
    json_wait      : '<p><em>loading...</em></p>',
    json_ready     : '<p><em>Please enter a search term</em></p>',
    extern_icon    : ' (external link)', // marker for external links (optional)
    err_badstring  : '<p>To short!</p>',
    err_noresult   : '<p>Unfortunately no search result. Please try again.</p>',
    err_norequest  : '<p style="text-align: center; color:red;">The full text search is currently not available.</p>',
    err_filefailed : '<p style="text-align: center;color: red;">Search file could not be retrieved.</p>',
    res_one_item   : '<p><em>[CNT] RESULT</em></p>',
    res_more_items : '<p><em>[CNT] RESULTS</em></p>',
    res_out_top    : '<ul>',
    res_out_bottom : '</ul>',
    res_item_tpl   : '<li><a href="[URL]">[TITLE]</a><br>[DATE]:[SUMMARY]<br><em>[SECTION][TAGS]</em></li>',
    add_searchlink : '<p><a href="https://duckduckgo.com/?q=site:yourdomain.com [QUERY]" target="_blank"><i>Not satisfied with the search results? External search via DuckDuckGo ...</i></a></p>'
};

// Translation of section name (optional)
let section_trans = {
    "post" : "Blog",
    // "other-section" : "Other Section"
};

let searchfield_weight = {
    "title"   : 5,
    "tags"    : 5,
    "summary" : 2,
    "content" : 1
};
// CUSTOM AREA END
</script>
<script type="text/javascript" src="/js/search.js"></script>
```

## 6. Add the shortcode to your search page

Just add the new shortcode to your search.md page:

```go-html-template
{{⪡ search ⪢}}
```

And that's it, done. 

## Options

There are a number of options that can be changed in the `search.html` shortcode file.


## Links

1. The [original article](https://discourse.gohugo.io/t/a-simple-javascript-based-full-text-search-function/29119) on the Hugo forums.
2. [An alternative](https://discourse.gohugo.io/t/simple-hugo-search-using-json-file/36964) but similar idea by Vado, also on the Hugo forums.