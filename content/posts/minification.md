---
title: "Minification"
date: 2022-07-30T10:53:37+01:00
draft: true
tags: ["minification"]
summary: "How to have Hugo minify your files"
---

The default options for minification, if not set in your config file, are:

```toml
[minify]
  disableCSS = false
  disableHTML = false
  disableJS = false
  disableJSON = false
  disableSVG = false
  disableXML = false
  minifyOutput = false
```

Looking at this list it appears as if everything will be minified by default. However this is wrong. Firstly anything in the `/static/` folder won't be touched because Hugo doesn't process anything in that folder. So if your CSS and JS is in there it can't be minified by Hugo.

Secondly the HTML files won't be minified either unless you run `hugo` with the `--minify` flag. The above options are there to prevent minification once it's set up elsewhere.

If you want your CSS, JS files minified you first need to put them in the assets directory (`/assets/`). Secondly you need to use Hugo's `resources.Get` command to access them and then run a hugo pipes command on the file. Finally you need to set a link to this new minified file.

With a CSS file stored at `/assets/css/main.css` do the following in the `<head>` section of your template files.

```go-html-template
{{ $style := resources.Get "css/main.css" }}
{{ $style = $style | minify }}
<link rel="stylesheet" href="{{ $style.RelPermalink }}">
```

This file will then be minified so long as the default value of `disableCSS = false` in the config file has not been changed.

To minify your main content files use `hugo --minify` when building your site.

## Other non-Hugo options

Hugo is not the only way to minify your project's files. Other options include:

- VS Code extensions that allow you to minify files manually without using Hugo.
- Some hosting services such as Netlify allow you to specify a minify plugin when building the site.
- npm packages like css-minify.
- Online tools like CSS Minifier where you paste your code in and get back a minified version of it.


See the [SASS page](/posts/sass/) for how.