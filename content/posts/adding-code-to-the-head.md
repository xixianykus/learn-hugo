---
title: "Adding Code to the Head"
date: 2020-07-07T21:38:51+01:00
draft: false
tags: ["frontmatter", "safeHTML", "markdownify", "CSS", "Google fonts", "safeCSS"]
summary: "How to add HTML code to the head section of specific pages from the frontmatter"
---

Some times you want to add a specific piece of code to the head section of some specific pages. For instance maybe there's some page specific javascript or CSS you want to use on that page. Maybe a link to a CDN or cloudbased font file. You can do this from the frontmatter but there's one slight issue.

First think of a variable name and add it to the `<head>` section of your templates. Lets call it *headtag*:

```go
{{ .Params.headtag }}
```

Suppose we want to link an external JQuery file on Google CDN:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
```

We can use our new *headtag* variable in the frontmatter. Using YAML it would look like this:

```yaml
---
headtag: <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
---
```

However doing that will not work. When the *headtag* value is pulled into the document all the *greater than* and *less than* characters are changed into `&gt;` and `&lt;` code.

Hugo can't tell if you want the string to be merely printed to the screen or used as actual HTML. It assumes the former but in this case we want the latter.

Fortunately there's a simple way to tell it what we want. The `safeHTML` function does just that. Back to our template file we just pipe the code to the function like this.

```go
{{ .Params.headtag | safeHTML }}
```

Now the code will be reproduced as it was written and you'll have a link to the JQuery file in the head of that particular page.

## Adding CSS to the head

You can add CSS to the head in a similar way. First add a key to the head with a list of CSS declarations. I'll call this *extracss*. 

NB. These must be wrapped in single quotes:

```yaml
---
extracss:
  - 'h1 { color: blue }'
  - 'h3 ~ p { margin-top: 0; }'
---
```

Next in the head section of your template there are two ways to add this using either `if` or `with`. I'll show both methods here to illustrate the difference in scope between the two.

### Using the `if` function:

```go-html-template
{{ if .Params.extracss }}
<style>

    {{ range .Params.extracss }}
        {{ .  | safeCSS }}
    {{ end }}

</style>
{{ end}}
```

### Using the `with` function

Notice the difference in the range function. Unlike `if` `with` restricts the scope or context to the extracss parameter. So when the range function is invoked we only range over the dot.

```go-html-template hl_line=5
{{ with .Params.extracss }}
<style>

    {{ range . }}
        {{ .  | safeCSS }}
    {{ end }}

</style>
{{ end}}
```

The `if` or `with` line checks first to see if that variable exists. You probably won't need extra CSS on every page so we don't want a pair of empty `<style` tags on every page. If it finds `extracss` in your frontmatter it adds the following code. First it adds an opening style tag. Then it works through the list of `extracss`. It outputs these into CSS format. Normally it would fail with the special characters like the curly brackets and you'll just see `ZgotmplZ` in place of your CSS. The range function ends when it's gone through the list and the final style tag is *printed* before the if function ends.



## Using markdownify

One problem appears to be rendering of the `+` symbol from inside templates. Despite numurous [text functions](/text-functions/) this symbol is always escaped, or turned into HTML characters replacing the `+` with `&2b` or something else. This [appears to be a bug](https://discourse.gohugo.io/t/how-to-prevent-hugo-from-parsing-to-43/22214/5) not in Hugo but in the Go language itself.

One way around this was to use the `markdownify` function. When converting markdown into HTML the plus symbol remains unaffected and `markdownify` uses this.

The markdownify function takes markdown and changes it to simple text. Some strings are complex with lots of special characters. For instance a link to google fonts:

`https://fonts.googleapis.com/css?family=Source+Sans+Pro:400|Open+Sans:700`

The easiest way to do this [according to BEM](https://discourse.gohugo.io/t/render-html-link-with-yaml/1271/2?u=horbes), Hugo creator, is to use markdownify.

First you convert this long url to a markdown link using square brackets for the link then just something in the brackets:

`[https://somelink.com/family=Source+Sans:400|Josefine:etc](uh)`

Then you add this to your YAML frontmatter with a key and surround the string with *single* quotes.

```yaml
paramname: '[https://somelink.com/family=Source+Sans:400|Josefine:etc](ugh)'
---
```

In your template file, it would the `<head>` section for this, you use the markdownify function to convert this markdown link to plain text:

```go-html-template
<link href="{{ .Params.paramname | markdownify }}" rel="stylesheet">
```

Now I didn't find this worked. Another solution is `safeURL` which would be the same as above but using `safeURL` instead of markdownify (and you wouldn't need to make the link look like a markdown file with square brackets). But again this didn't work for me.

## Google fonts

The solution I found that worked was simple though not obvious. Instead of trying to embed the Google link directly in the head of the document I used an `@import` call from an embeded stylesheet.

First in the head of the Hugo template file:

```go-html-template
{{ if .Params.headcss }}
  <style>

  {{ range .Params.headcss }}
      {{ .  | safeCSS }}
  {{ end }}
  
  </style>
{{ end}}
```

This will list out the value of the array or list of `headcss`. This array is in the frontmatter:

```yaml
headcss:
- '@import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:400|Open+Sans:700);'
- 'h1 { color: green; font-family: georgia; }'
- 'body { background: red; }'
- '.anotherStyle { margin: 0; }'
```

Because the `@import` is CSS and perhaps because I used `url(. . . )` this worked fine.

## Text functions

The functions used in Hugo to modify (or not) text strings are:

- [anchorize](https://gohugo.io/functions/anchorize/) - makes everything lowercase and replaces spaces, dots, apostophes into hyphens. Ideal for links in other words.
- [emojify](https://gohugo.io/functions/emojify/) - "For emojis in content files, set `enableEmoji = true` in your site’s configuration."
- html - can be used to make sure HTML is rendered as HTML.
- [htmlEscape](https://gohugo.io/functions/htmlescape/) - converts an ampersand to `&amp;`. It escapes only: <, >, &, ' and ".
- [htmlUnescape](https://gohugo.io/functions/htmlunescape/) - reverses the above so `&amp` will become & etc.
- [lower](https://gohugo.io/functions/lower/) - changes everything to lowercase
- [humanize](https://gohugo.io/functions/humanize/) - replaces hyphens between word with spaces and capitalizes the first letter of the first word. On numbers adds an st / nd / rd  or th (eg. 105th)
- [markdownify](https://gohugo.io/functions/markdownify/)
- [plainify](https://gohugo.io/functions/plainify/) - strips any HTML and returns plain text.
- [pluralize](https://gohugo.io/functions/pluralize/) - pluralizes a word according to english rules.
- [safeCSS](https://gohugo.io/functions/safecss/) - declares a string is "safe" CSS. So CSS strings won't have `{` and `}` stripped.
- [safeHTML](https://gohugo.io/functions/safehtml/) - same as above but for HTML. Used in a TOML or YAML variable you still need to escape the `"` by putting a backslash just in front of them, eg.: `<a href=\"https://creativecommons.org/licenses/by/4.0/\">`
- [safeHTMLAttr](https://gohugo.io/functions/safehtmlattr/) - for HTML attributes. Useful in link variables like `.URL`, sometimes with `printf`.
- [safeJS](https://gohugo.io/functions/safejs/) - declares the string is "safe" javascript.
- [safeURL](https://gohugo.io/functions/safeurl/) - `http:`, `https:` and `mailto:` are considered safe by Go templates. But others like `ftp:`, `irc:` etc. require this to work.
- [singularize](https://gohugo.io/functions/singularize/) - opposite of pluralize.
- [title](https://gohugo.io/functions/title/) - converts to title case. Can be [configured](https://gohugo.io/getting-started/configuration/#configure-title-case).
- [upper](https://gohugo.io/functions/upper/) - converts all letters to uppercase.
- [urlize](https://gohugo.io/functions/urlize/) - useful in urls or partial urls. Converts spaces to hyphens. Protects forward slashes. See tags [example](/posts/how-to-add-tags/).

## Links

1. [Hugo forum discussion](https://discourse.gohugo.io/t/how-do-i-add-css-js-to-head-from-partial-content-template/26382/21)