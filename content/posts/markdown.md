---
title: "Markdown"
date: 2022-08-28T09:47:15+01:00
draft: true
tags: ["markdown", render hooks]
summary: "Info Hugo's markdown processing"
---

There are many options with Hugo for changing markdown files.

The markup section of the config file is where many options can be changed.

## Render Hooks

The markdown format is very limited compared to HTML. When you add an image in markdown like say:

```markdown
![this is the alt text](/path/to/my-image.jpg "And some text for the title attribute")
```

you only have 3 options, the alt text, the path to the image and title text.

```html
<img src="/path/to/my-image.jpg" alt="this is the alt text" title="And some text for the title attribute" />
```

This limited set of option can be changed with Hugo's render hooks. These allow you to change the way certain markdown blocks of code are rendered in HTML. 

These are:

- headings
- images
- links 
- code blocks (ie. syntax highlighted blocks)

To create render hooks first create a subfolder called `_markup` in your specific layouts folder. For instance `/layouts/_default/_markup/`.

Create a file or files for each type of HTML block you wish to change using one the following names:

- `render-heading.html`
- `render-image.html`
- `render-link.html`
- `render-codeblock.html`

Next write the HTML code you want in the specific file. 

A simple example is if we want to add the `loading="lazy"` attribute to all our images. In the `render-image` file we first recreate how an image from markdown is rendered. It can take up to 3 variables for the 3 attributes: `src`, `alt` and `title`. So our new image needs to incorporate these using Hugo's built in variables: `.Destination`, `.Text` and `.Title`. Here is an example from the [Hugo docs](https://gohugo.io/templates/render-hooks/#image-markdown-example).

```go-html-template
<p class="md__image">
  <img src="{{ .Destination | safeURL }}" alt="{{ .Text }}" {{ with .Title}} title="{{ . }}"{{ end }} />
</p>
```

You can now add the `loading="lazy"` attribute directly to this HTML and all images will have the attribute.

```go-html-template
<p class="md__image">
  <img src="{{ .Destination | safeURL }}" alt="{{ .Text }}" {{ with .Title}} title="{{ . }}"{{ end }} loading="lazy" />
</p>
```

### Incorporating attributes

In the post on [adding HTML attributes to our markdown](/posts/add-attributes-in-markdown/) we saw how by amending the config file with

```toml
[markup]
  [markup.goldmark.parser.attribute]
    block = true
```

we can add HTML attributes to block level elements. Just start a new line and inside curly braces add either

```markdown
last part of a paragraph.
{.class-name #id-name }
```

IF you want to be able to add attributes to your markdown in this way AND you're using *render hooks* then you need to add the `.Attributes` variable to render hook HTML. This is a little more complicated than the other variable because it's an object or map consisting of potentially multiple variables with different key names.

The way to incorporate `.Attributes` into a render hook file is like so:

```go-html-template
{{ range $key, $val := .Attributes }}
    {{ $key | safeHTMLAttr }}={{ $val }}
{{ end }}
```

How does it work? First the range function loops over the `.Attributes` object, if it exists. It stores the keys in a variable I've named `$key` and the values in a variable I called `$val`. The second line uses the variables to write the attribute in the HTML. The `$key` needs to be piped to `safeHTMLAttr` to declare it safe to use in the HTML. 




### Smartypants

[Smartypants](https://daringfireball.net/projects/smartypants/) is another useful feature that automatically changes normal 'quotes' into "smart quotes". This feature is turned on by default (on line 10: `typographer = true`)

```toml {linenos=false}
[markup]
  [markup.goldmark]
    [markup.goldmark.extensions]
      definitionList = true
      footnote = true
      linkify = true
      strikethrough = true
      table = true
      taskList = true
      typographer = true
```
