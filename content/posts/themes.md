---
title: "Themes"
date: 2021-11-17T07:59:24Z
draft: true
tags: ["Themes"]
summary: "How to add themes in Hugo"
---


To add a theme to a Hugo site you create a themes folder to the route of your site. This can contain multiple themes each contained in a subfolder of the `/themes` directory with the themes name. To use the theme you add a line to your config file:

```toml
theme = "ThemeName"
```

So the basics are simple but how do you get your theme?

### From a zip file

You can download the the theme as a zip file and unzip it into your `/themes` folder

### 2. Git clone

To clone the theme using Git from the root of your project to clone into the themes directory use: 

```bash
git clone https://github.com/Vimux/blank.git themes
```

NB. The project does not need to be a git repository to use `git clone`.

### 3. Git submodules

If you are using Netlify to host your site [they say](https://docs.netlify.com/configure-builds/common-configurations/hugo/#hugo-themes) you cannot use `git clone` for your theme and instead must use [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

Like `git clone` `git submodule` will clone a repo to your project. However it also adds the repo for the theme inside your project repo at a certain commit.

```bash
cd YOUR_PROJECT_DIRECTORY
git init
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
```

### 4. Hugo modules

Not everyone thinks git submodules are a good thing and [some](https://chrisshort.net/from-git-submodule-to-hugo-modules-using-netlify/) recommend using Hugo modules instead.

However Hugo modules are fairly new and many themes do not support their use. If the theme's minimum Hugo version requirement is 0.55 or above you can probably use Hugo modules to use the theme.

You will also need [Go version 1.12](https://golang.org/dl/) (or higher) installed and may need to set this in the environment variables of a netlify.toml file (in the root of your project).

There's a bit more to it than this. See the [Chris Short post](https://chrisshort.net/from-git-submodule-to-hugo-modules-using-netlify/) on how to do this and read through the [official docs](https://gohugo.io/hugo-modules/) on modules too


## Links

1. [Chris Short on Hugo modules](https://chrisshort.net/from-git-submodule-to-hugo-modules-using-netlify/)
2. [Netlify on Git submodules for Hugo themes](https://docs.netlify.com/configure-builds/common-configurations/hugo/#hugo-themes)
3. [Official Git docs on submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
4. [Hugo docs on Hugo modules](https://gohugo.io/hugo-modules/)