---
title: "Add HTML Attributes to Markdown"
linktitle: "Add HTML Attributes"
date: 2021-09-03T19:50:00+01:00
draft: false
tags: [config, markdown]
summary: "How you can add classes and ids to elements in Markdown"
extlinks:
- ['https://gohugo.io/getting-started/configuration-markup#goldmark', Markdown configuration ]
css:
- ".green {color: green}"
---


### Class value { .green }

So a new and useful feature in Hugo is the ability to add id and class attributes to HTML elements in markdown files.
{.green title="hello"}

Now this is a kind of test page since I couldn't get this to work the way it's described. On headings it was simply a case of adding your class name in a single pair of curlies on the same line as the heading.

```markdown
### Class value { .green }
```

And or other HTML elements (lists, tables etc.) the curlies are meant to go on the next line down. But here it would not work for me.

```markdown
1. item 1
2. item 2
3. item 3
{ .green }
```

Well the fix was in the docs. Just gotta read them through. It turns out the ability to do this is set in the config file under `[markup]`.

The default is:

```toml
[markup]
  [markup.goldmark.parser.attribute]
    block = false
    title = true
```

So all you have to do is copy that under the `markup` section of your config (create it if you need to) and then change the line begining with `block` to `true`

```toml
block = true
```

This is something I'll value a lot and will be adding to config files everywhere. Maybe this will be a default value soon. This only became available in Hugo 0.81 and I'm only 0.86 now as I write.

## More { style="background-color: #ddd" }

But it's not just class and id attributes you can add. You can add any regular attribute the same way: `{title="Boffdong"}` or, as the heading above, `{ style="background-color: #ddd" }`.

BUT there are some limitations. The docs say for now at least you can't add attributes to list items or table cells or rows. I don't think you can add them to inline elements either. I tried to add to some bold text but ... not happening.


### Auto ID tags to headings

Another setting that is switched on by default is auto heading ID's. This adds an id tag to every heading. The default style is `github` which seems to be a kebab case version of the text of your heading.

Thus my `h3` heading above looks like:

```html
<h3 id="auto-id-tags-to-headings">Auto ID tags to headings</h3>
```

The config default is:

```toml
[markup.goldmark.parser]
   autoHeadingID = true
   autoHeadingIDType = "github"
```



## Adding CSS to the head of a page

So a style sheet is usually in a seperate file but you can have one in the `<head>` section of individual pages too. To add CSS rules to the `<head>` section of a page first add this to your Hugo head partial template. (typically in `/layouts/partials/head.html`)

Assuming you call your frontmatter variable `css` (call it what you want though):

```go-html-template
{{ with .Params.css }}
   <style>
      {{ range . }}
      	{{ . | safeCSS }}
      {{ end }}
   </style>
{{ end }}
```

So the `with` bit is saying if the page has frontmatter variable called `css` do this. First add `<style>` tags. Then inside those go through each variable listed. The dot say put each one here and pipe it to safe CSS (which I think means preserve the curly brackets mostly).

The frontmatter looks like this:

```yaml
css:
- ".green {color: green}"
```

Just one rule but you can add as many as you want and the range function will output each to a new line in your style tags in `<head>` section of your page.