---
title: "SASS in Hugo"
date: 2020-05-27T14:12:15+01:00
draft: false
tags: ["sass", "css"]
summary: "How to set up Hugo when using SASS"
---

This tutorial is in parts. The first part shows how to use Hugo pipes with a regular CSS file and how we can fingerprint and minify that file.

The second part is the small step from there to use SASS files instead of regular CSS.

**NB. To use SASS you must be using the extended version of Hugo.**

## Overview

To accomplish these we need to do 4 things:

1. Create a folder called *assets* in the project root directory.
2. Create a custom variable in the `<head>` section of the document
3. Add a pipe command/s next in the `<head>` section.
4. Change the stylesheet link in the `<head>` to that of our new variable.



## Using Hugo pipes

So first we'll see how to use Hugo pipes with a regular CSS file.

1. Create a new folder from the root of your project called *assets*. (If you prefer a different folder you need to set in the site config using `assetDir = "folderName"`)
2. Add a subfolder, perhaps named *css*, and move your css file or create a new one there, ie. move the CSS file from *static/css/* to *assets/css/*.
3. Create a new variable (called *style* here but call it what you want) to the `<head>` section of your template file. Variables that are not built in to Hugo always begin with a dollar sign, $. This will get something (a file) from somewhere (our folder called css) in our *assets* folder.
   ```go
   {{ $style := resources.Get "css/main.css" }}
   ```
4. For the link use:
```go-html-template
<link rel="stylesheet" href="{{ $style.Permalink }}">
```
5. Next add a fingerprint. The fingerprint function takes a hash of the file and adds it to the filename. If any changes are made to the file the filename will change too, which prevents the browser using a cached version of the css file.
```go-html-template
{{ $style := resources.Get "css/main.css" }}
{{ $style = $style | fingerprint }}
<link rel="stylesheet" href="{{ $style.Permalink }}">
```
1. You can add easily add further pipes to this line. For instance *minify*
```go-html-template
{{ $style = $style | minify | fingerprint }}
```
If you're using the live server this will only minify the cached server version of the file. It will add a *.min* to the file name too.

The file is now called `main.min.4a94ae0697fad1ed0cb224288a8674e319fd1b7e0e3a96adf60912682a77de81.css`

## SASS/SCSS

We're now in a good position to use SASS files. The key pipe here is `toCSS`. You add this to the pipes like so:

```go-html-template
{{ $style = $style | toCSS | fingerprint }}
```
This works if the input file is a SASS file of course.

```go-html-template
{{ $style := resources.Get "sass/main.scss" }}
```

### Hugo docs

Hugo docs shows an alternative way using two new variables:

```go-html-template
{{ $sass := resources.Get "sass/main.scss" }}
{{ $style := $sass | resources.ToCSS }}
```

or in the section on pipes:

```go-html-template
{{ $style := resources.Get "sass/main.scss" | toCSS | minify | fingerprint }}
<link rel="stylesheet" href="{{ $style.Permalink }}">
```

1. If there's any error in the filename or folder path everything fails.
2. You may need to stop and restart the the Hugo server: `Ctrl + C` and `Hugo Server -D` (The D means build drafts))

Here's a video on this:

{{< yt NKLwuZIkReg >}}

## Lib SASS and Dart SASS

The version of SASS that comes with Hugo is called Lib SASS and is longer being maintained so is quickly becoming inadequate for modern CSS. You can still use Hugo with an actively maintained version of SASS, Dart SASS, however. To do so requires installing Dart SASS, adding an extra line of code in Hugo's templates to specify using Dart rather than Libsass.

1. Local development
   1. install Dart SASS.
   2. add it to Hugo templating code
2. Making sure it is available on your server, assuming your site is built by the server.


To install on your machine from Windows with chocolatey package manager:

```bash
choco install sass
```

For other OS's see [the docs](https://gohugo.io/functions/resources/tocss/#installing-in-a-development-environment).

You can check that Dart SASS is available by running `hugo env` from the command line. Here is an example of the output:

```bash
hugo v0.120.4-f11bca5fec2ebb3a02727fb2a5cfb08da96fd9df+extended windows/amd64 BuildDate=2023-11-08T11:18:07Z VendorInfo=gohugoio
GOOS="windows"
GOARCH="amd64"
GOVERSION="go1.21.4"
github.com/sass/libsass="3.6.5"
github.com/webmproject/libwebp="v1.2.4"
github.com/sass/dart-sass/protocol="2.3.0"
github.com/sass/dart-sass/compiler="1.69.5"
github.com/sass/dart-sass/implementation="1.69.5"
```

The last 3 lines show that Dart SASS is available for Hugo to use.


With the above verified next we create an options *map* in our template file then incorporate it into the compile process:


```go-html-template
{{ with resources.Get "scss/main.scss" }}
{{ $opts := dict "transpiler" "dartsass" "targetPath" "css/main.css" }}
  {{ with . | toCSS $opts }}
  <link rel="stylesheet" href="{{ .RelPermalink }}">
  {{ end }}
{{ end }}
```

If you fingerprint the file you can also add an *integrity* attribute to the `<link>` element:

```go-html-template
{{ $opts := dict "transpiler" "dartsass" "targetPath" "css/main.css" }}
{{ with resources.Get "scss/main.scss" | toCSS $opts | minify | fingerprint "md5" }}
  <link rel="stylesheet" href="{{ .RelPermalink }}" integrity="{{ .Data.Integrity }}" crossorigin="anonymous">
{{ end }}
```

Note that fingerprint can take the values of: `sha256`, `sha384`, `sha512` and `md5`. If unspecified the default `sha256` is used. More on this in [the docs](https://gohugo.io/hugo-pipes/fingerprint/).

> Any so processed asset will bear a .Data.Integrity property containing an integrity string, which is made up of the name of the hash algorithm, one hyphen and the base64-encoded hash sum.


Here is the code for using Dart SASS on Netlify. Add this to the `netlify.toml` file which should be created in the root of your Hugo project:


```toml
[build.environment]
HUGO_VERSION = "0.120.4"
DART_SASS_VERSION = "1.69.5"
TZ = "Europe/London"

[build]
publish = "public"
command = """\
  curl -LJO https://github.com/sass/dart-sass/releases/download/${DART_SASS_VERSION}/dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz && \
  tar -xf dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz && \
  rm dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz && \
  export PATH=/opt/build/repo/dart-sass:$PATH && \
  hugo --gc \
  """
  ```

The above is based on a couple of articles I found on this topic:

- [Setting Up Dart SASS in your Hugo project](https://www.brycewray.com/posts/2022/03/using-dart-sass-hugo/)
- [Using the Dart Sass transpiler](https://discourse.gohugo.io/t/using-the-dart-sass-transpiler/41878) a post on the Hugo forums.

There is a full list of TZ (time zone) codes on [Wikipedia](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).


## Links

1. [Full Stack Digital](https://blog.fullstackdigital.com/how-to-cache-bust-and-concatenate-js-and-sass-files-with-hugo-in-2018-9266fd3c411e) on SASS in Hugo
2. [Use Hugo Template variables in SCSS](https://blog.fullstackdigital.com/how-to-use-hugo-template-variables-in-scss-files-in-2018-b8a834accce) another good Full Stack Digital article.
3. [Use Hugo template variables in SCSS files (in 2018)](https://blog.fullstackdigital.com/how-to-use-hugo-template-variables-in-scss-files-in-2018-b8a834accce) Another Full Stack Digital article on using Hugo template code inside the main.scss filehugo v0.111.3-5d4eb5154e1fed125ca8e9b5a0315c4180dab192+extended windows/amd64 BuildDate=2023-03-12T11:40:50Z VendorInfo=gohugoio
