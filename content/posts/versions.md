---
title: "Versions"
date: 2020-06-22T07:45:32+01:00
draft: false
tags: ["netlify", "config", "version"]
summary: "How to get your site built with the specific version of Hugo"
---

You a probably using one of the latest versions of Hugo on your own computer. But how do you ensure that your site will be built with the same version.

The answer will vary depending on where your site is hosted. Netlify is probably the most popular choice for static site hosting so here's how to do that with Netlify.

## Netlify

There are several ways to tell Netlify which version of Hugo you'd like your site built with. I just use the first method, via their website, to specify the version. 

WARNING: Probably best not to combine these methods. When I tried having all 3 methods at once my site didn't update at all. When I deleted the two file based methods it worked straightaway.

### 1. Via the Netlify web site

Log in to your Netlify account. Find your web site and navigate to *Build and Deploy*, *Environment* and then *Environment Variables*. Here click on *Edit variables* button and then *New variable*. You are presented with two boxes, *key* on the left and *value* on the right.

In the LH, key box type: `HUGO_VERSION`. In the RH *value* box type the version number like this: `0.70.0`. Click save and you're done. The next time your site is rebuilt it should be built with that version of Hugo. Obviously make sure the version number you use actually exists. There is a list of some of the versions below.

See [Netlify docs](https://docs.netlify.com/configure-builds/common-configurations/#hugo)

### 2. Via a Netlify config file

There are several ways reported on how to do this and I haven't tested them so not sure which work and which don't.

The basic is to add this line to either a new file called netlify.toml or your config file, both in the root of your project.

```toml
[context.production.environment]
  HUGO_VERSION = "0.100.2"
```

According the Netlify documentation create a `netlify.toml` file and add this line to that. Elsewhere I've seen you can just add it to your `config.toml` file.

Here is an example of other possible config options in the `netlify.toml` file. [This](https://github.com/bep/docuapi/blob/master/netlify.toml) is from Bep, one of the main Hugo devs.

```toml
[build]
publish = "exampleSite/public"
command = "hugo --gc -s exampleSite --minify"

[context.production.environment]
HUGO_VERSION = "0.100.2"
HUGO_ENV = "production"
HUGO_ENABLEGITINFO = "true"

[context.deploy-preview]
command = "hugo -s exampleSite -D -F -b $DEPLOY_PRIME_URL"

[context.deploy-preview.environment]
HUGO_VERSION = "0.62.2"

[context.branch-deploy]
command = "hugo -s exampleSite --gc -b $DEPLOY_PRIME_URL"

[context.branch-deploy.environment]
HUGO_VERSION = "0.62.2"
```

NB. The `context.branch-deploy` settings are used to configure another branch.

## Via the site config file

So this method may or may not work, perhaps depending on where your site is hosted. These lines are added to your `config.toml` file.

```toml
[module]
  [module.hugoVersion]
    extended = true
    max = "0.72.0"
    min = "0.70.0"
```

## What about `min_version` ?

There is also a line in the config file that sometimes comes in a theme's config file:

```toml
min_version = "0.20"
```

As far as I understand this is simply stating the minimum version of Hugo that can be used with the theme. It might be more for the person using the theme rather than a build command for software.


## How to check which version was used

An easy way to see which version was used to build your site is to use a function to tell you. `hugo.Generator` is what you need. Simply add this line to the head section of your template/s like this:

```go-html-template
{{ hugo.Generator }}
```

Your head may look like this:

```go-html-template
<head>
    {{ hugo.Generator }}
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>My Website</title>	
	<link rel="stylesheet" href="/css/style.css">
</head>
```

This will produce a metatag in the source code of your website pages:

```html
<meta name="generator" content="Hugo 0.101.0" />
```


### Hugo versions

Some of the version numbers that exist. Use for matching with online build services like Netlify.

- 0.32
- 0.41
- 0.46
- 0.55
- 0.56
- 0.58
- 0.62
- 0.63.1
- 0.63.2
- 0.65.3
- 0.67
- 0.67.1
- 0.68.2
- 0.68.3
- 0.69
- 0.69.1
- 0.70
- 0.71
- 0.71.1
- 0.72
- 0.72