---
title: "Arrays"
date: 2020-06-03T11:28:12+01:00
draft: false
tags: [dictionaries, "variables", "params", "index", slice, dict]
summary: "Using arrays in Hugo"
---

With everything in Hugo there are several languages at work. <abbr title="YAML Ain't Markup Language">YAML</abbr> is used for the frontmatter, <abbr title="Tom’s Own Minimal Language">TOML</abbr> for the config file and Go for the templating language.

Arrays are simply a kind of variable that takes multiple values. You create a new array like a variable by:

1. adding a key/values pair somewhere. In YAML frontmatter for instance.
2. adding the Hugo code to one or more of your template files.

In TOML an array looks like this:

```toml
fruit = ["oranges", "bananas", "apples", "pears"]
```

In YAML that could be either:

```yaml
newVariable: ["civet", "owl", "mongoose", "chair"]
```

or

```yaml
newVariable:
  - civet
  - owl
  - mongoose
  - chair
```

In a template file using Go an array is created using the word `slice`:

```go
{{ $newVariable := slice "civet" "owl" "mongoose" "chair" }}
```

## Associative Arrays or dictionaries

These are where each value of an array also has it's own key. They are also known as maps, dictionaries, or hash tables.

Here is an example in YAML (the spaces are important).

```yaml
link:
  url: https://gohugo.io
  name: Hugo
  description: The official Hugo website
```

In TOML these are called tables and look like this (spaces don't matter):

```toml
[link]
 url = "https://gohugo.io"
 name = "Hugo"
 description = "The official Hugo website"
```

In Go the word _dictionary_ is used and they're declared like this with odd words as the keys and even as the values:

```go
{{ $link := dict "url" "https://gohugo.io" "name" "Hugo" "description" "The official Hugo website" }}
```

This could then be arranged on the page using:

```go-html-template
<p> {{ range $key, $val := $link }}
   <b>{{ $key }}:</b> {{ $val }}<br>
{{ end }}</p>
```

Which would output to:


**description:** The official Hugo website  
**name:** Hugo  
**url:** https://gohugo.io


You can create a dictionary containing and array using brackets:

```go-html-template
{{ $animals := dict "mammals" (slice "elephants" "sheep" "dogs") "reptiles" (slice "snakes" "lizards" "crocodilians") "birds" (slice "ratites" "other") }}
```

## Accessing the arrays

You can access this in a template the same way as a variable `{{ .Params.fruit }}` but that produces a linear list in square brackets: `["oranges", "bananas", "apples", "pears"]`

A better way is using the `range` function:

```go-html-template
{{ range .Params.fruit }}
  <li>{{ . }}</li>
{{ end }}
```

The range function will go through the array and every time it finds one it will write out the line between _range_ and _end_. The value will take the place of the dot in the curly brackets.

### Accessing a specific part of the array

To return just one member of an array you can do so by figuring out it's position. With _oranges, bananas, apples, pears_ the third item, apples, is indexed 2 because the count begins with zero not 1.

Rather than using `range` we use `with` like this:

```go-html-template
{{ with .Params.fruit }}
    <li>{{ index . 2}}</li>
{{ end }}
```

Another use of arrays is adding external links to a sidebar. Since the sidebar is a separate bit of HTML from your content section you can add in information to your frontmatter:

```yaml
externalLinks:
  - ["https://netlify.com/", "Netlify"]
  - ["https://vercel.com/", "Vercel"]
```

So this is an array of arrays. The first part is the url of the link and the second part is word or phrase we want to link to. How do we take this information and use it to produce a list of links in our template file?

```go-html-template
{{ with .Params.externalLinks }}
    <ul>
    {{ range . }}
        <li><a href="{{ index . 0 }}">{{ index . 1 }}</a></li>
    {{ end }}
    </ul>
{{ end }}
```

The `with` function checks to see if `externalLinks` actually exists on the page. If found the context is restricted to just the `externalLinks` variable.

Next there is a `range` function. As the context is already set it can just use the dot which is that context. In the href part we want the urls. As this this is the first part of the array it's index has a value of 0. The word or phrase to be used is in the second part of the arrays which has an index of 1.

## Links

1. [Intro to TOML](https://npf.io/2014/08/intro-to-toml/)
2. [Regis Philibert's Hugo cheatsheet](https://regisphilibert.com/blog/2017/04/hugo-cheat-sheet-go-template-translator/)
