---
title: "Multiple Content Files"
date: 2022-05-07T04:37:49+01:00
draft: true
tags: ["page", GetPage, leaf bundle]
summary: "Using multiple content files in a single page"
---

A typical Hugo site will have only one content file, the markdown file for each page in the `/content` section of the site.

However sometimes you might wish to use multiple content files on on page. This could be useful for pages that have multiple sections with different content. This is perfectly possible with Hugo and there are a number of different ways to achieve this.

## Simple method

A basic way to achieve this is with the `.GetPage` function. This function will get the page object of any page. This means any part of the page, `{{ .Title }}`, `{{ .Content }}`, `{{ .Date }}`, `{{ .Summary }}` and many more are accessible (See the [full list]). `.GetPage` is typically used with `{{ with }}` to reduce the scope for a block of possibilities.

For example lets say we create folder in the content section of our site called `home-page`. In this folder we keep several files to be used on the home page.

```go-html-template
{{ with .GetPage "home-page/about" }}
    <section>
        <h2>{{ .Title }}</h2>
        {{ .Content }}
    </section>
{{ end }}
```

This little block of code could be used anywhere in a page template file and title and content pulled from that 'page'.

Note that you don't need to use the file's extension, typically `.md`.

The downside of this method is that these bits of the home page could turn up elsewhere. If you use a `{{ range .Pages }}` these 'pages' would show up. 

To avoid this you to keep these separate *content* files in a leaf bundle. A leaf bundle is where instead of having a single markdown file for your page you have a folder, with the name of the page, and an `index.md` file.



## Using range to loop over a leaf bundle

If you have an indeterminate number of content files you can use the `range` function to loop through these.

### Home page and list pages

The home page, and section pages, use an `_index.md` file to ensure a home page or list page template is used rather than a single page template. So how do we use other content files for these pages? We can't use an `_index.md` file inside a new directory because that would make the new directory a new section of sub section of the whole site.

So we keep the existing `_index.md` file and create a new folder as before with an `index.md` file in. To prevent this from simply becoming a new leaf bundle we add the line `headless: true` to the frontmatter of that `index.md` file. This folder can then be used to store additional content files that can be accessed in our home page or list page templates.

In short we need:

1. A leaf bundle, for the content files plus an empty `index.md` file with `headless: true`.
2. Some template code that outputs the content of these files.

### Temporary new test template

If you want to try this out on an existing site's home page but don't wish to change all the code in your `home.html` template file you can just create a new template file named `home.html.html` which will take precedence over `home.html`. 

See the [template lookup order](https://gohugo.io/templates/lookup-order/) to see which templates get used first.


### Template code

Now assuming we have created a *leaf bundle* named *home-bundle* that contains several different content pages as well as an empty `index.md` file with the built in variable `headless`  set to `true`.

This code could be used in your home page template:

```go-html-template
 {{- $bundle := .Site.GetPage "page" "home-bundle" -}}
 {{- range $bundle.Resources.ByType "page"  }}

 <section class="{{ .File.BaseFileName }}">
     <div class="inner">
         <h2>{{ .Title }}</h2>
         {{ .Content }}</div>
 </section>
 {{- end -}}
```

<!-- This grabs every file that contains content in the folder. Then the `range` function loops through these, creates an HTML `section` for each one and puts the page's title and content within it. -->

Here `.GetPage` is used to get this leaf bundle called *home-bundle*. Then the `range` function goes through all the *resources* of the page&mdash;that is files in the same directory as the `index.md` file. It selects them by type, in this case the *page* type.

From here we can output each page to a new HTML `<section>`. Each section has a class name that is the same as the file name without the extension: `{{ .File.BaseFileName }}`. Any other page variables could have been used to generate a class name.

## Conclusion

The `.GetPage` function retrieves the page object of a specific content page. Typically it could be used as:

```go-html-template
{{ with .GetPage "/posts/multiple-content-files.md" }}
    {{ .Title }}
    {{ .Summary }}
{{ end }}
```

All the [page variables](https://gohugo.io/variables/page/#page-variables) are available.