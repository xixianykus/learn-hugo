---
title: "Menu Overview"
date: 2020-05-30T05:43:51+01:00
draft: false
tags: ["menus"]
summary: "A brief overview of creating menus in Hugo"
---

Menus can be created in 4 main ways: 
1. [Hard coded](#hard-coded) simple hardcoded links
2. [Config based menu](#config-based-menu) creating a menu object in the site's config file
   - [Menu for Lazy bloggers](#menu-for-lazy-bloggers)
3. [Frontmatter based menus](#frontmatter-based-menus) adding various pages to a named menu in the frontmatter of each page
4. [Using the `range` function](#using-the-range-function) to loop over part or all of the site.


## Hard coded

You can create a link anywhere directly in the HTML. Pages from the `content` folder will be in the root of your site, as will any main sections, that is folders directly below `/content/`.  If you're unsure on what the link is you can run the `hugo` command to build your site in the `public` directory and have a look there.

This is the least flexible way. If things in your blog change the links could break.

## Config based menu

Here you write out your menu in your config file. Again give it name and use the `range` function as above to access it in your templates.

This is more complicated but allows a lot more options. See [menus](/posts/menus/) for more detail on this method.

## Menu for Lazy bloggers

This creates a virtual menu (one you can't see or edit) of the list pages for each section of the web site. That is for each section you have in your content folder. It first requires a name and a single line in you're config file: 

```toml
sectionPagesMenu = "mymenu"
```

To place this in your templates you then use the range function on it:

```go-html-template
{{ range .Site.Menus.mymenu }}
  <a href="{{ .URL }}"> {{ .Name }} </a>
{{ end }}
```

**IMPORTANT x2** 
1. It's easy to get the syntax wrong here. This range function is NOT ranging over pages but over a menu object. That object has the fields `.URL` and `.Name` rather than `.Permalink` and `.Title`. 
2. Secondly the line in the config file does NOT reside under the `menus` object. It's just a single line on it's own and in a `toml` file. Like all single lines in config.toml it should be placed above any lists or objects (ie. lines with square brackets).

N.B The term `mymenu` can be any name you want to call your menu.

~~N.B.2 You used to use `.URL` to range over the menu object but that is now deprecated and won't be available in the future.~~

The pros of this method are that it is quick to do and if you add any new top level section your menu will update automatically. The downside is a lack of control to remove items or position them. You could potentially do that in the frontmatter of each section's `_index.md` file though at that point you may as well just use the frontmatter based method below.

If you want to add extra pages, like an About page or Contact page just add a line to your frontmatter: `menu: mymenu`.


## Using the `range` function

You can create a "menu" using the `range` function. I'm using inverted commas here because technically this is NOT a menu in Hugo. That's because there is no menu object.

If you want the menu to have the same list from anywhere on the site you need to start with `.Site` or `site` to prevent the menu changing when appearing in different sections.

```go-html-template
{{ range .Site.sections }}
   <a href="{{ .RelPermalink }}">{{ .Title }}</a>
{{ end }}
```

The above will produce a list the same as the next option and it saves you editing the config file.




## Frontmatter based menus

You use `menu: mymenu` in the frontmatter of each page you wish to appear in your menu. This also creates a menu object that can be looped over with the `range` function in the same way as config file based menus.

If you wish to order your menu items you can add a *weight* value in the front matter like so: `weight: 20`. The higher the number the lower down the menu it will appear.





