---
title: "List Page"
date: 2020-05-12T15:19:51+01:00
draft: true
tags: ["range", "list pages"]
summary: "How list pages work"
---

The two most basic templates are `single.html` and `list.html`. The latter is what is also called a section page. A food web site might have several sections: recipes, eating out and a blog for instance. The job of these section pages is to list and link to all of the pages in that section. 

If you're not using a `baseof.html` template then begin with a regular HTML block of code including a `<head>` and `<body>` sections. 

If you're using a `baseof.html` template then start with defining your block. If it was called *main* you would write:

```go
{{ define "main" }}

{{ end }}
```

Your code goes in between those two tags.

## Creating a list

How do we populate the list page with an actual list, of say all the pages on the site?

This is done using the **range** function. Like the *define* function it too always has a closing tag:

```go
{{ range <some-list> }} {{ end }}
```

The `range` function works on arrays, that is lists of things. For each item in the list it outputs it. A list of objects, such as `.Pages` the parts of each item you specify in a list. 

You need to tell it what to list, in this case we want the site's pages.

We also want the title of the page to be returned...

```go
{{ range .Pages }}
  {{ .Title }}
{{ end }}
```

(We can also get the url of each page using `{{ .Permalink }}`)

To generate an HTML unordered list of links you can use the range function like this:

```go-html-template
<ul>
{{ range .Pages }}
    <li><a href="{{ .Permalink }}">{{ .Title }}</a></li>
{{ end }}
</ul>
```

## Ordering the list

To order the list in a specific way we just need to tell ```{{ range .Pages }}``` by adding another parameter?

```go
{{ range .Pages.ByTitle }}
```

Now they should be listed alphabetically.