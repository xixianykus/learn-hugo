---
title: "Posts"
date: 2020-05-26T07:29:52+01:00
draft: false
tags: [""]
summary: ""
---

This is is alphabetical list of all posts. Those in brown are drafts or unfinished posts. I decided to display them as they still might be useful to some people and I might need to refer to them myself sometime.

In total there are now {{< shorts/countpages >}} pages on this site.

