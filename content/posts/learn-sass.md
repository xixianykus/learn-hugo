---
title: "Learn SASS"
date: 2020-05-28T07:01:05+01:00
draft: true
tags: ["css", "sass"]
summary: "Quick primer on using SASS"
---

Aside from using all the features of SASS you can also write plain css in a sass file too.


## 1. Variables

    “Probably one of the most powerful things in SASS.”

Defined using the dollar sign and with a colon:

```sass
$primaryColor : #56f2d3
```

Then used elsewhere as:

```sass
body {
    background-color: $primaryColor;
}
```

### Default value

A variable can be assigned a default value in SASS in similar way to using `!important` in CSS.

```sass
color: #c20 !default;
```

### Scope

SASS variables can be scoped to a selector by defining them inside of it. For instance following colour will only be used inside the HTML element.

```sass
html {
    $color: #c20;
}
```


### When to use?

When deciding whether to use SASS variables or CSS custom properties it's generally better to use SASS variables when they're not going to change in the browser because it's less work for the browser.



## 2. Nesting

Rather than: 

```css
header {
    color: green;
    margin: 0;
}
header ul {
    list-style-type: none;
}
```

We can do this:

```sass
header {
    color: green;
    margin: 0;
    ul {
        list-style-type: none;
    }
}
```

When you nest a declaration inside another it just means the nested element is a child of the declaration that contains it:

```css
header ul 
```

You can add pseudo classes too using &amp;

```sass
button {
    background-color: $primary;
        &:hover {
        background-color: green;
    }
}
```

## 3. Splitting up files

Instead of one big long css file we can split it into sub files covering different things.

These sub files begin with and underscore.

To import these files add the following in the main file

```sass
@import "./header";
@import "./footer";
```

This will import files (located in the same folder) called `header.scss` and `footer.scss`. No need to add the file extension.


## 4. Mixins

Mixins are the equivalent of functions in Javascript. A block of code can be declared once then used repeatedly.

To declare a mixin use `@mixin *yourmixinname*`

```sass
@mixin flexCenter {
    display: flex;
    align-items: center;
    justify-content: center;
}
```

To call or place this block of code we just use `@include` like so:

```sass
@include flexCenter()
```

## Using variables in mixins



## Video

The above was based on the following video:

{{< youtube Zz6eOVaaelI >}}