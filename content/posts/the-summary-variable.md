---
title: "The Summary Variable"
linktitle: "Summaries"
date: 2021-12-26T04:32:07Z
draft: false
tags: ["variables"]
summary: "How the built in summary variable works"
---

The `{{ .Summary }}` variable is another built-in Hugo variable that works differently depending on how your site is set up. 

## No summary

If there is no `summary` set elsewhere then adding `{{ .Summary }}` to your template files means Hugo will use the first `70` characters of the page's content, ie. from `{{ .Content }}`. 

This character limit of `70` can be changed in the site's config file using `summaryLength`.

```toml
summaryLength = 90
```

NB. An alternative way to limit the length of any text in Hugo is to use  Hugo's `truncate` function in your template files:

```go-html-template
{{ .Summary | truncate 100 }}
```

## Frontmatter summary

You add a `summary` variable to your page's frontmatter and thus have a summary independent of the page's content. Hugo will then choose this rather than the first `x` number of characters on the page.

```yaml
summary: Some summary text...
```

## Using `more`

The second option is to use a `more` reference in your page's content like this:

{{< highlight "md" >}}
Some summary text is above this line.

<--more-->

The content below is not referenced by .Summary
{{< /highlight >}}


Content above the `more` line of code will be used for the page's summary. That below will not.

NB. There are no spaces between the hyphens or other characters in this.

## Options

These 3 options will be preferentially used by Hugo in this order:

1. the text up to the `more` line is used first
2. the summary frontmatter variable  is next
3. the first 70 characters on the page is used last
  
So if both the frontmatter `summary` and `<--more-->` are set the text up to `<--more-->` will be used. The first 70 characters option is a useful fallback, only used where neither of these are set.

## Using .Params.summary

Because summary is a built in variable you don't use `{{ Params.summary }}` to reference your frontmatter `summary` text. However if you do it still works but in a different way to just `{{ .Summary }}` that can prove useful.

As explained above: with `{{ .Summary }}` if a page has no frontmatter summary set the first *x* number of characters from the page's content will be used. However if your summary is placed just above your page's content this is not what you want: it will just dupicate the first bit of text.

But if you use `{{ .Params.summary }}` this doesn't happen. If there is no frontmatter summary then nothing will appear.

```go-html-template
{{ with .Params.summary }}{{ . }}{{ end }}

{{ .Content }}
```

Another advantage of using `{{ .Params.summary }}` is that you can include HTML in your frontmatter as long as it's piped to the `safeHTML` function.

```go-html-template
{{ .Params.summary | safeHTML }}
```

Trying this with `.Summary` does not work.

The nice thing is you can use either output from one `summary` frontmatter variable. You might want `.Summary` in a list and `.Params.summary` on your article page. If the summary contains HTML it will be replaced with HTML comment tags in source of the page when using `.Summary`.


## Links

1. [Hugo docs on summaries](https://gohugo.io/content-management/summaries/) has a few more details

