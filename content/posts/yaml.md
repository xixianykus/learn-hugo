---
title: "YAML"
date: 2020-05-16T09:53:54+01:00
draft: false
summary: "Notes on YAML syntax"
tags: ["YAML", "frontmatter", date]
---

YAML stands for *YAML Ain't Markup Language*.

YAML is a *data serialisation language*. It's used to store data typically in the form of key/value pairs. For example `title: biscuits` where title is the key and *biscuits* is it value. 

It's main strength is that it's very easy to read. It doesn't use lots of different symbols like JSON and TOML. In fact you can get a long way using just a colon and a hyphen.


## Basic rules

1. No tabs. YAML uses only spaces
2. The language is case sensitive.
3. YAML files end in .yaml OR .yml, though *yaml* is preferred.
4. Comments are preceded with a hash  or pound symbol `#`.

## Syntax

The basic unit of YAML is the key/value pair written simply like this.

```yaml
species: tiger
order: feliformia
diet: meat # This is a comment
```

These are 3 key/value pairs. Note the space after the colon is mandatory.

Like other languages YAML's values can be categorized into a string, integer, floating point number or boolean (binary values like *true* or *false* for example).

```yaml
# A simple pound sign denotes a comment in YAML. The space after is optional.
string: This is a string. # Use single or double quotes where special characters are used (see below)
integer: 95
float: 95.1
boolean: yes
```

NB. Boolean values in YAML can use any of the following: `yes/no` `on/off` or `true/false`

### Spaces

Spaces are critical in YAML. The space after the colon is as important as the colon.

When in comes to *keys*, a space before the colon the space is optional. Since it's unnecessary it's typically left out. YAML allows multi-word keys separated by spaces BUT in Hugo it makes life easier in Hugo to avoid spaces. For example although `my var: 10` is correct YAML you couldn't use `my var` in `{{ .Site.Params.my var }}`. So camelcase, `myVar` or underscores `my_var` are preferred.

### String Quotes

You don't have to use quotes for regular strings though the quotes can be used to force a string from a number or boolean.

If the string contains special characters then quotes should be used and single quotes are better.

If the string contains any of the following it must be surrounded in quotes:

`:, {, }, [, ], ,, &, *, #, ?, |, -, <, >, =, !, %, @, \` `

If the string contains unicode characters you can embed it in the string by adding `\n` to the end:

"A double-quoted string in YAML\n"

See: [leopathu.com](https://leopathu.com/content/string-quotes-yaml-file) for more.


### Lists

Some variables may have multiple values. These are referred to as arrays or lists. Here ways to write out the same list:

```yaml
Vegetables: [carrot, "peas", cauliflower]

Vegetables:
- carrot
- peas
- cauliflower
```


Again quotes are optional except where special characters are used. Don't use quotes if using numbers or booleans, unless you want them to be interpreted as a string.

In lists the list order matters. As there is no specific key to target a value you use the position in the list instead. The first item is numbered `0`.

### Maps or Dictionararies

These are collection of variables (key/value pairs) rather than values in lists. Again these go by different names. Often the word object is used. Another term is associative array. In Hugo the term *dictionary* or *map* means the same thing and the Hugo function `{{ dict }}` is used to create one of these.

In YAML objects can be expressed in two ways: as an indented list of key value pairs OR in curly brackets with each key/value pair separated from the next with a comma.

```yaml
Banana:
  calories: 105
  fat: 0.4 grams
  carbs: 27 grams

Banana: {calories: 105, fat: 0.4 grams, carbs: 27 grams}

Grapes:
  calories: 62
  fat: 0.3 grams
  carbs: 24 grams
```

The blank spaces fon indentation must be all the same, for instance two spaces, so they're all aligned. And you cannot use tabs for spacing.

NB. A key either has a value or a list of hashmap NOT both. Line 1 of the following makes it invalid:

```yaml
Fruit: banana
  calories: 105
  fat: 0.4 grams
  carbs: 27 grams
```

So whereas a list is a collection of values a map is a collection of key/value pairs.

### Lists of objects

Can be written like this:

```yaml
friends:
  - name: Bob
    age: 55
    pet: dog
  - name: Suzie
    age: 17
    pet: goldfish
```

Or this:

```yaml
friends: [ {name: Bob, age: 55, pet: dog }, {name: Suzie, age: 17, pet: goldfish} ]
```

NB. The list of objects must still be separated by commas. 


An object needn't have a title when part of a list and a list may have only 1 item. It may just be a collection of key values. So a list of objects with only one member can be written like this:

```yaml
- age: 27
  name: Jon
  married: true
```

The hyphen denotes the start of the object and variables below belong to that object until either another hyphen is used denoting a different object or a key/value pair is unindented.

A list of objects can also be written inside curly brackets and separated by commas like this:

```yaml
friend: {name: bob, age: 55, pet: monkey}
```

```yaml
friends:
  - {name: bob, age: 55, pet: monkey}
  - {name: Tim, age: 75, pet: fish}
```


{{< youtube o9pT9cWzbnI >}}

## HTML and CSS

If you are putting HTML or CSS in your string you can *pipe* the string to `safeHTML`, `safeCSS` or `safeHTMLAttr` (for an HTML attribute) in your templates.

```go
{{ .Params.htmlcode | safeHTML }}
{{ .Params.mycss | safeCSS }}
```



### Date and Time

Dates can be defined using the [ISO 8601](https://www.ionos.com/digitalguide/websites/web-development/iso-8601/) standard which means the year, the month and the day separated by hyphens: YYYY-MM-DD.

Thus Christmas day in 2025 would be: `2025-12-25`. The idea is largest units on the left and decending rightwards.

Times can be included with a `T-` and then separated with a colon like this:

`2019-09-07T-15:50+00` Here the +00 represents *standard time* or Greenwich Meantime. Use + or - hours for the time zone you're refering to followed by the hours in two digits, eg. `-04`.




## Choosing the layout for a page

Can be set in the frontmatter using:

```yaml
layoutDir: layouts
```

The directory from where Hugo reads layouts (templates).

## Slug

You can use this to change the name of the last part of the url.

```yaml
slug: peanuts
```
Alternatively you can use the key `url` which is more powerful and affects the whole url. (Not in the doc guide though?)


## Example YAML file

Here's an example from [json2yaml.com](https://www.json2yaml.com/)
```yaml
---
# <- yaml supports comments, json does not
# did you know you can embed json in yaml?
# try uncommenting the next line
# { foo: 'bar' }

json:
  - rigid
  - better for data interchange
yaml:
  - slim and flexible
  - better for configuration
object:
	key: value
  array:
    - null_value:
    - boolean: true
    - integer: 1
paragraph: >
   Greater than symbol means you can break
   up lines link this but when read back everything
   is still on one line

   paragraph breaks
content: |-
   Using the pipe symbol instead
   means line breaks are preserved
```

And another one:

```yaml
---
colors:
- color: black
  category: hue
  type: primary
  code:
    rgba:
    - 255
    - 255
    - 255
    - 1
    hex: "#000"
- color: white
  category: value
  code:
    rgba:
    - 0
    - 0
    - 0
    - 1
    hex: "#FFF"
- color: red
  category: hue
  type: primary
  code:
    rgba:
    - 255
    - 0
    - 0
    - 1
    hex: "#FF0"
```

### Anchors

It's also possible to use *anchors* inside a yaml file or block. These allow you to pull a value or even a key/value pair from one part of the code to somewhere else. You use the `&` with a string to grab something and an asterick, `*` with the same name to replicate it. `&name` could pull the value and `*name` would replicate it.

```yaml
name: &thisname Coastal Plain
```

Then to use it elsewhere:

```yaml
chart: *thisname
```

If the name changes in the first example it will also change in the second `chart`.

## TOML

So there's a good [article on GitHub](https://github.com/toml-lang/toml#user-content-example) on TOML.

Interesting is the use of double brackets in an *Array of Tables* [link](https://github.com/toml-lang/toml#array-of-tables)

## Links
1. [Frontmatter](https://gohugo.io/getting-started/configuration/#configure-front-matter) - from the official Hugo docs.
2. [Grav guide to YAML](https://learn.getgrav.org/16/advanced/yaml) is nicely written.
3. [YAML Lint](https://www.yamllint.com/) paste your YAML code in to check for errors.
4. [Online YAML Tools](https://onlineyamltools.com/validate-yaml) very quick and easy way to check for errors.
5. [Learn YAML for absolute beginners](https://www.tutorialspoint.com/yaml/index.htm) by Tutorials Point is a very thorough spread over 22 pages