---
title: "Text Functions"
date: 2020-09-28T17:59:07+01:00
draft: false
tags: ["text", "safeHTML", "markdownify", "humanize"]
summary: "Simple functions that modify text strings"
---


These functions are used in Hugo to modify (or preserve) text strings. Although there's a lot of them they're mostly fairly simple to use and understand.

These are typically used with the pipe. For example `<a href="/folder1/{{ . | urlize }}">`

- [anchorize](https://gohugo.io/functions/anchorize/) - makes everything lowercase and replaces spaces, dots, apostophes into hyphens. Ideal for links in other words.
- [emojify](https://gohugo.io/functions/emojify/) - "For emojis in content files, set `enableEmoji = true` in your site’s configuration."
- html - can be used to make sure HTML is rendered as HTML.
- [htmlEscape](https://gohugo.io/functions/htmlescape/) - converts an ampersand to `&amp;`. It escapes only: <, >, &, ' and ".
- [htmlUnescape](https://gohugo.io/functions/htmlunescape/) - reverses the above so `&amp` will become & etc.
- [lower](https://gohugo.io/functions/lower/) - changes everything to lowercase
- [humanize](https://gohugo.io/functions/humanize/) - replaces hyphens between word with spaces and capitalizes the first letter of the first word. On numbers adds an st / nd / rd  or th (eg. 105th)
- [markdownify](https://gohugo.io/functions/markdownify/)
- [plainify](https://gohugo.io/functions/plainify/) - strips any HTML and returns plain text.
- [pluralize](https://gohugo.io/functions/pluralize/) - pluralizes a word according to english rules.
- [safeCSS](https://gohugo.io/functions/safecss/) - declares a string is "safe" CSS. So CSS strings won't have `{` and `}` stripped.
- [safeHTML](https://gohugo.io/functions/safehtml/) - same as above but for HTML. Used in a TOML or YAML variable you still need to escape the `"` by putting a backslash just in front of them, eg.: `<a href=\"https://creativecommons.org/licenses/by/4.0/\">`
- [safeHTMLAttr](https://gohugo.io/functions/safehtmlattr/) - for HTML attributes. Useful in link variables like `.URL`, sometimes with `printf`.
- [safeJS](https://gohugo.io/functions/safejs/) - declares the string is "safe" javascript.
- [safeURL](https://gohugo.io/functions/safeurl/) - `http:`, `https:` and `mailto:` are considered safe by Go templates. But others like `ftp:`, `irc:` etc. require this to work.
- [singularize](https://gohugo.io/functions/singularize/) - opposite of pluralize.
- [title](https://gohugo.io/functions/title/) - converts to title case. Can be [configured](https://gohugo.io/getting-started/configuration/#configure-title-case).
- [upper](https://gohugo.io/functions/upper/) - converts all letters to uppercase.
- [urlize](https://gohugo.io/functions/urlize/) - useful in urls or partial urls. Converts spaces to hyphens. Protects forward slashes. See tags [example](/posts/how-to-add-tags/).