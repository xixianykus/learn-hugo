---
title: "Adding Template Code in CSS Files"
linktitle: "Template Code in CSS"
date: 2024-03-26T21:02:52Z
# draft: true
tags: [CSS, assets, ExecuteAsTemplate]
summary: "You can add Hugo's template code to CSS files when you know how."
---

It's possible to add Hugo's template code inside a CSS or SASS file. But..

## Why would you want to do this?

There are many possibilities here.

One example might be that you wish to set a background image in the CSS with an image in the `/assets/` directory. To retrieve the image you need to use Hugo's `resources.Get` code. From there you have access to all of Hugo's image processing capabilities.

For instance you might have something like this inside your CSS or SASS file:

```go-html-template
{{ $coding := resources.Get "images/coding.jpg" }}

header  {
    background-image: url({{ $coding.RelPermalink }});
    background-size: cover;
}
```

You may also want to set some variables inside your config file to set things such as the colours making it easy to change without diving into the CSS files.


## How?

It's possible to add Hugo's template code inside a CSS file but you need to do several things for this to work.

1. The CSS file must be in the `/assets/` folder somewhere.
2. The file needs to be *executed as a resource*.
3. A new CSS file (without the template code) is created so a path and name for that needs to be set.
4. There needs to be context, typically just the dot.

[Hugo docs](https://gohugo.io/hugo-pipes/resource-from-template/#usage) says:

> In order to use Hugo Pipes function on an asset file containing Go Template magic the function resources.ExecuteAsTemplate must be used.
>
> The function takes three arguments: 
> - the target path for the created resource 
> - the template context, 
> - and the resource object. 
> The target path is used to cache the result.


## The `<head>` section

And in the `head` section the CSS link looks like this:

```go-html-template
{{ $base_css := resources.Get "css/styles.tpl.css" | resources.ExecuteAsTemplate "css/styles.css" . | fingerprint }}
 
<link rel="stylesheet" href="{{ $base_css.RelPermalink }}">
```
