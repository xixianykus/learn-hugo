---
title: "1. Getting Started"
date: 2030-05-12T13:26:08+01:00
draft: true
tags: [""]
summary: "Making a new site part 1."
---

# Starting Out.

After installing Hugo you can create the skeleton of new site from the command line. Navigate to the folder that you want to contain the folder of the new site.

```bash
hugo new site MySite
```

This will create some empty folders and just two text files: *default.md* is markdown file in the archetypes folder and *config.toml* is the config file for the whole site in the root folder.

From here there are two ways to proceed.

1. install a theme
2. create a new blank theme
3. create your own files

We'll go for 3 here as you'll learn much more doing it this way.

## Templates

Hugo uses go which is templating language. So to make a site we need to create templates from which to build the site.

Templates belong in the *layouts* folder BUT there is strict way of adding these. This folder is empty because if we were to use a theme all the templates would be contained in that. But if there were also template files in the layouts folder those would override the theme's template files.

So the structure of the layouts folder is like this:

```
layouts
   _default
      baseof.html
      list.html
      single.html
    partials
      footer.html
      header.html
```

We'll ignore baseof.html for now. Instead put some HTML in the list.html and single.html files.

If you start the Hugo server (**hugo server -D**), you should be able to see your home page which by default is a list page.

### Alternative

To avoid manually creating each folder and file creating a new theme will do this for you:

```bash
hugo new theme Zafta
```
This is what Hugo creates if you created a theme called Zafta

![new theme folder structure](../../static/img/new-theme.PNG "new theme folder")

## Adding some CSS

To link a CSS file to every page is slightly counter intuitive at first

```html
	<link rel="stylesheet" href="{{ "css/style.css" | relURL }}">
```

Note the extra pair of quotes here.


## Adding a partial: The Header

Open the header file and add some html: a header tag with a div or some other inside it plus whatever word you want in your header:

```html
<header>
    <div>MY HEADER</div>
</header>
```
This is all that needs to go in this file for now.

Now how do we get this into our list.html file?

```go
{{ partial "header" . }}
```

Write this into the list.html file just below the opening body tag.

Now we could put the name of the site there so, back in header.html, remove MY HEADER and replace it with:

```go
{{ .Site.Title }}
```

This will pull the title from the config.toml file. This variable can be used in lots of different places on the site. And if you decide to change the site's name sometime you only have to do it one place.

So:

```html
<header>
    <div>{{ .Site.Title }}</div>
</header>
```

But how about making that a link back to the home page? We can use the variable defined in the config page:

```toml
baseURL = "http://example.org/"
```

This will work on our local Hugo server whatever is written there. But if you upload this to the web put the right url there.

To make the header title a link:

```go
<header>
  <div><a href=" {{ .Site.BaseURL }} ">{{ .Site.Title }}</a></div>
</header>
```

NB Yes, these function and variables ARE case sensitive.
