---
title: "Count Pages"
date: 2021-04-09T07:45:06+01:00
draft: false
tags: ["plugins","len"]
summary: "How to count and display the number of pages your web site has"
---

This is easy to do using the `len`, length function.

```go-html-template
{{ len .Site.Pages }}
```

However since you may not want to count all pages you could use:

```go-html-template
{{ len .Site.RegularPages }}
```

So currently this site has {{< shorts/countpages >}} regular pages.

`len` can be used on slice (array), map (object) or string

Here's an example the [official docs](https://gohugo.io/functions/len/) give to add a class name to long titles:

```go-html-template
<header>
    <h1 {{ if gt (len .Title) 80 }} class="long-title"{{ end }}>{{ .Title }}</h1>
</header>
```

And another possible version for counting pages:

```go-html-template
{{ $posts := (where .Site.RegularPages "Section" "==" "posts") }}
{{ $postCount := len $posts }}
```

## Links

1. [Official Hugo docs page](https://gohugo.io/functions/len/) on `len`