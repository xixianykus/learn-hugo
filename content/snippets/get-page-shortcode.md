---
title: "Get Page Shortcode"
date: 2022-07-29T20:07:22+01:00
draft: false
tags: [shortcodes, readFile, Scratch, splits, syntax highlighting]
summary: "A shortcode to grab code files from a project and display in a page"
style: |
  .alert {
    padding: 1em;
    border: solid 4px currentColor;
    color: var(--accent-1);
    font-weight: bold;
  }
---

Note that a simpler way of achieving the same thing is to use Hugo's `readFile` and `highlight` functions. `readFile` grabs the content of a file and `highlight` can add syntax highlighting in the language desired.
{.alert}


The purpose of this shortcode is to display the contents of another file like the site's CSS or JS files and embed them in the page using Hugo's syntax highlighting for the language used.

Here is an example using the shortcode to display it's own code.

{{< get-page file="layouts/shortcodes/get-page.html" lang="go-html-template" >}}

The shortcode called `get-page` here takes two parameters `file` and `lang`. The former is the path to the file. The latter is the language the file is in using the same format as Hugo's code fences. For example: `js`, `css`, `html`, `go-html-template` etc..

The shortcode comes from [a blog post](https://it.knightnet.org.uk/kb/hugo/hugo-snippets/#shortcode-to-render-a-code-example-in-a-pagepost) by Julian Knight


## Autodetect

[Another version](https://discourse.gohugo.io/t/code-block-with-readfile-in-markdown/14555/6?u=horbes) which is more complex can auto detect the language the file is using based on it's file extension. An optional second parameter allows you to override this and put the language in manually.

```go-html-template
{{- if gt ( len .Params ) 1 -}}
    {{- $.Scratch.Set "format" ( .Get 1 ) -}}
{{- else -}}
    {{- $splits := ( split ( .Get 0 ) "." ) -}}
    {{- $splitsLength := ( len $splits ) -}}
    {{- $format := ( index $splits ( sub $splitsLength 1 ) ) -}}
    {{- $.Scratch.Set "format" $format -}}
{{- end -}}

{{- $file := ( .Get 0 ) -}}
{{- $format := ( $.Scratch.Get "format" ) -}}
{{- ( print "```" $format "\n" ( readFile $file ) "\n" "```" ) | markdownify -}}
```

The first code block sets the format. If the second parameter (the language) is set in the shortcode it will choose that. If not it will grab the file extension and use that: css, js etc..

The second block prints the file out using *codefences* along with the language format to print out the file.

The first block uses Hugo's `.Scratch` which is explained well in [a post](https://www.regisphilibert.com/blog/2017/04/hugo-scratch-explained-variable/) by Regis Philibert.