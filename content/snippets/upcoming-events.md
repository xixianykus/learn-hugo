---
title: "Upcoming Events"
date: 2021-05-15T06:54:04+01:00
draft: false
tags: ["date"]
summary: "Code comparing Unix date with the date now to present a list of future dates."
---

This is a code block from the end of the [Intro to Hugo Templating](https://gohugo.io/templates/introduction/#example-show-only-upcoming-events) that has interesting use of the Unix date.

```go-html-template
<h4>Upcoming Events</h4>
<ul class="upcoming-events">
{{ range where .Pages.ByDate "Section" "events" }}
    {{ if ge .Date.Unix now.Unix }}
        <li>
        <!-- add span for event type -->
          <span>{{ .Type | title }} —</span>
          {{ .Title }} on
        <!-- add span for event date -->
          <span>{{ .Date.Format "2 January at 3:04pm" }}</span>
          at {{ .Params.place }}
        </li>
    {{ end }}
{{ end }}
</ul>
```

