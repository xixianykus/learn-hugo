---
title: "Image Gallery"
date: 2022-07-29T06:29:19+01:00
draft: false
tags: ["image processing"]
summary: "Shortcode to quickly create a gallery of images from a folder"
---

This shortcode is from Joost van der Schee of [Hugo Codex](https://hugocodex.org/add-ons/image-gallery/).

```go-html-template
{{ $dir := .Params.gallery_dir }}
<ul class="image-gallery">
{{ range (readDir (printf "assets/%s" $dir)) }}
  {{- $image := resources.Get (printf "%s/%s" $dir .Name) -}}
  {{- $imageurl := printf "/%s/%s" $dir .Name -}}
  {{- $imagetitle := index (split .Name ".") 0 -}}
    <li>
      <a href="{{ ($image.Fit "1600x1600 q50").Permalink }}" title="{{ $imagetitle }}" class="lightbox-image">
        <img src="{{ ($image.Fill "300x300 smart q50").Permalink }}" alt="{{ $imagetitle }}" title="{{ $imagetitle }}">
        <span>{{ $imagetitle }}</span>
      </a>
    </li>
  </ul>
{{ end }}
{{ end }}
```

## Usage

To use it you first add a group of images to a subfolder within the `assets` directory[^1].

Add this shortcode to your content page replacing the word album with the name (or path) of your directory of images. You don't need to specify the `/assets` directory as that is written in the shortcode.

```go-html-template
{{⪡ image-gallery gallery_dir="album" ⪢}}
```

<script src="/js/lightbox.js"></script>
<link rel="stylesheet" href="/css/lightbox.css">

### With lightbox.js

The anchor tags produced in the thumbnail list have a class of `lightbox-image`. This means all you need to do to create a lightbox gallery is provide a link to the [lightbox css](/css/lightbox.css "right click and choose save link as") and [js](/js/lightbox.js "right click and choose save link as") files. These are 3kb and 7kb respectively and not minified.


<div class="lightbox-code" x-data="{openJS : false, openCSS : false}">
<button @click="openJS = !openJS">lightbox.js</button>
<button @click="openCSS = !openCSS">lightbox.css</button>

<div x-show="openJS" x-transition.duration.500ms>

{{< get-page file="static/js/lightbox.js" lang="js" >}}
</div>


<div x-show="openCSS" x-transition.duration.500ms>

{{< get-page file="static/css/lightbox.css" lang="css" >}}
</div>
</div>



## An example

{{< image-gallery gallery_dir="img/design" >}}



[^1]: The `assets` directory is not created by default in Hugo. Simply create a directory called `assets` in the root of your project. Alternatively you can use an existing directory for assets and add the line `assetsDir = path/to/my-dir` in your config file with the actual path to your chosen directory.