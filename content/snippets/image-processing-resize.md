---
title: "Image Processing Resize"
date: 2021-05-08T17:51:11+01:00
draft: false
tags: ["image processing"]
summary: "A shortcode using image processing to make multiple copies in different sizes of an image using Hugo's image processing."
---

Here's a shortcode to resize an image using Hugo's image processing. Taken from the blog of [Nils Norman Haukås](https://nilsnh.no/2018/06/10/hugo-how-to-add-support-for-responsive-images-trough-image-processing-and-page-bundles-3/) where there is accompanying explanation too.

```go-html-template
{{ $altText := .Get "alt"}}
{{ $caption := .Get "caption"}}
{{ with $.Page.Resources.GetMatch (.Get "name") }}
  <figure>
    <a href="{{.RelPermalink}}">
      <img
        srcset="
          {{ (.Resize "320x").RelPermalink }} 320w,
          {{ (.Resize "600x").RelPermalink }} 600w,
          {{ (.Resize "1200x").RelPermalink }} 2x"
        src="{{ (.Resize "600x").RelPermalink }}" alt="{{$altText}}"/>
    </a>
    <figcaption><p>{{ $caption }}</p></figcaption>
  </figure>
{{ else }}
  could not find image
{{ end }}
```

The shortcode will look like this if it is named `bundle_image.html` in the shortcodes folder. (Those are single less than and greater than symbols but using these: `< >` is intepreted as a short code hence the double ones):

```go-html-template
{{⪡ bundle_image name="/image/myimage.jpg" alt="Alt text for image here" caption="This image is blah, blah" ⪢}}
```


## Links

1. [Hugo docs on Image processing](https://gohugo.io/content-management/image-processing/)