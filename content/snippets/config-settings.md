---
title: "Config Settings"
date: 2022-08-22T07:40:47+01:00
draft: false
tags: [config, TOML]
summary: Useful configuration settings in TOML
---

For Hugo to run you have to have a config file in the root of your project - even an empty one will do.

There are lots of settings Hugo's configuration file can take including your own variables in the `[Params]` section. You can have multiple config files if you like too.

Here is a list of some settings I find useful on a regular basis.

A [more thorough list](https://gohugo.io/getting-started/configuration/) can be found in the Hugo docs.


```TOML
title = "My Website"

theme = "blank"
baseURL = "https://example.com/"
languageCode = "en-us"
name = "Blank"
license = "MIT"
licenselink = "https://github.com/vimux/blank/blob/master/LICENSE"
description = "Code snippets, mostly from Hugo static site generator."
homepage = "https://github.com/vimux/blank/"
tags = ["blog", "starter", "hugo"]
features = ["blog"]
min_version = "0.20"

paginate = 12 
enableRobotsTXT = true
enableGitInfo = true
enableEmoji = true
disableKinds = ["page", "home", "section", "taxonomy", "term", "RSS", "sitemap", "robotsTXT", "404"]  # can choose from the list

# Hugo generates content in the following formats. The default is HTML and RSS.
[outputs]
   home = [ "HTML", "RSS", "JSON" ]

[author]
  name = "Joe Bloggs"
  homepage = "https://joebloggs.com/"

[[menu.main]]
  name = "About"
  identifier = "About"
  url = "/About/"
  pre = '<svg class="icon icon-about"><use xlink:href="#icon-about"></use></svg>'
  weight = 5
[[menu.main]]
  name = "Posts"
  identifier = "posts"
  url = "/posts/"
  pre = '<svg class="icon icon-posts"><use xlink:href="#icon-posts"></use></svg>'
  weight = 10
[[menu.main]]
  name = "Links"
  identifier = "links"
  url = "/links/"
  pre = '<svg class="icon icon-links"><use xlink:href="#icon-links"></use></svg>'
  weight = 50


[params]
  mainSections = ["posts", "code", "music"]
  subtitle = ["experiment", "lots", "learn", "more"]

# The default taxonomies are tags and categories so this is only set if you want something other than those.
[taxonomies]
  tag = "tags"


# The [markup] section controls the markdown processor settings.
[markup]

# the syntax highlighting settings
  [markup.highlight]
    codeFences = true
    guessSyntax = false
    hl_Lines = ""
    lineNoStart = 1
    lineNos = true
    lineNumbersInTable = false
    noClasses = true
    style = "monokailight"
    tabWidth = 4
    autoHeadingIDType = "github"

# allows block level element to add HTML attributes using {.some-class} or {#some-id}
  [markup.goldmark.parser.attribute]
    block = true
    title = true

# allows HTML to be parsed in markdown files.
  [markup.goldmark.renderer]
   unsafe = true

# settings for Hugo's table of contents variable. The levels indicate the <h2> and <h3> headings that Hugo generates the table list from.
  [markup.tableOfContents]
    ordered = false
    startLevel = 2
    endLevel = 3
```
