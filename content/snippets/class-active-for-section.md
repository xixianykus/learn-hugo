---
title: "Class Active for Section"
date: 2021-08-24T21:51:47+01:00
draft: false
tags: ["menus", navigation, if]
summary: "How to make any page in a section make the main menu link active."
---

So rather than a specific link to *this page* this is how to set a `class="active"` to a menu item for any pages in it's section.

```go-html-template
<nav class="sidebar-nav">
    {{ $currentPage := . }}
    {{ range .Site.Menus.lazy }}
    <a class="sidebar-nav-item{{if or ($currentPage.IsMenuCurrent "main" .) ($currentPage.HasMenuCurrent "main" .) }} active{{end}}" href="{{ .URL }}" >{{ .Name }}</a>
    {{ end }}
</nav>
```

## Beware the capitilization on Menus

Also note the capitilization on the range function:

```go-html-template
{{ range .Site.Menus.lazy }}
```
`Menus` gets a capital coz it's official Hugo term. But your specific menu name is anything you want it to be, so not official Hugo and no capital letter.