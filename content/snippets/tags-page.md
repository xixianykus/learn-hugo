---
title: "Tags Page"
date: 2021-08-10T17:02:14+01:00
draft: false
tags: ["tags"]
summary: "Code for my tags page"
---

So this bit of code comes straight from Hugo docs. It allows you to *count* the number of Pages tagged with a given tag:

```go-html-template
<ul>
    {{ range .Data.Terms.Alphabetical }}
            <li><a href="{{ .Page.Permalink }}">{{ .Page.Title }}</a> {{ .Count }}</li>
    {{ end }}
</ul>
```

Here the pages are listed in alphabetical order but you can list them by the number of pages each has using {{ .Data.Terms.ByCount }}

## Links

1. [Order Taxonomies](https://gohugo.io/templates/taxonomy-templates/#order-taxonomies) in Hugo docs.