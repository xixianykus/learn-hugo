---
title: "Changing the List Order"
date: 2021-04-10T07:52:55+01:00
draft: false
tags: ["plugins", range]
summary: "Change the list order of a list page with cond"
---


So a common problem is where you have different sections of the site. On one section, like the blog, you want the posts to be listed by date so that users see the latest stuff at the top. However another section where you have longer articles it makes more sense to list them alphabetically.

One way of doing this would be to create to list page templates, one for each section. But that would be overkill. You can change the outcome with a `cond`, an `eq` and just a couple of short lines.

```go-html-template
<ul>
    {{ $bytitle := .Pages.ByTitle }}
    {{ $bydate := .Pages }}
    {{ range (cond (eq .Section "articles") $bytitle $bydate) }}
        <li><a href="{{ .Permalink }}">{{ .Title }}</a></li>
    {{ end }}
</ul>
```

Initially I couldn't get this to work. What did I get wrong? Instead of `eq .Section` I had used `eq "Section"`. 