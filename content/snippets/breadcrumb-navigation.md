---
title: "Breadcrumb Navigation"
date: 2021-04-09T11:59:01+01:00
draft: false
tags: [navigation, menus, ancestors]
summary: "How to add breadcrumb navigtion easily"
---

Breadcrumb navigation is really helpful for web sites that have a multi level folder structure.

Since Hugo version 0.109.0 a new method, `{{ .Ancestors }}` simplifies breadcrumb navigation considerably.

```go-html-template
<ul class="breadcrumb-nav">
  {{- range .Ancestors.Reverse }}
  <li>
    <a href="{{ .RelPermalink }}">{{ .LinkTitle }}</a>
  </li>
  {{- end }}
  
  <li class="active" aria-current="page">
    <a href="#">{{ .LinkTitle }}</a>
  </li>
</ul>
```

Save the above code in a partial for use in Hugo templates then use some CSS, such as `display: flex;`, to style it into a useful format.

## Ancestors

The `.Ancestors` method produces a slice (or array) consisting of the parent page, the grand parent, the great grandparent etc. all the way back to the home page. You can loop through this with Hugo's `range` function then access [various page variables](https://gohugo.io/variables/page/) like `.Title` or `.RelPermalink` etc..


{{< details "The old way" >}}
The old way (before `.Ancestors`) was more complicated as seen below:


```go-html-template
<ul  class="breadcrumbs">
    {{ template "breadcrumbnav" (dict "p1" . "p2" .) }}
</ul>

{{ define "breadcrumbnav" }}
  {{ if .p1.Parent }}
    {{ template "breadcrumbnav" (dict "p1" .p1.Parent "p2" .p2 )  }}
  {{ else if not .p1.IsHome }}
    {{ template "breadcrumbnav" (dict "p1" .p1.Site.Home "p2" .p2 )  }}
{{ end }}

  <li{{ if eq .p1 .p2 }} class="active"{{ end }}>
    <a href="{{ .p1.RelPermalink }}">{{ .p1.Title }}</a>
  </li>

{{ end }}
```

{{< /details >}}

  ## Link

  1. [Offical docs page on breadcrumb nav](https://gohugo.io/content-management/sections/#example-breadcrumb-navigation)
  2. [Generating breadcrumbs for Hugo](https://discourse.gohugo.io/t/generating-breadcrumbs-for-hugo/42059/2) a forum post showing how to generate breadcrumb navigation when using [schema](http://schema.org/).
