---
title: "Comma separated Tag List"
title: "Comma separated list"
date: 2022-07-28T20:42:49+01:00
draft: false
tags: ["tags", delimit, range]
summary: "How to produce a comma separated list without a trailing comma when using the range function"
---


When producing a comma seperated list you don't usually want a comma at the end of the list. An easy way to produce a list from an array is using the [`delimit`](https://gohugo.io/functions/delimit/) function.

However it's a bit trickier if you are looping through an array or slice with the `range` function. There are various techniques to do this but the best I've found is the following. It's concise and works well is all situations. 

NB. If you want to run this from within a shortcode use `.Page.Params.tags` instead of `.Params.tags`. Obviously this will work on any taxonomy, not just `tags` and it can also be adapted to other situations too.

## The code

This is a nice simple way that seems to work the best [^1].

Assume we have a frontmatter list of tags something like so:

```yaml
tags: [lemons, apples, banana, grapefruit]
```

The code in a template file to loop over these and produce a linked, comma seperated list without a trailing comma looks like this.


```go-html-template
{{ if .Params.tags }}
    {{ range $i, $e := .Params.tags }}
        {{ if $i }}, {{ end }}
        <a href="/tags/{{ $e | urlize }}">{{ $e }}</a>
    {{ end }}
{{ end }}
```

## How it works

The whole thing is surrounded by an `if` statement so the rest only runs if there is a tags list in the frontmatter of the page.

The first thing the `range` function does is to create two variables for the tags array (or slice in Hugo parlence), `$i` and `$e`. The first, `$i` is the index or array number of each item in the list. The second, `$e`, refers to each element in the array: lemons, apples etc.. The count for the index of each array element always starts at `0`. So the first item in this list, *lemons*, will have and index of `0`. 

The next line is the first thing to be put in our list and that is a comma and a space. However this is part of an `if` statement: `{{ if $i }}`. Now `if` statements return either a `true` or `false` value. The condition required seems to make no sense: *if the index?* What it's really asking is whether the index of the array is not zero. If it's zero, as it will be for the first item, this line won't run because a `0` returns a value of `false`. So the first thing printed out from this range function is not this line but the next which is a link to the first tag in the array.

When the range function goes around a second time (assuming there is more than on item in the list of tags) it will now print the comma and space first so they'll be immediately after the first item.

The looping will continue until all the items are covered and the last line will be the link with the name of the tag rather than the comma and space.

[^1]: The [one in Hugo docs](https://gohugo.io/templates/taxonomy-templates/#example-comma-delimit-tags-in-a-single-page-template) I tried worked well with single words but failed with tag names made up of two or more words.