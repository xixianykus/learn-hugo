---
title: "Last vs Reverse"
date: 2021-04-09T12:20:22+01:00
draft: false
tags: ["range"]
summary: "A subtle difference in range function to produce a list of the latest pages"
---

There is a difference between:

```go-html-template
    <ul>
        <li>Latest Posts</li>
        {{ range first 10 .Site.Pages.ByDate.Reverse }}
        <li><a href="{{.Permalink}}">{{ .Title }}</a></li>
        {{ end }}
    </ul>
```
And 

```go-html-template
    <ul>
        <li>Latest Posts</li>
        {{ range last 10 .Site.Pages.ByDate }}
        <li><a href="{{.Permalink}}">{{ .Title }}</a></li>
        {{ end }}
    </ul>
```

`{{ range first 10 .Site.Pages.ByDate.Reverse }}` is probably most useful. This reverses the order of the entire list of pages then grabs the first 10. The latest post is at the top.

 `{{ range last 10 .Site.Pages.ByDate }}` is almost the same but not quite. The same posts but the latest are at the bottom not the top. If you use `.Reverse` then the WHOLE list is reversed and you get not the latest but the earliest posts on the site.

 NB. You might want `.RegularPages` perhaps though it could be interesting to see new sections too?