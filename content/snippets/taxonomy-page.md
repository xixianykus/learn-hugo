---
title: "Taxonomy Page"
date: 2022-07-28T22:29:12+01:00
draft: false
tags: ["list page", tags]
summary: "The code for my tags and any taxonomy page"
---

The code for my tags page using `.Count` to get the number of entries for each tag...

Note the use `.Page` before the `.RelPermlink` and `.Title`

```go-html-template
{{ if eq .Kind "taxonomy" }}
    <ul class="taxonomy">
        {{ range .Data.Terms.Alphabetical }}
                <li><a href="{{ .Page.RelPermalink }}">{{ .Page.Title }}</a><small> x{{ .Count }}</small></li>
        {{ end }}
    </ul>
{{ else }}
```