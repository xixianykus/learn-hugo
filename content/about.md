---
title: "About"
date: 2020-05-26T06:43:02+01:00
draft: true
menu: lazy
summary: "Some notes about the purpose of this site"
---

This is the more comprehensive *finished* tutorials on Hugo. Some other sites, particularly X7, have been used to experiment with and have some notes there.


## Upcoming Topics

Things I want to learn about:

1. Using SASS within Hugo :heavy_check_mark:
2. Changing the CSS of a page by adding class or id names via the frontmatter
3. Pagination ([good article](https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/) on this)
4. More on the `range` function, scope etc.
5. The *range* function and different parameters.
6. More on Page *kinds*
7.  Shortcodes
8. More on [YAML syntax](/posts/yaml/)
9. TOML syntax
10. Setting up a menu in the config file :heavy_check_mark:
11. .params :heavy_check_mark:
12. Data in the data file
13. Making menu items *active*. That is how to get hugo to recognize a that the menu item is in fact the page that is being shown. ([docs code](https://gohugo.io/templates/menu-templates/#section-menu-for-lazy-bloggers))
14. Images and Image processing.
15. Hugo pipes
16. The dollar sign
17. Adding the date - formatting, time, lastmod etc.
18. Adding XML / xml sitemap 


## ToDo's for this site

1. ~~Set page up so footer is at the bottom on pages with little text.~~
2. ~~Get the footer menu to work~~
3. ~~Add footer to home page~~
4. ~~Use baseof.html~~
5. ~~Get some decent fonts.~~
6. Try to set up a config folder again
7. Tags and categories (add to archetypes/default.md)
8. Add a search page